<?php

// Note: the HTML input files should have &nbsp; coded as &#160; to survive xsl transforms
// also '&' should be coded as '&amp;'
//

ini_set("display_errors", "true");
error_reporting (E_ALL);

require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	
$App 	= new App();	
$Nav	= new Nav();	$Menu 	= new Menu();		
include($App->getProjectCommon());    # Normally all on the same line to unclutter the user's desktop'

$pageKeywords	= "WTP IP Log Eclipse Webtools Platform";
$pageAuthor		= "David Williams";
$root = $_SERVER['DOCUMENT_ROOT'];
$pageTitle = "Eclipse Webtools Platform IP Log";


$xml = new DOMDocument;
$xml->loadHTMLFile("introduction.html");
	// work on just the body of the original (not head, etc.)
	$xmlbody=$xml->getElementsByTagName('body')->item(0);
	

	$committersxml = new DOMDocument;
	$committersxml->loadHTMLFile("committers.html");

	$committersxmlBody=$committersxml->getElementsByTagName('body')->item(0);
	
	foreach ($committersxmlBody->childNodes  as $newNode) {
		$insertableNode = $xml->importNode($newNode, TRUE);
		$xmlbody->insertBefore($insertableNode);
	}
	
		
    $xmlContributions = new DOMDocument;
	$xmlContributions->loadHTMLFile("contributions.html");
	$xmlContributionsBody=$xmlContributions->getElementsByTagName('body')->item(0);
	
	foreach ($xmlContributionsBody->childNodes  as $newNode) {
		$insertableNode = $xml->importNode($newNode, TRUE);
		$xmlbody->insertBefore($insertableNode);
	}
	
	$xmlDistributed = new DOMDocument;
	$xmlDistributed->loadHTMLFile("2008distributed.html");
	$xmlDistributed=$xmlDistributed->getElementsByTagName('body')->item(0);
	
	foreach ($xmlDistributed->childNodes  as $newNode) {
		$insertableNode = $xml->importNode($newNode, TRUE);
		$xmlbody->insertBefore($insertableNode);
	}

	$xmlOther = new DOMDocument;
	$xmlOther->loadHTMLFile("2008dependancies.html");
	$xmlOther=$xmlOther->getElementsByTagName('body')->item(0);
	
	foreach ($xmlOther->childNodes  as $newNode) {
		$insertableNode = $xml->importNode($newNode, TRUE);
		$xmlbody->insertBefore($insertableNode);
	}
	

	// Load the XSL source
	$xsl = DOMDocument::load($root . '/webtools/wtpphoenixPlain.xsl');

	// Configure the transformer
	$proc = new XSLTProcessor;
	$proc->importStyleSheet($xsl); // attach the xsl rules
	
	$maincontent = $proc->transformToXML($xmlbody);
	
	$html = <<<EOHTML
	<div id="midcolumn">
		$maincontent
	</div>

EOHTML;

$Menu=NULL;
$Nav=NULL;
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);


?>
