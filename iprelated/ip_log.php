<?php

// Note: the HTML input files should have &nbsp; coded as &#160; to survive xsl transforms
// also '&' should be coded as '&amp;'
//

//ini_set("display_errors", "true");
//error_reporting (E_ALL);

require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

$pageKeywords	= "WTP IP Log Eclipse Webtools Platform";
$pageAuthor		="Ugur Yildirim @ Eteration A.S.";
$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

$htmlFile="ip_log_current.html";

if (file_exists($htmlFile)) {

	# Generate the web page
	// Load the XML source
	$xml = new DOMDocument;
	$xml->loadHTMLFile($htmlFile);

	//Set the page title
	$xpath = new DOMXPath($xml);
	$titleNode = $xpath->query("/html/head/title")->item(0);
	// echo "<br />titleNodeValue: " . $titleNode->nodeValue;
	$pageTitle = ($titleNode != null) ? $titleNode->childNodes->item(0)->nodeValue : "eclipse.org webtools page";

	// Load the XSL source
	$xsl = DOMDocument::load($root . '/webtools/wtpnova.xsl');

	// Configure the transformer
	$proc = new XSLTProcessor;
	$proc->importStyleSheet($xsl); // attach the xsl rules

	// work on just the body of the original (not head, etc.)
	$xmlbody=$xml->getElementsByTagName('body')->item(0);
	$maincontent = $proc->transformToXML($xmlbody);
	$html = <<<EOHTML
	<div id="maincontent">
	$wtpTopButtons
	$maincontent
	</div>

EOHTML;
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);

} else {
	$pageTitle="File Not Found";
	$html="<p>There is no file: " . $htmlFile . "</p>";
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);

}
?>
