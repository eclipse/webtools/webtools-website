<?xml version="1.0" encoding="UTF-8"?>
<sections title="Dali JPA Tools"
	subtitle="An Eclipse Web Tools Platform Sub-Project">
	<section class="main" name="About Dali">
		<description>
			Dali JPA Tools Project is to build extensible frameworks and
			exemplary tools for the definition and editing of
			Object-Relational (O/R) mappings for EJB 3.0 Java
			Persistence API (JPA) Entities. JPA mapping support will
			focus on minimizing the complexity of mapping by providing
			creation and automated initial mapping wizards, and
			programming assistance such as dynamic problem
			identification.
			<div align="center">
				<p>
					<img
						src="images/dali-screenshot.jpg" />
				</p>
			</div>
		</description>
	</section>
	<section class="main" name="Project Organization">
		<description>
			The Dali JPA Tools project is a sub-project of
			the
			<a href="http://www.eclipse.org/webtools">
				Eclipse Web Tools Platform Project
			</a>.
		</description>
	</section>
	<section class="main" name="Why Dali?">
		<description>
			<table border="0">
				<tr>
					<td width="133">
						<a
							href="http://www.moma.org/collection/browse_results.php?object_id=79018">
							<img
								src="images/persist.jpg" width="133"
								height="99" border="0" />
						</a>
					</td>
					<td width="328">
						Salvador Dali .
						<a
							href="http://www.moma.org/collection/browse_results.php?object_id=79018">
							The Persistence of Memory
						</a>
						. 1931.
						<br />
						(c) 2005 Salvador Dali , Gala-Salvador Dali
						Foundation/Artists Rights Society (ARS), New
						York
					</td>
				</tr>
			</table>
			The focus of Dali is support for JPA O/R mapping which is
			concerned with the persistence of in memory Java POJOs to
			relational databases. The title of the famous painting "The
			Persistence of Memory" aptly describes the goal of the JPA
			specification and provides the link to the Spanish painter
			Salvador Dali.
		</description>
	</section>
	<section class="main" name="Project News" anchor="ProjectNews">
		<section name="Dali Webinar"
			link="http://www.eclipse.org/community/webinars2006.php">
			Wednesday, April 18, 2007 at 8:00 am PDT / 11:00 am EDT / 3:00 pm GMT/UTC
			Presented by Shaun Smith, Dali Project Co-lead and Oracle TopLink Product Manager.
			To register email webinar-dali at eclipse dot org.
			<em>April 2006</em>
		</section>
		<section name="Dali at Eclipse Summit Europe"
			link="http://www.eclipsecon.org/summiteurope2006/"
			image="http://www.eclipsecon.org/summiteurope2006/image138x38.php">
			Plan to attend an introduction to Dali at the upcoming
			<a href="http://www.eclipsecon.org/summiteurope2006/"
				target="_blank">
				Eclipse Summit Europe
			</a>
			.
			<em>August 2006</em>
		</section>
		<section name="Dali at EclipseWorld 2006"
			link="http://www.eclipseworld.net"
			image="images/eclipseworld2006.gif">
			If you'll be at EclipseWorld this year plan to attend the
			session
			<a href="http://www.eclipseworld.net/friday_pm.htm"
				target="_blank">
				Building Applications with the Java Persistence API and
				Dali
			</a>
			.
			<em>August 2006</em>
		</section>
		<section name="Dali 0.5 Released"
			link="downloads.php">
			Dali Preview Release 0.5 is now available for
			<a href="downloads.php">download</a>
			.
			<em>June 2006</em>
		</section>
		<section name="Dali Milestone 3 Released"
			link="https://wiki.eclipse.org/index.php/Dali_Project#Milestone_3:_May_12.2C_2006">
			<a
				href="https://wiki.eclipse.org/index.php/Dali_Project#Milestone_3:_May_12.2C_2006">
				Dali Milestone 3
			</a>
			is now available for
			<a href="downloads.php">download</a>
			. New viewlets of the basic functionality have also been
			posted (see right hand sidebar).
			<em>May 2006</em>
		</section>
		<section name="Dali at EclipseCon 2006"
			link="http://www.eclipsecon.org/2006/Sub.do?id=63">
			Dali project co-leads Neil Hauge and Shaun Smith will be
			presenting along side EJB 3.0 spec co-lead Mike Keith at
			EclipseCon in Santa Clara in March. The presentation title
			says it all
			<a href="http://www.eclipsecon.org/2006/Sub.do?id=63">
				EJB 3.0 Persistence and the Dali EJB ORM Project
			</a>
			.
			<em>Jan 2006</em>
		</section>
		<section name="Dali at Javapolis 2005"
			link="http://wiki.javapolis.com/confluence/display/JP05/Home"
			image="http://www.eclipse.org/dali/images/JavaPolis_logo.gif">
			As part of the
			<a
				href="http://wiki.javapolis.com/confluence/display/JP05/Eclipse%2Bin%2BAction">
				Eclipse In Action
			</a>
			track at
			<a
				href="http://wiki.javapolis.com/confluence/display/JP05/Home">
				Javapolis 2005
			</a>
			, Dali co-lead Shaun Smith will present an introduction to
			<a
				href="http://wiki.javapolis.com/confluence/display/JP05/Eclipse%2Bin%2BAction#EclipseinAction-11%3A4512%3A30HelloDali%21EnterpriseJavaBeans%28EJB%293.0ObjectRelationshipMappingtools">
				EJB 3.0 development using Dali.
			</a>
			<em>Nov 2005</em>
		</section>
		<section
			name="Hello, Dali! - EJB 3.0 Persistence Tools at Eclipse"
			link="http://ryman.eclipsedevelopersjournal.com/hellodali.htm">
			In his
			<a
				href="http://ryman.eclipsedevelopersjournal.com/hellodali.htm">
				blog
			</a>
			, Arthur Ryman the Web Tools Project co-lead discusses Dali
			and other new WTP-related Eclipse projects.
			<em>Sept 2005</em>
		</section>
		<section name="EclipseWorld Demos now available as Viewlets">
			Demos of the Dali tools given at EclipseWorld are now
			available as viewlets. See the 'Dali Demos' sidebar on the
			right.
			<em>Sept 2005</em>
		</section>
		<section name="Dali article in SD Times"
			link="http://www.sdtimes.com/article/story-20050901-01.html">
			The article
			<a
				href="http://www.sdtimes.com/article/story-20050901-01.html">
				EJB Mapping Gets Go-Ahead From Eclipse
			</a>
			by Jenniffer deJong provides details on the project and
			discusses the role of JSR220 in defining a standard Java
			persistence API.
			<em>Sept 2005</em>
		</section>
		<section
			name="Eclipse EJB ORM Project has a new nickname: Dali">
			At EclipseWorld 2005 in New York, the new nickname of the
			Eclipse EJB ORM project was unveiled: 'Dali'. See above for
			details.
			<em>Aug 2005</em>
		</section>
		<section name="Eclipse EJB ORM Project approved">
			The EMO held the proposal creation review on Tuesday, July
			26th. The review attendees included the leads of the Web
			Tools Project and a representative from the Data Tools
			Project PMC. Soon after the review we received the news that
			the Eclipse EJB Object-Relational Mapping Project was
			approved! We're now going through the provisioning process
			to get CVS and other resources setup.
			<em>July 2005</em>
		</section>
		<section name="JBoss joins the Eclipse EJB ORM Project"
			link="http://jboss.com/products/ejb3">
			<a href="http://jboss.com/products/ejb3">JBoss</a>
			has joined
			<a href="http://otn.oracle.com/jpa">Oracle</a>
			and
			<a href="http://www.solarmetric.com" target="_blank">
				SolarMetric
			</a>
			on the Eclipse EJB ORM Project. The participation of three
			of the most active companies on the EJB 3.0 (JSR 220) expert
			group will ensure that the tools and frameworks produced by
			the project will provide excellent support for the
			development of standards compliant EJB 3.0 Entities.
			<em>July 2005</em>
		</section>
		<section name="SolarMetric joins the Eclipse EJB ORM Project"
			link="http://www.solarmetric.com">
			EJB 3.0 Expert Group member company
			<a href="http://www.solarmetric.com" target="_blank">
				SolarMetric
			</a>
			joins
			<a href="http://otn.oracle.com/jpa">Oracle</a>
			to work on the creation of vendor neutral JPA tooling
			for Eclipse.
			<em>July 2005</em>
		</section>
		<section name="JavaOne Demo"
			link="viewlets/EclipseEJB3PluginDemo-3.swf">
			At the Oracle EJB 3.0 booth the first version of the Eclipse
			EJB 3.0 plugin was on display. A
			<a target="_top"
				href="viewlets/EclipseEJB3PluginDemo-3.swf">
				viewlet of the plugin is now available.
			</a>
			<em>June/July 2005</em>
		</section>
		<section name="UI Design Concept Document Posted"
			link="docs/EclipseEJB3ORMFeaturesUseCases.pdf">
			The first draft of the
			<a href="docs/EclipseEJB3ORMFeaturesUseCases.pdf"
				target="_blank">
				ui design document
			</a>
			was posted on the project
			<a
				href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.ejb-orm"
				target="_blank">
				newsgroup
			</a>
			for feedback. The document lays out the prpposed
			code-centric mapping ui approach.
			<em>June 2005</em>
		</section>
		<section name="Oracle Proposes Eclipse EJB ORM Project"
			link="http://www.oracle.com/corporate/press/2005_apr/ejb%20eclipse.html">
			Oracle
			<a
				href="http://www.oracle.com/corporate/press/2005_apr/ejb%20eclipse.html"
				target="_blank">
				announced
			</a>
			that it is expanding its leadership role in driving
			developer adoption of the next-generation enterprise Java
			platform by spearheading a project within the Eclipse
			open-source community to support the Enterprise Java Beans
			(EJB) 3.0 JPA specification.
			<em>April 2005</em>
		</section>
	</section>
	<section class="infobox" name="Learn More">
		<item title="Project News" link="#ProjectNews" />
		<item title="Milestone Plan (Wiki)"
			link="https://wiki.eclipse.org/index.php/Dali_Project" />
		<item title="Newsgroup"
			link="http://www.eclipse.org/newsportal/thread.php?group=eclipse.technology.dali">
			<a href="http://www.eclipse.org/newsgroups/index.html"
				target="_blank">
				[sign up]
			</a>
		</item>
		<item title="Original EJB ORM Project Proposal"
			link="http://www.eclipse.org/proposals/eclipse-ejb30-orm/index.html" />
		<item title="Creation Review Slides"
			link="http://www.eclipse.org/proposals/eclipse-ejb30-orm/Eclipse%20EJB%203.0%20ORM.pdf" />
		<item title="JSR 220 EJB 3.0 Specification Page"
			link="http://www.jcp.org/en/jsr/detail?id=220" />
		<item title="Documentation (User Guide)"
			link="docs/dali_user_guide.pdf" />
		<item title="Tutorial [HTML]"
			link="docs/tutorial/jsf/dali_jsf_tutorial.htm">
			<a href="docs/tutorial/jsf/dali_jsf_tutorial.pdf">[PDF]</a>
		</item>
	</section>
	<section class="infobox" name="Dali Demos">
		<description>Updated for M3</description>
		<item title="Adding JPA Support to a Project"
			link="viewlets/01-Add_Persistence_viewlet_swf.html" />
		<item title="Generating Entities from Tables"
			link="viewlets/02-Generate_Entities_from_Tables_viewlet_swf.html" />
		<item title="Mapping an Entity to a Table"
			link="viewlets/03-Map_Entities_to_Tables_viewlet_swf.html" />
	</section>
	<section class="infobox" name="Downloads">
		<item title="Latest Milestone Build" link="downloads_main.html" />
	</section>
	<section class="infobox" name="Project Participants">
		<item title="JBoss" link="http://jboss.com/products/ejb3" />
		<item title="Oracle - Lead" link="http://otn.oracle.com/jpa" />
	</section>
</sections>
