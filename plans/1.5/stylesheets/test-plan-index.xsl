<?xml version="1.0" encoding="UTF-8"?>

<!--
	
	This stylesheet generates an index of all the test plans from the release.xml file.
	The test-plans.xml file is used to check for the existence of the test plans
	for a given component and milestone.
	
	ChangeLog:
	
	2005-10-13: Arthur Ryman <ryman@ca.ibm.com>
	- Created
	
-->

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="xml" encoding="UTF-8" indent="yes" />

	<!-- plans is the list of all plans that have a location attribute -->
	<xsl:variable name="test-plans"
		select="document('../test-plans.xml')//test-plan[@location]" />

	<xsl:variable name="rel" select="'../'" />

	<!-- 
		This stylesheet generates:
		/1.0/test-plan-index.xml
		
		The root directory for CSS and images is:
		/1.0/www/
		
		So relative to the test-plan-index.xml document, root is:
		www
	-->

	<xsl:template match="/">

		<xsl:processing-instruction name="xml-stylesheet">
			type="text/xsl" href="stylesheets/wtp.xsl"
		</xsl:processing-instruction>

		<xsl:apply-templates />
	</xsl:template>

	<xsl:template match="release">

		<html>
			<head>
				<title>
					<xsl:value-of select="@name" />
					test plan index
				</title>
				<meta name="root" content="www" />
			</head>
			<body>
				<h1>test plan index</h1>
				<h2>Introduction</h2>
				<p>
					This page lists all the test plans for
					<xsl:value-of select="@name" />
					.
				</p>

				<xsl:apply-templates select="bz_component">
					<xsl:sort select="@name" />
				</xsl:apply-templates>

			</body>
		</html>

	</xsl:template>

	<xsl:template match="bz_component">
		<xsl:variable name="component-dir" select="@dir" />
		<xsl:if
			test="$test-plans[starts-with(@location,$component-dir)]">
			<br />
			<h2>
				<xsl:value-of select="@name" />
			</h2>
			<xsl:for-each select="../bz_target_milestone">
				<xsl:variable name="milestone-dir"
					select="concat($component-dir,'/',@dir,'/')" />
				<xsl:variable name="locations"
					select="$test-plans[starts-with(@location,$milestone-dir)]" />
				<xsl:if test="$locations">
					<h3>
						<xsl:value-of select="@milestone" />
					</h3>
					<ul>
						<xsl:for-each select="$locations">
							<li>
								<a href="{@location}">
									<xsl:value-of select="substring-after(@location,$milestone-dir)" />
								</a>
							</li>
						</xsl:for-each>
					</ul>
				</xsl:if>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>
