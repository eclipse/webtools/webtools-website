<?xml version="1.0" encoding="utf-8"?>

<!-- 
	This XSLT sytlesheet combines the adopter hotlist data with the Bugzilla query results to form the report.
	
	Change Log:
	
	2006-01-04: Arthur Ryman <ryman@ca.ibm.com>
	- Updated for WTP 1.5.
	
	2005-11-30: Arthur Ryman <ryman@ca.ibm.com>
	- Added timestamp.
	
	2005-11-29: Arthur Ryman <ryman@ca.ibm.com>
	- Created.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0" xmlns:bz="http://www.bugzilla.org/rdf#"
	exclude-result-prefixes="bz">

	<xsl:param name="timestamp" select="'1900-01-01 00:00:00 UTC'" />

	<xsl:output method="xml" indent="yes" />

	<xsl:variable name="buglist"
		select="document('../adopter-buglist.xml')//bz:bug" />

	<xsl:template match="/">
		<xsl:processing-instruction name="xml-stylesheet">
			type="text/xsl" href="stylesheets/wtp.xsl"
		</xsl:processing-instruction>

		<html>
			<head>
				<title>Web Tools 1.5 Adopter Hot Bug List</title>
				<meta content="www" name="root" />
			</head>

			<body>
				<h1>wtp 1.5 adopter hot bug list</h1>
				<h2>Introduction</h2>
				<p>
					This document lists bugs that adopters have given a
					high priority for resolution in WTP 1.5.
				</p>
				<xsl:apply-templates select="hotlist" />
			</body>
		</html>
	</xsl:template>

	<xsl:template match="hotlist">
		<h2>Hot Bugs</h2>
		<xsl:variable name="bugids">
			<xsl:for-each select="bug">
				<xsl:if test="position()&gt;1">
					<xsl:text>,</xsl:text>
				</xsl:if>
				<xsl:value-of select="id" />
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="query_base"
			select="concat('https://bugs.eclipse.org/bugs/buglist.cgi?bug_id=',$bugids)" />
		<xsl:variable name="exclude_base"
			select="concat($query_base,'&amp;product=Web+Tools&amp;bugidtype=exclude')" />
		<h3>Bugzilla Queries for Hot Bugs Only</h3>
		<p>
			<ul>
				<li>
					<a
						href="{$query_base}&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED">
						Unresolved
					</a>
				</li>
				<li>
					<a
						href="{$query_base}&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED">
						Resolved
					</a>
				</li>
				<li>
					<a href="{$query_base}">All</a>
				</li>
			</ul>
		</p>
		<h3>Bugzilla Queries that Exclude Hot Bugs</h3>
		<p>
			<ul>
				<li>
					<a
						href="{$exclude_base}&amp;priority=P1&amp;priority=P2&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED">
						Unresolved P1/P2
					</a>
				</li>
				<li>
					<a
						href="{$exclude_base}&amp;bug_severity=blocker&amp;bug_severity=critical&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED">
						Unresolved blocker/critical
					</a>
				</li>
			</ul>
		</p>
		<h3>Bugzilla Snapshot</h3>
		<p>
			The following table lists the all the hot bugs, sorted by
			id, and their status as of
			<xsl:value-of select="$timestamp" />
			:
		</p>
		<table cellspacing="4">
			<tr>
				<th>Bug</th>
				<th>Adopter</th>
				<th>Contact</th>
				<th>Assigned To</th>
				<th>Status</th>
				<th>Resolution</th>
				<th>Priority</th>
				<th>Severity</th>
				<th>Target</th>
				<th>Description</th>
			</tr>

			<xsl:apply-templates select="bug">
				<xsl:sort select="id" data-type="number" />
			</xsl:apply-templates>

		</table>

	</xsl:template>

	<xsl:template match="bug">
		<xsl:variable name="id" select="id" />
		<xsl:variable name="bz_bug" select="$buglist[bz:id=$id]" />
		<tr>
			<td>
				<a
					href="https://bugs.eclipse.org/bugs/show_bug.cgi?id={id}">
					<xsl:value-of select="id" />
				</a>
			</td>
			<td>
				<xsl:value-of select="adopter" />
			</td>
			<td>
				<xsl:value-of select="contact" />
			</td>
			<td>
				<xsl:value-of select="$bz_bug/bz:assigned_to" />
			</td>
			<td>
				<xsl:value-of select="$bz_bug/bz:bug_status" />
			</td>
			<td>
				<xsl:value-of select="$bz_bug/bz:resolution" />
			</td>
			<td>
				<xsl:value-of select="$bz_bug/bz:priority" />
			</td>
			<td>
				<xsl:value-of select="$bz_bug/bz:bug_severity" />
			</td>
			<td>
				<xsl:value-of select="$bz_bug/bz:target_milestone" />
			</td>
			<td>
				<xsl:value-of select="$bz_bug/bz:short_short_desc" />
			</td>
		</tr>
	</xsl:template>


</xsl:stylesheet>