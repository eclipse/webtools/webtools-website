<?xml version="1.0" encoding="utf-8"?>

<!-- 
	This XSLT sytlesheet extracts the bug ids from the hot list and converts them into a properties file
	that can be read by Ant.
	
	Change Log:
	
	2005-11-29: Arthur Ryman <ryman@ca.ibm.com>
	- Created.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">

	<xsl:output method="text" />

	<xsl:template match="/">
		<xsl:text>adopter_bugids=</xsl:text>
		<xsl:for-each select="hotlist/bug">
			<xsl:if test="position()&gt;1">
				<xsl:text>,</xsl:text>
			</xsl:if>
			<xsl:value-of select="id" />
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>