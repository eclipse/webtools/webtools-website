<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="../../../../wtp.xsl"?>
<html>
    <head>
        <meta name="root" content="../../../www/" />
        <title>css wtp 2.0 final test plan</title>
    </head>
    <body>
        <h1>css wtp 2.0 final test plan</h1>
        <h2>Status of this Plan</h2>
        <p>Proposed Plan (5.14.07)</p>

        <h2>Overall goals</h2>

        <h3>
            <b>End User Testing</b>
        </h3>
        <p>
            The nature of the end-user testing is intentionally planned
            to be "ad hoc" instead of specifying step by step "how to"
            directions and specific "expected results" sections often
            seen in test cases. This is done because its felt leads to
            greater number of "paths" being tested, and allows end-users
            more motivation for logging "bugs" if things didn't work as
            <i>they</i>
            expected, even if it is working as designed.
        </p>

        <p>
            As we progress through milestones, we'll add more and more
            detail for special cases, special files, special projects,
            etc. When we do have special or sample test files and
            projects, we will keep those stored in CVS, as projects
            under a
            <i>testdata</i>
            directory under the
            <i>development</i>
            directory of relevant component so that testers (from
            immediate team, or community) can easily check out into the
            environment being tested.
        </p>

        <h3>
            <b>Platform Testing</b>
        </h3>
        <p>
            While we do not have any platform specific code, or
            function, we will have some team members do end-user tests
            on Linux, some on Windows. We will also confirm unit tests
            pass on both platforms.
        </p>

        <h3>
            <b>Performance Testing</b>
        </h3>
        <p>
            We have added (some) automated performance tests along the
            lines of the Eclipse base performance unit tests in future
            milestones. These are currently in the
            <b>org.eclipse.wst.*.ui.tests.performance</b>
            and
            <b>org.eclipse.jst.jsp.ui.tests.performance plugins</b>
            <br />
            <br />
            We will continue to add more test cases in upcoming
            milestones.
        </p>
        <h2>Testing focus for 2.0</h2>
        <ul>
            <li>Verify quick fix for spelling mistakes works</li>
            <li>
                Formatting:
                <ul>
                    <li>
                        Verify format with no selection will format
                        document while format while text selected will
                        only format selected text
                    </li>
                    <li>
                        Verify formatting of classes (.myclass) and ids
                        (#myid) in css are handled better
                    </li>
                    <li>
                        Make sure to run
                        <a
                            href="../../../../wst/components/sse/tests/formatting-test.html">
                            formatting regression test
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                Preferences:
                <ul>
                    <li>
                        Syntax highlighting preferences:
                        <ul>
                            <li>
                                Verify font styles (bold, italics,
                                strikethrough, etc) are obeyed
                            </li>
                            <li>
                                Verify background syntax color
                                preference is followed
                            </li>
                        </ul>
                    </li>
                    <li>Verify keywords for preference pages works</li>
                    <li>
                        Verify text editor's smart home/end caret
                        positioning preference is followed
                    </li>
                    <li>
                        Verify hyperlinking preferences in text editor's
                        hyperlinking preference page is followed
                    </li>
                    <li>
                        Verify content assist proposal
                        background/foreground color preferences in
                        structured text editor's preference page are
                        followed
                    </li>
                </ul>
            </li>
            <li>
                Verify Web Content Settings and Task Tags
                preference/properties pages are correct and correctly
                followed for workspace vs project vs file.
            </li>
            <li>
                Make sure to run
                <a
                    href="../../../../wst/components/sse/tests/action-test.html">
                    structured text editor action regression test
                </a>
            </li>
        </ul>
        <h2>CSS Tests</h2>
        <ul>
            <li>
                Check Source Editing features from
                <a href="../sse/test_plan.php#matrix">feature matrix</a>
            </li>
            <li>
                Test Source page, Outline view, Properties view
                synchronization
            </li>
            <li>
                Preferences:
                <ul>
                    <li>
                        Test our editors follow preferences in the "All
                        Text Editors" preference page
                    </li>
                    <li>
                        Check that editors follow content type specific
                        preferences under Web and XML preference pages
                    </li>
                    <li>
                        Make sure editors (already open and closed and
                        reopened) are updated when preferences change
                    </li>
                    <li>
                        Check preferences are saved when shutdown and
                        restart workbench
                    </li>
                </ul>
            </li>

            <li>
                Tab preferences:
                <ul>
                    <li>
                        Web and XML-&gt;Content Type Files -&gt; Content
                        Type Source -&gt; Indent using tabs / Indent
                        using spaces -&gt; Indentation size
                    </li>

                    <li>Verify the correct tab character is used</li>
                    <li>
                        Verify the correct number of tab characters is
                        used
                    </li>
                    <li>
                        Verify Source-&gt;Shift Left/Shift Right and the
                        Shift-Tab/Tab key follow the preferences
                    </li>
                    <li>With nothing selected</li>
                    <li>With multiple lines selected</li>
                    <li>Verify Format follow the preferences</li>
                    <li>
                        Verify when using tab characters, the displayed
                        tab width preference is followed
                        (General-&gt;Editors-&gt;Text Editors -&gt;
                        Displayed tab width)
                    </li>
                </ul>
            </li>
            <li>
                New File Wizard:
                <ul>
                    <li>make sure the template creates a valid file</li>
                    <li>
                        Verify not entering an extension will generate a
                        new file with the default file extension you
                        specified in the preference.
                    </li>
                    <li>
                        Verify entering a file name that already exists
                        without the extension will still give you an
                        error saying the file already exists (for
                        example, if index.jsp already exists, typing
                        "index" will tell you that it already exists)
                    </li>
                    <li>
                        Verify entering a file name with valid/invalid
                        extension still works
                    </li>
                </ul>
            </li>
            <li>
                General Content Settings
                <ul>
                    <li>
                        Verify project properties are inherited when
                        file properties are not set
                    </li>
                    <li>
                        Verify setting properties actually saves the
                        properties settings when you reopen properties
                        dialog and when you restart workbench
                    </li>
                    <li>
                        Verify properties still valid/saved when you
                        move the associated file
                    </li>
                    <li>
                        Verify nothing funny happens with build or if
                        you have a readonly project or something that
                        nothing goes wrong (since properties are stored
                        inside project)
                    </li>
                </ul>
            </li>
            <li>
                Web Content Settings
                <ul>
                    <li>
                        Verify Web Content Settings shows up in
                        Properties dialog for html, jsp, css files and
                        web projects (and only then)
                    </li>
                    <li>
                        For CSS files, there should only be a CSS
                        Profile option. Verify it is used For HTML/JSP
                        files there should also be a Document type
                        option.
                    </li>
                    <li>
                        Verify it is used (for example, specify frameset
                        doctype and verify content assist proposes
                        frameset tag)
                    </li>
                </ul>
            </li>
            <li>
                Profiling:
                <ul>
                    <li>
                        Using your favorite profiler (
                        <a
                            href="http://eclipsefaq.org/chris/xray/index.html">
                            XRay
                        </a>
                        , YourKit, etc...) test basic editor functions
                        and look for problem areas (large memory
                        consumption, intense CPU usage)
                    </li>
                </ul>
            </li>

        </ul>
        <h2>
            <b>Regression Tests</b>
        </h2>
        <p>
            <a
                href="../../../../wst/components/sse/tests/viewerconfig-test.html">
                Structured Text Viewer Configuration tests
            </a>
            <br />
            <a
                href="../../../../wst/components/sse/tests/action-test.html">
                Structured Text Editor Action tests
            </a>
            <br />
            <a
                href="../../../../wst/components/sse/tests/formatting-test.html">
                Formatting tests
            </a>
            <br />
            <a
                href="../../../../wst/components/sse/tests/codefolding-test.html">
                Code Folding tests
            </a>
            <br />
        </p>

        <h2>Source Editing Test Plans</h2>
        <p>
            <a href="../sse/test_plan.php">org.eclipse.wst.sse</a>
            <br />
            <a href="../xml/test_plan.php">org.eclipse.wst.xml</a>
            <br />
            <a href="../html/test_plan.php">org.eclipse.wst.html</a>
            <br />
            <a href="../css/test_plan.php">org.eclipse.wst.css</a>
            <br />
            <a href="../dtd/test_plan.php">org.eclipse.wst.dtd</a>
            <br />
            <a href="../javascript/test_plan.php">
                org.eclipse.wst.javascript
            </a>
            <br />
            <a href="../../jst/jsp/test_plan.php">
                org.eclipse.jst.jsp
            </a>
        </p>
    </body>
</html>
