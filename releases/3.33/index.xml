<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../../wtpphoenix.xsl"?>
<html>
	<head>
    	<meta name="root" content="../../../" />
		<title>eclipse web tools platform release 3.33</title>
	</head>
	<body>
		<h1>WTP 3.33</h1>
		<h2>Status as of 2024-03-13</h2>
		<p>
			WTP 3.33 was officially released on March 13, 2024, as part of the
			Eclipse IDE 2024-03.
			Users of Eclipse should use the 2024-03 Update Site,
			<a href="https://download.eclipse.org/releases/2024-03/">https://download.eclipse.org/releases/2024-03/</a>,,
			to install WTP 3.33, or
			<a href="https://download.eclipse.org/releases/latest/">https://download.eclipse.org/releases/latest/</a>
			to always have the latest release. The next release, WTP 3.34, will be part of
			the simultaneous release in June.
		</p>
		<ul>
					<li>Download WTP as part of a complete Eclipse <a href="https://www.eclipse.org/downloads/packages/release/2024-03/r">package</a> or <a href="https://download.eclipse.org/webtools/downloads/drops/R3.33.0/R-3.33.0-20240304165142/">on its own</a>. WTP is featured in the Eclipse IDE for <a href="https://www.eclipse.org/downloads/packages/release/2024-03/r/eclipse-ide-enterprise-java-and-web-developers">Enterprise Java and Web Developers</a>.</li>
					<li>Read the <a href="https://wiki.eclipse.org/Category:New_Help_for_Old_Friends">adopter migration documentation</a></li>
					<li>Find WTP in the <a href="https://marketplace.eclipse.org/user/nitind/listings">Eclipse Marketplace</a></li>
<!--
					<li><a href="http://download.eclipse.org/webtools/downloads/">Download</a> the final WTP 3.33 built P2 Update Site now</li>
          <li>Browse the <a href="http://help.eclipse.org/2024-03/topic/org.eclipse.wst.doc.user/topics/overview.html?cp=90" target="_top">Web Tools Platform User Guide</a></li>
-->
					<li>Read the <a href="https://wiki.eclipse.org/WTP_FAQ">FAQ</a></li>
					<li>Read the <a href="https://wiki.eclipse.org/WTP_Tomcat_FAQ">WTP and Apache Tomcat FAQ</a></li>
            <!--  <h3><a href="contributorrecognition.html">Recognition of Contributors</a></h3> -->
            <!--
             <h2>Press Coverage for WTP 3.33</h2>
             <h3><a href=""  target="_top">title</a>, <i>source</i></h3>
             -->
		</ul>
        <!--
		<h2><a name="NewAndNoteworthy" />New &amp; Noteworthy</h2>
		Check out what's <a href="NewAndNoteworthy/">New and Noteworthy</a> in this release -->
		<h2><a name="what-is-fixed" />What is Fixed</h2>
		<p>Fixed <a href="https://bugs.eclipse.org/bugs/buglist.cgi?classification=WebTools&amp;query_format=advanced&amp;target_milestone=3.33&amp;target_milestone=3.33%20M1&amp;target_milestone=3.33%20M2&amp;target_milestone=3.33%20M3&amp;target_milestone=3.33%20RC1&amp;target_milestone=3.33%20RC2&amp;target_milestone=3.33.0">list</a>/<a
		href="https://bugs.eclipse.org/bugs/report.cgi?x_axis_field=bug_severity&amp;y_axis_field=product&amp;no_redirect=1&amp;query_format=report-table&amp;classification=WebTools&amp;target_milestone=3.33&amp;target_milestone=3.33+M1&amp;target_milestone=3.33+M2&amp;target_milestone=3.33+M3&amp;target_milestone=3.33+RC1&amp;target_milestone=3.33+RC2&amp;target_milestone=3.33.0&amp;format=table&amp;action=wrap">table</a>. Note that as some WTP projects now live on GitHub, this is an incomplete collection.</p>
		<p>Adopters should consult the <a href="https://wiki.eclipse.org/Category:New_Help_for_Old_Friends">migration</a> information for potentially breaking changes.</p>

		<h2>
			<a name="reported-problems" />
			Reported and Known Problems
		</h2>
		<p>
			All reported problems are recorded in the Eclipse Bugzilla
			system under:
			<ul>
				<li>
					Classification =
					<code>WebTools</code>
				</li>
				<li>
					Status =
					<code>NEW</code>
					or
					<code>ASSIGNED</code>
					or
					<code>REOPENED</code>
				</li>
			</ul>
		</p>
		<a href="https://bugs.eclipse.org/bugs/buglist.cgi?bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED&amp;bug_status=UNCONFIRMED&amp;classification=WebTools&amp;order=bugs.bug_id%20desc">All reported problems</a>
		<p>Check for suggested workarounds in the bug report comments. Remember: <b>bugs that aren't reported have little chance of being fixed.</b><!--Here are more specific items:--></p>

		<!--
		<table cellpadding="3%" >

		<tr>
			<td><a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=number">number</a></td>
			<td> title </td>
		</tr>
		</table>
		-->
	</body>
</html>
