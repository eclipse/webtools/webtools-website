<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../../../development/news/new_and_noteworthy.xsl"?>
<release name="3.1" root="../../..">
	<greeting>Graduating from the WTP Incubator this year are the WTP XSL
		Tools, featuring an XSLT source editor with debugging support,
		initiated by the community, and built upon WTP's existing XML tools.
		Users of the <a href="http://eclipse.org/ecf/">Communication Framework</a>'s
		Real-Time Shared Editing features may find a few pleasant surprises as well. 
	</greeting>
	<component name="SSE">
		<item title="Semantic Highlighting support">
			<description>
				Although it will be more exciting once adopters start using it,
				SSE-based editors now support Semantic Highlighting. Enablement of
				this functionality is controlled from the
				<b>Structured Text Editors</b>
				Preference page.
				<br />
				<br />
				<img src="sourceediting/semantic.png" alt="Semantic Highlighting option on the Structured Text Editors preference page" />
			</description>
		</item>
	</component>
	<component name="XML">
		<item title="New XML Perspective">
			<description>
				An XML perspective is now provided, showcasing the new <b>XPath</b> and
				<b>Templates</b> views.
				<br />
				<br />
				<img src="sourceediting/xml-perspective-3.1.png" alt="XML Perspective with its default layout" />
			</description>
		</item>
		<item title="Tooltips in the Outline">
			<description>
				The Outline view will now show the content of a Comment when
				hovered over. Hovering over an Element will now also the content of
				its preceding Comment.
				<br />
				<br />
				<br />
				<img src="sourceediting/outline-comments.png" alt="Tooltip showing over an XML comment in Outline view" />
			</description>
		</item>
		<item
			title="XML Validation now supports the 'Honour all schema locations' option">
			<description>
				XML Schema documents with multiple import statements that share the
				same namespace but point to different schema locations can be
				successfully validated by using the 'Honour all schema locations'
				option in the Preferences dialog. It is now also possible to
				validate XML instance documents that make use of such schemas. To
				accommodate the change, the checkbox control to enable/disable this
				feature has been moved to a new location in the preferences dialog.
				<br />
				<br />
				<img src="sourceediting/sourceEditing001.png" alt="New XML Validation preferences" />
			</description>
		</item>
		<item title="New XML Smart Insert preference">
			<description>
				When the typing preference is enabled, the XML editor can
				automatically insert a matching end tag when a start tag is
				completed.
				<br />
				<br />
				<img src="sourceediting/xml_typing_preferences.png" alt="New XML Typing preference page" />
			</description>
		</item>
	</component>
	<component name="XML Schema">
		<item title="Advanced tab for simple types">
			<description>
				<p>
					There is now an Advanced tab in the Properties view for simple
					types. This tab allows editing of advanced attributes like final.
					<br />
					<br />
					<img src="sourceediting/advancedsimpletype.png" alt="Simple type Advanced tab" />
				</p>
			</description>
		</item>
		<item title="Export diagram as PNG image">
			<description>
				<p>
					The
					<b>Export Diagram as Image</b>
					function now supports the
					<a href="http://www.w3.org/Graphics/PNG/">PNG</a>
					format.
					<br />
					<br />
					<img src="sourceediting/exportimage.png" alt="Export diagram as PNG image dialog" />
				</p>
			</description>
		</item>
		<item title="Properties sheet for enumeration">
			<description>
				<p>
					Users can now edit the values for enumerations via the
					<b>Properties</b>
					view.
					<br />
					<br />
					<img src="sourceediting/enumerationproperties.png" alt="Properties sheet for enumeration" />
				</p>
			</description>
		</item>
		<item title="Advanced tab for schema element">
			<description>
				<p>
					There is now an
					<b>Advanced</b>
					tab in the
					<b>Properties</b>
					view for the schema element. This tab allows users to edit
					namespace prefixes, block default, and final default.
					<br />
					<br />
					<img src="sourceediting/schemaadvanced.png" alt="Advanced tab for schema element" />
				</p>
			</description>
		</item>
		<item title="Prompt for schema location when adding directive">
			<description>
				<p>
					By default, users are now presented with a wizard to select the
					schema location when adding a directive in the XML Schema Editor
					outline view.
					<br />
					<br />
					<img src="sourceediting/directiveprompt2.png" alt="Prompt for schema location when adding directive" />
					<br />
					<br />
					Automatic showing of the wizard can be disabled from the
					<b>XML Schema Files</b>
					preference page.
					<br />
					<br />
					<img src="sourceediting/directiveprompt.png"
						alt="Prompt for schema location when adding directive preference" />
				</p>
			</description>
		</item>
		<item title="Improved visual cues for abstract types and elements">
			<description>
				<p>
					Abstract types and elements are now italicized in the XML Schema
					editor.
					<br />
					<br />
					<img src="sourceediting/abstractElement.png" alt="Italicized abstract element" />
				</p>
			</description>
		</item>
		<item title="Multiple selection of items">
			<description>
				<p>
					Users can now hit Ctrl+A or select Edit-&gt;Select All to select
					all items in the XML Schema editor. Users can also hold the Shift
					key to select a range of items in the editor. 
					<br />
					<br />
					<img src="sourceediting/selectAll.png" alt="Select all items" />
				</p>
			</description>
		</item>
		<item title="New validation section in XML Schema File preferences">
			<description>
				A new 'Validation' subsection has been created for XML Schema Files
				preferences. The content of the setting pages has been moved to
				accommodate this change. A hyperlink between the XML Schema and XML
				File validation sections has been provided.
				<br />
				<br />
				<img src="sourceediting/sourceEditing002.png" alt="New XML Schema Validation preference page" />
			</description>
		</item>
		<item title="Set base type in Design View">
			<description>
				It is now possible to set the base type of simple and complex type
				declarations in the XML Schema editor design view:
				<br />
				<br />
				<img src="sourceediting/sourceediting0001.png" alt="Set Base Type context menu" />
				<br />
				The new context menu item "Set Base Type" brings up a dialog box
				with the available type declarations that are suitable for the
				selection:
				<br />
				<br />
				<img src="sourceediting/sourceediting0002.png" alt="Set Type dialog box" />
				<br />
				<br />
				<br />
				When the selection consists of a complex type declaration, setting
				the base to a simple type will generate in the source view simple
				content and extension elements:
				<br />
				<br />
				<img src="sourceediting/sourceediting0003.png" alt="Simple content" />
				<br />
				<br />
				<br />
				If the base is instead set to a complex type, complexContext and
				extension elements are generated in the source view:
				<br />
				<br />
				<img src="sourceediting/sourceediting0004.png" alt="Complex content" />
				<br />
				<br />
				<br />
				Finally, if the selection is a simple type declaration, a
				restriction element in the source view is generated:
				<br />
				<br />
				<img src="sourceediting/sourceediting0005.png" alt="Simple Type" />
				<br />
				<br />
				<br />
				At all times the properties view is updated to reflect the changes
				performed in the document:
				<br />
				<br />
				<img src="sourceediting/sourceediting0006.png" alt="XML Schema editor Properties view" />
			</description>
		</item>
	</component>
	<component name="HTML">
		<item title="Hyperlinks between anchors">
			<description>
				<p>
					The HTML editor now provides hyperlinks between
					<a href="http://www.w3.org/TR/html401/struct/links.html#edef-A">anchor</a>
					tags and their targets in the same document, to anchors in other
					resources, and to their referrer(s) in the same file.
					<br />
					<br />
					<img src="sourceediting/anchor_forward.png" alt="Links to named anchors within the same file" />
					<br />
					<!--
						Note: having multiple identically named targets within a file is
						invalid.
					-->
					Hyperlinks to matching named anchors within the same file.
					<br />
					<br />
					<img src="sourceediting/anchor_forward2.png" alt="A link to an anchor in another file" />
					<br />
					A hyperlink to an anchor in another file.
					<br />
					<br />
					<img src="sourceediting/anchor_reverse.png"
						alt="Links to anchors pointing to this anchor from within this file" />
					<br />
					Hyperlinks to anchors pointing to this anchor from within this
					file.
					<br />
					<br />
				</p>
				<p>
					Enablement of the Anchors link kind is controlled from the
					<b>Hyperlinking</b>
					Preference page.
					<br />
					<br />
					<img src="sourceediting/anchor_prefs.png"
						alt="Enablement of the Anchors link kind on the Hyperlinking Preference page" />
				</p>
			</description>
		</item>
		<item title="New HTML Smart Insert preference">
			<description>
				When the typing preference is enabled, the HTML editor can
				automatically insert a matching end tag when a start tag is
				completed.
				<br />
				<br />
				<img src="sourceediting/html_typing_preferences.png" alt="HTML Typing preference page" />
			</description>
		</item>
	</component>
	<component name="JSP">
		<item title="Automatic revalidation from depencencies">
			<description>JSP files are now automatically
				revalidated any fragments they include are modified.
				Existing workspaces will need to revalidate their JSP files to
				enable this feature.</description>
		</item>
		<item title="Java Task Tags in JSP files">
			<description>
				Tags specified for Java files will now be detected for Java sources
				within JSP files. This features requires that the
				<b>JSP Syntax Validator</b>
				be enabled and the
				<b>Validation Builder</b>
				run.
				<br />
				<br />
				<img src="sourceediting/java_tasks.png" alt="Java Task Tags within a JSP Scriptlet" />
				<br />
				<br />
				These tags are set from the <b>Java Compiler Task Tags</b>
				preference page instead of the <b>Structured Text Editors Task Tags</b>
				preference page.
				<br />
				<br />
				<img src="sourceediting/java_tasks2.png" alt="The Java Compiler Task Tags Preference page" />
			</description>
		</item>
		<item title="Enhancements when editing Tag Library Descriptors">
			<description>
				When editing a JSP Tag Library Descriptor, the XML Editor will offer
				some additional functionality. First, the Outline view will show
				more information about the TLD's values.
				<br />
				<br />
				<img src="sourceediting/tld-outline.png" alt="Outline view when working with a TLD" />
				<br />
				<br />
				Also, hyper-links are now available for Java classes referenced
				anywhere within the TLD source.
				<br />
				<br />
				<img src="sourceediting/tld-class-hyperlink.png" alt="Armed hyperlink to a Java class" />
			</description>
		</item>
	</component>
	<component name="XSL">
		<item title="XSLT Java Processors Preference Page">
			<description>
				The setup for XSLT Java processors is now done on their own
				Preference page.
				<br />
				<br />
				<img src="sourceediting/processors.png" alt="" />
			</description>
		</item>
		<item title="Content Assist: exclude-result-prefix attribute">
			<description>
				<p> Content assistance is now available on XSLT exclude-result
					prefixes.</p>
				<img src="sourceediting/ExcludeContentAssist1.png" alt="" />
				<p> The content assistance will provide a list of all available
					namespace prefixes that can be excluded.</p>
				<img src="sourceediting/ExcludeContentAssist2.png" alt="" />
				<p> If a result prefix has already been excluded, it is not
					available in the proposal list. i.e. html has already been excluded
					so it does not show up in the list.</p>
			</description>
		</item>
		<item title="Content Assist: mode attribute">
			<description>
				<p> Content assistance is now available for the XSLT mode attribute.
					This attribute is on the xsl:template and xsl:apply-templates
					elements.</p>
				<img src="sourceediting/modeAssistance.png" alt="" />
				<p> The assistance will find all available modes that have been
					defined in the current stylesheet as well as any imported or
					included stylesheets.</p>
			</description>
		</item>
		<item title="Content Assitance: call-template">
			<description>
				<p> Content Assistance is available for the name attribute on the
					xsl:call-template element.</p>
				<img src="sourceediting/calltemplate.png" alt="" />
				<p> This will provide proposals of available named templates that
					can be called. This searches the current stylesheet as well as any
					imported or included stylesheets.</p>
			</description>
		</item>
		<item title="Content Assitance: include/import href">
			<description>
				<p> Content assistance is available for the href attribute on
					xsl:include and xsl:import elements.</p>
				<img src="sourceediting/hrefAssistance.png" alt="" />
				<p> This will search the current project for any XSLT stylesheet
					that is available, and provide it as a possible proposal. It is
					limited in scope to the current project.</p>
			</description>
		</item>
		<item title="Content Assistance: xsl element proposals">
			<description>
				<p> XSL element proposals are now available for positions that
					aren't within the current XSL namespace. This allows for xsl
					proposals underneath other elements. The scope is determined by the
					first ancestor xsl element that is found.</p>
				<img src="sourceediting/xslproposals.png" alt="" />
			</description>
		</item>
		<item title="Debugger: Result View">
			<description>
				<p> The XSLT debugger now supports a result view. The view will show
					the output that has been generated to the current break point. As a
					user steps through code the view will be updated as well.</p>
				<img src="sourceediting/resultView.png" alt="" />
			</description>
		</item>
		<item title="Editor: Template Override Marker">
			<description>
				<p> The XSLT editor now provides a marker for when a template
					overrides an imported template.</p>
				<img src="sourceediting/override1.png" alt="" />
				<p> Moving the mouse over the green triangle will show in which xsl
					stylesheet the template resides that is being overridden.</p>
				<img src="sourceediting/override2.png" alt="" />
			</description>
		</item>
		<item title="XSLT Syntax Coloring">
			<description>
				<p> The XSL Tools Editor can have syntax coloring that is specific
					just for the XSLT Namespace.</p>
				<img src="sourceediting/syntaxColoring.png" alt="XSL Editor Syntax Coloring" />
				<p> Control of the coloring that is used for the XSLT namespace
					items is handled through the Syntax Coloring preference page for
					XSL.</p>
				<img src="sourceediting/xslSyntaxColoring.png" alt="XSL Syntax Coloring Preference" />
				<p> Non-XSLT coloring is handled by the standard XML Syntax Coloring
					preference page.</p>
			</description>
		</item>
		<item title="Project specific Validation Settings">
			<description>
				<p> XSL Tools now supports project specific settings for validation.
				</p>
				<img src="sourceediting/projectLevelValidation.png" alt="Project Level Validation" />
			</description>
		</item>
		<item title="Content Assist: Named Templates">
			<description>
				<p> XSLT named templates have Content Assistance based on the
					call-templates that are available in the model. This includes
					imported and included style-sheets.</p>
				<img src="sourceediting/namedTemplate.png" alt="Named Template Assistance" />
			</description>
		</item>
		<item title="Debug: XSL NodeSets">
			<description>
				<img src="sourceediting/nodesetVariable.png" alt="XSLT Nodeset Variable" />
				<p> During debugging of an XSLT Stylesheet, variables that contain
					NodeSets are now expandable, allowing inspection of the contents of
					the Nodes carried.</p>
			</description>
		</item>
		<item title="Content Assist: Templates View">
			<description>
				<p>
					The XSL Tools Editor now supports the
					<b>Templates View</b>
					. This allows for drag and drop support of XPath templates into the
					editor. Users may also use this view to create and maintain new or
					existing templates.
				</p>
				<img src="sourceediting/templatesView.png" alt="Templates View" />
				<p>
					The
					<b>Templates View</b>
					has been added by default to the XML perspective. Reset the
					perspective if you already are using this perspective to have it
					added. Users may also open the view using
					<i>Quick Access</i>
					(usually CTRL+3/Command+3).
				</p>
			</description>
		</item>
		<item title="XML Model Content Assist in XPath Expressions">
			<description>
				<p> The XSLT editor now supports content assistance for XML files
					with namespaces or an inferred grammar.</p>
				<img src="sourceediting/xmlXPathAssist.png" alt="XML Model XPath Assistance" />
				<p> This currently only works for globally defined elements in the
					namespace that has been defined in the XML Catalog. It also will
					work with DTDs as well as grammars loaded using XML Schema
					Location. It is recommended though that the grammar be setup with
					in the XML Catalog as a Namespace Key.</p>
			</description>
		</item>
		<item title="XPath Content Assistance">
			<description>
				<p> XPath content assistance has been reworked to address several
					bugs when it came to providing proposals. Proposals should now work
					directly after slashes, commas, parentheses, brackets, and axis
					statements.</p>
			</description>
		</item>
		<item title="XPath View: No Matches">
			<description>
				<p> The XPath View has been updated so that if there are no results
					returned for the expression, it will return "No Matches" within the
					view itself. It also has been updated to clear the display when a
					non SSE based editor is in focus.</p>
				<p> The view will also dynamically try and update itself as the
					active XML document is being editted.</p>
			</description>
		</item>
		<item title="Validators: XPath 2.0 Validation for XSLT 2.0">
			<description>
				<p> The XSLT Validators will correctly handle valid XPath 2.0
					expressions in XSLT 2.0. In the past, these were marked as being in
					error. This functionality is leveraging the XPath 2.0
					processor/parsing capabilities of the PsychoPath processor.</p>
			</description>
		</item>
		<item title="XPath 2.0: PsychoPath processor">
			<description>
				<p> XSL Tools now includes and maintains the XPath 2.0 processor,
					known as psychopath, thanks to a contribution from Andrea Bittau.
					This is a mostly compliant implementation of a 2004 Draft of the
					XPath 2.0 specificiation. It is also an XML Schema aware processor,
					allowing for the use of the XML Schema data types and grammars. The
					processor can be used standalone outside of eclipse as it's own JAR
					file.</p>
				<p> Future plans for this processor include having it be compliant
					to the current approved XPath 2.0 specification.</p>
			</description>
		</item>
		<item title="Launching: Open Files">
			<description>
				<p> A code contribution from Stuart Harper now allows for the
					selection of existing files that may be already opened in editors.
					This is available when selecting an XSL to launch or pressing
					CTRL+F11 or F11 for debugging.</p>
				<img src="sourceediting/openfilesLaunch.png" alt="Launch Open Files" />
				<p> Note: Selection is limited to those files that are stored on a
					file system and not accessed remotely.</p>
			</description>
		</item>
	</component>
</release>