<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="../../wtphome.xsl"?>
<sections title="what's new in WTP 0.7" subtitle="Eclipse Web Tools Platform (WTP) Project 0.7">
  <meta name="root" content="../../.." />
  <section name="" class="main">
    <description>
      Download WTP 0.7 <a href="http://download.eclipse.org/webtools/downloads/drops/R-0.7-200507290654/">here</a>.
    </description>
  </section>
  <section name="Scope and Architecture of Web Tools Platform Project" class="main">
    <description>
      The Eclipse Web Tools Platform Project will initially focus on infrastructure for tools used 
      to build applications for standards-based Web and Java runtime environments. (See diagram for 
      illustration.) Outside the project's scope, at least at first, will be support for vendor-specific 
      application architectures, such as ASP.Net and ColdFusion, or for extensions not backed by the JCP, 
      such as Apache Struts. This can be re-evaluated later according to user needs and contributor 
      availability and may lead to the creation of a new subproject.<br/>
      <br/>
      Fig 1. WTP Scope<br/>
      <img src="../../images/subprojects.gif"/><br/>
      <br/>
      The project will be further limited to providing infrastructure for tooling proper, in contrast to 
      infrastructure related to the application run-time. We will typically use a simple litmus test to 
      set the boundary between tooling and run-time. Application artifacts, once developed, have no execution 
      dependencies on the relevant tooling framework, while the converse would be true for run-time frameworks. 
      In keeping with our objective of maximizing vendor-neutrality, where multiple frameworks exist in the 
      market for a given functional domain, we will attempt to develop tooling based on a common abstraction 
      (or superset) to the extent feasible.<br/>
      <br/>
      The ultimate objective of the project is to support tooling that allows developers to produce applications 
      providing a high degree of extensibility and reuse with increasing development efficiency. The tooling 
      foundation the project will deliver will support these values by enforcing appropriate separations of 
      concern in application architecture, raising the level of technical abstraction in application development 
      and enabling repeatability in development processes. These values, however, will be achieved incrementally 
      over time. Early deliverables will focus on an extensible foundation supporting the most widely used Web 
      and Java standards and technologies.<br/>
      <br/>
      In addition, we expect the Web Tools Platform Project to produce functional requirements that are more 
      appropriately satisfied through the Eclipse Project or other Eclipse foundational subprojects. Areas in 
      which we might expect to see these elaborated requirements would be in working with components, or 
      supporting complex project layouts. In such case, the Web Tools Platform Project PMC will coordinate 
      the corresponding Project PMCs the design and implementation of the corresponding contribution. <br/>
      <br/>
      Fig 2.  WTP Architecture<br/>
      <img src="images/wtp-architecture.gif"/><br/>
    </description>
  </section>
  <section name="Extending Eclipse into the domain of J2EE Web Application Development" class="main">
    <description>
      While the WebTools Platform project itself does not provide server runtimes, it supports a variety 
      of 3<sup>rd</sup> party containers &amp; servers, as well as an extensibility model for additional server support.   
      The WTP allows developers to configure, start, stop, debug, and profile servlet containers like Apache 
      Tomcat 3.2, 4.0, 4.1, 5.0, 5.5, as well as popular J2EE servers like JBoss 3.2.3, JOnAS 4.1.4, Apache 
      Geronimo 1.0, BEA WebLogic 8.1, and IBM WebSphere 6.0. 
    </description>
    <doublesection name="Web Standard Tools (WST)" link="../../wst/main.html" icon="../../../images/Adarrow.gif">
      The Web Standard Tools project provides common infrastructure available to any Eclipse-based development 
      environment targeting multi-tier Web-enabled applications. Within scope are tools for the development of 
      three-tier (presentation, business and data logic) and server publication of corresponding system artifacts. 
      Outside of scope are tools for Java language or Web framework specific technology, which are left to other 
      projects like the J2EE Standard Tools project. Tools provided include editors, validators and document 
      generators for artifacts developed in a wide range of standard languages (for example, HTML/XHMTL, Web 
      services, XQueries, SQL, etc.) Supporting infrastructure comprises a specialized workbench supporting actions 
      such as publish, run, start and stop of Web application code across target server environments. Web are now 
      first class citizens with respect to the capabilities that Eclipse users expect.<br/>
      <br/>
      The Web Standard Tools Project includes server tools which extend the Eclipse platform with servers as 
      first-class execution environments. Server tools provide an extension point for generic servers to be added 
      to the workspace, and to be configured and controlled. For example, generic servers may be assigned port numbers,
      and may be started and stopped. The Web Standard Tools Project defines an extension for Web servers, which builds 
      on the generic server extension point, and includes exemplary adapters for popular commercial and Open Source 
      servers, e.g. Apache Tomcat. Server vendors are encouraged to develop adapters for their Web servers. The Web 
      Standard Tool Project also includes a TCP/IP Monitor server for debugging HTTP traffic, especially SOAP messages 
      generated by Web Services. The generic server extension point is intended to be used for other types of servers, 
      for example J2EE application servers, but these are outside the scope of the Web Standard Tools project.<br/>
      <br/>
      <ul>
        <li>Internet Tools
          <ul>
            <li>Proxy Settings, Browser, TCP/IP Monitor</li>
          </ul>
        </li>
        <li>WST Structured Source Editor Framework
          <ul>
            <li>Framework to simplify development of editors for XML-like formats</li>
            <li>DOM based</li>
            <li>Code assist</li>
            <li>Syntax highlighting</li>
            <li>Red squiggles</li>
            <li>Quick fixes</li>
          </ul>
        </li>
        <li>Web Tools
          <ul>
            <li>HTML source editor</li>
            <li>CSS source editor</li>
            <li>Javascript source editor</li>
          </ul>
        </li>
        <li>XML Tools
          <ul>
            <li>XML source editor</li>
            <li>XSD editor</li>
            <li>Graphical and source modes</li>
            <li>DTD source editor</li>
            <li>Code generators</li>
          </ul>
        </li>
        <li>Web Service Tools
          <ul>
            <li>WSDL Editor</li>
            <li>Graphical and source modes</li>
            <li>Integrated XSD editor</li>
            <li>Web Service Explorer</li>
            <li>Query and publish to UDDI</li>
            <li>Dynamically execute WSDL</li>
            <li>Web Service Wizard</li>
            <li>Extension points for code generation, deploy, test, etc.</li>
            <li>Web Service Interoperability (WS-I) organization Test Tools</li>
            <li>Validate WSDL and SOAP for WS-I compliance</li>
          </ul>
        </li>
        <li>Data Tools
          <ul>
            <li>Database server explorer</li>
            <li>SQL scrapbook</li>
            <li>Output view</li>
            <li>RDB and SQL models</li>
          </ul>
        </li>
      </ul>
    </doublesection>
    <doublesection name="J2EE Standard Tools (JST)" link="../../jst/main.html" icon="../../../images/Adarrow.gif">
      The initial scope of the J2EE Standard Tools project is to provide a basic Eclipse plug-in for developing 
      applications based on standards-based application servers, as well as a generic tooling infrastructure for 
      other Eclipse-based development products. Within scope is a workbench providing a framework for developing, 
      deploying, testing and debugging J2EE applications on JCP-compliant server environments, as well as an exemplary 
      implementation of a plug-in for at least one JSR-88 compliant J2EE Server. Included is a range of tools simplifying 
      development with J2EE APIs including EJB, Servlet, JSP, JCA, JDBC, JTA, JMS, JMX, JNDI, and Java Web Services. 
      This infrastructure is architected for extensibility for higher-level development constructs providing 
      architectural separations of concern and technical abstraction above the level of the J2EE specifications.<br/>
      <br/> 
      The J2EE Standard Tools Project builds on the Server Tools provided by the Web Standard Tools Project to provide 
      support for application servers, including both servlet engines and EJB containers. The scope of the J2EE Standard 
      Tools Project includes exemplary adapters for popular commercial and open source J2EE servers, e.g. Apache Tomcat, 
      Apache Geronimo, and ObjectWeb JOnAS. Server vendors are encouraged to develop adapters for their products. Support 
      of frameworks not covered by the J2EE specification (e.g., Struts, Hibernate, XMLC, JDO) are outside the scope of 
      this project, although such projects could find a home in an Eclipse Technology project.<br/>
      <br/>
      Although the scope of the Web and J2EE Standard Tools projects includes the development of exemplary adapters for 
      popular commercial and Open Source servers, these are not necessarily intended to be the definitive adapters. 
      Instead, they are intended to serve two purposes. First, they are intended to enable users to immediately use these 
      servers, although the adapters may not exploiting all the server features. Second, they are intended to serve as 
      examples to both commercial and Open Source developers who want to integrate servers into Eclipse. It is consistent 
      with the goals of this project that the exemplary adapters become superseded by more complete implementations provided 
      by third parties, both commercial and open source.<br/>
      <br/>
      <ul>
        <li>Common
          <ul>
            <li>J2EE Core Models
              <ul>
                <li>Natures and Builders</li>
                <li>J2EE Views and Navigators</li>
                <li>J2EE Models</li>
              </ul>
            </li>
            <li>J2EE Projects and Modules
              <ul>
                <li>Flexible Project Layout</li>
              </ul>
            </li>
            <li>Servlet Tools
              <ul>
                <li>Creation Wizard</li>
                <li>Run as</li>
              </ul>
            </li>
            <li>JSP Tools
              <ul>
                <li>JSP Editor Extends HTML editor</li>
                <li>JSR-45 compliant debugging</li>
              </ul>
            </li>
          </ul>
        </li>
        <li>Advanced
          <ul>
            <li>Annotation Support
              <ul>
                <li>EJB annotations, WebServices annotations</li>
              </ul>
            </li>
            <li>EJB
              <ul>
                <li>Wizard, xDoclet support</li>
               </ul>
             </li>
             <li>Java Web Services
               <ul>
                 <li>Web Service Wizard</li>
                 <li>Navigator integration</li>
               </ul>
             </li> 
           </ul>
         </li>
       </ul>
      </doublesection>
    </section>
    <section name="Platform API for tool developers" class="main">
      <description>
        Although one of the main goals of the WTP is to provide a platform quality API, the WTP 
        project management committee has decided that this API will not be available in the WTP 
        0.7 release. Many of the extension points are currently available as provisional API and, 
        as part of the Eclipse process, these API can only become final once clients review, 
        comment, and use them.<br/>
        <br/>
        The goals of the WTP API are as follows:
        <ol>
          <li>Provide a stable API for plug-in developers to create value-add tools</li>
          <li>Achieve binary compatibility from release to release
            <ol>
              <li>All API-compliant plug-ins MUST run without recompilation on new versions of the WTP API</li>
              <li>Allows Eclipse and WTP to be upgraded without breakage of existing tools</li>
            </ol>
          </li>
          <li>Collaboration with the Eclipse Platform API team – already underway with the concept of “Component”
            <ol>
              <li>component.xml descriptor (like plugin.xml)</li>
              <li>API scanning tools to detect API violation and breakage</li>
              <li>Future enhancement of JDT to enforce API compliance (e.g. restricting code assist to allowed interfaces)</li>
            </ol>
          </li>
        </ol>
      </description>
    </section>
</sections>