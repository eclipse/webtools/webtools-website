<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="../../../wtp.xsl"?>
<html>
<head>
  <meta name="root" content="../../../../" />
  <title>Web Services Required Jars Download Instructions using Ant (WTP 0.7 M3 only)</title>
</head>
<body>

<h1>Web Services Required Jars Download Instructions</h1>
<p>

<b>by Keith Chong</b><br/>
February 9, 2005 (updated June 16, 2005)<br/>
<br/>
<p><b>
These instructions apply only to
<a href="http://download.eclipse.org/webtools/downloads/drops/S-1.0M3-200502260707/">WTP 0.7 M3</a>.
Do not follow them for WTP 0.7 M4 and beyond.
</b></p>
The Web Services plugins require jars that are external to the WTP project
and are not found in the current WTP builds. In order for these plugins
to work properly, you must download (all!) of the zips and extract their jars
to the appropriate location in the plugins.
<br/>
<ol>
<li>
Before downloading, please read the licensing information that is associated
with each downloadable file.  The Apache License can be found at
<a href="http://www.apache.org/licenses/">http://www.apache.org/licenses</a>
</li>

<li>
Create a temporary folder, like C:/temp/jars, which will hold all the jars.
We'll refer to this folder as <i>downloadPath</i> from now on.</li>

<li>
Download the following items and put them in <i>downloadPath</i>:</li>
<br/>
<br/>
<table BORDER="1" COLS="3" WIDTH="95%" >
<tr><td><b>Item</b></td><td><b>File</b></td><td><b>Link</b></td></tr>
<tr><td>Apache Axis</td><td>axis-1_1.zip</td><td><a href="http://xml.apache.org/dist/axis/1_1/axis-1_1.zip">http://xml.apache.org/dist/axis/1_1/axis-1_1.zip</a>.</td></tr>
<tr><td>Tomcat</td><td>jakarta-tomcat-4.1.31.zip</td><td><a href="http://www.apache.org/dist/jakarta/tomcat-4/v4.1.31/bin/jakarta-tomcat-4.1.31.zip">http://www.apache.org/dist/jakarta/tomcat-4/v4.1.31/bin/jakarta-tomcat-4.1.31.zip</a></td></tr>
<tr><td>JavaBeans Activation Framework</td><td>jaf-1_0_2-upd.zip</td><td><a href="http://java.sun.com/products/javabeans/glasgow/jaf.html#download" target="_top">http://java.sun.com/products/javabeans/glasgow/jaf.html#download</a></td></tr>
<tr><td>JavaMail</td><td>javamail-1_3_2.zip</td><td><a href="http://java.sun.com/products/javamail/downloads/index.html" target="_top">http://java.sun.com/products/javamail/downloads/index.html</a></td></tr>
<tr><td>Apache Soap</td><td>soap-bin-2.3.1.zip</td><td><a href="http://www.apache.org/dist/ws/soap/version-2.3.1/soap-bin-2.3.1.zip">http://www.apache.org/dist/ws/soap/version-2.3.1/soap-bin-2.3.1.zip</a></td></tr>
<tr><td>Web Services Inspection Language for Java</td><td>wsil4j.jar</td><td><a href="http://awwebx04.alphaworks.ibm.com/wsil4j/wsil4j.jar">http://awwebx04.alphaworks.ibm.com/wsil4j/wsil4j.jar</a></td></tr>
<tr><td>UDDI - Universal Description, Discovery and Integration</td><td>uddi4j-bin-2_0_2.zip</td><td><a href="http://prdownloads.sourceforge.net/uddi4j/uddi4j-bin-2_0_2.zip?download">http://prdownloads.sourceforge.net/uddi4j/uddi4j-bin-2_0_2.zip?download</a></td></tr>
<tr><td>WSDL4J</td><td>wsdl4j-bin-1.4.zip</td><td><a href="http://prdownloads.sourceforge.net/wsdl4j/wsdl4j-bin-1.4.zip?download" target="_top">http://prdownloads.sourceforge.net/wsdl4j/wsdl4j-bin-1.4.zip?download</a></td></tr>
</table>
<br/>
<br/>

<li>
Now that you've successfully obtained all the zips in the <i>downloadPath</i>
folder, we need to extract the jars from those zips and put them into the plugins that require them. 
You can do this manually, or you can run an ANT script that would do this for you
automatically.

<ul>
<br/>
<li>  
<b>ANT Script</b> - the ANT script 
<a href="http://dev.eclipse.org/viewcvs/index.cgi/~checkout~/org.eclipse.wtp.releng/fetchVendorContent.xml?content-type=text/xml&amp;cvsroot=WebTools_Project">fetchVendorContent.xml</a>
is found in the Web Tools CVS module org.eclipse.wtp.releng.
This is how you run the script: 
<P/>
ant -DlocalDownloads=<i>downloadPath</i> -DbuildDirectory=&lt;Eclipse plugins directory&gt; -f fetchVendorContent.xml<br/>
<br/>
</li>

<li><b>Manual</b> - Otherwise here is a table that shows you where you should copy the jars.  IMPORTANT, copy only the jars to the
destination folder, AND ignore the directory structure of the source jar.
<br/>
<p/>
<table BORDER="1" COLS="3" WIDTH="90%" >
<tr><td><b>Zip</b></td><td><b>Extracted Jars From Zip</b></td><td><b>Copy jar(s) to this plugin folder location</b></td></tr>
<tr>
<td>axis-1.1.zip</td>
<td>axis-1_1/lib/axis.jar
<br/>axis-1_1/lib/axis-ant.jar
<br/>axis-1_1/lib/commons-discovery.jar
<br/>axis-1_1/lib/commons-logging.jar
<br/>axis-1_1/lib/jaxrpc.jar
<br/>axis-1_1/lib/log4j-1.2.8.jar
<br/>axis-1_1/lib/saaj.jar
<br/>axis-1_1/lib/wsdl4j.jar
</td>
<td>&lt;wtpInstallDirectory>/eclipse/plugins/org.eclipse.wst.ws.apache.axis_1.0.0<b>/lib/</b></td>
</tr>

<tr>
<td>jakarta-tomcat-4.1.31.zip</td>
<td>jakarta-tomcat-4.1.31/common/lib/servlet.jar</td>
<td>&lt;wtpInstallDirectory>/eclipse/plugins/org.eclipse.wst.ws.apache.axis_1.0.0<b>/lib/</b></td>
</tr>

<tr>
<td>soap-bin-2.3.1.zip</td>
<td>soap-2_3_1/lib/soap.jar</td>
<td>&lt;wtpInstallDirectory>/eclipse/plugins/org.eclipse.wst.ws.apache.soap_1.0.0/<b>lib/</b></td>
</tr>

<tr>
<td>javamail-1_3_2.zip</td>
<td>javamail-1.3.2/mail.jar</td>
<td>&lt;wtpInstallDirectory>/eclipse/plugins/org.eclipse.wst.ws.apache.soap_1.0.0/<b>lib/</b></td>
</tr>

<tr>
<td>jaf-1_0_2-upd.zip</td>
<td>jaf-1.0.2/activation.jar</td>
<td>&lt;wtpInstallDirectory>/eclipse/plugins/org.eclipse.wst.ws.apache.soap_1.0.0/<b>lib/</b></td>
</tr>

<tr>
<td>(Jar already downloaded from step 3)</td>
<td>wsil4j.jar</td>
<td>&lt;wtpInstallDirectory>/eclipse/plugins/org.eclipse.wst.ws.apache.wsil_1.0.0<b>/lib/</b></td>
</tr>

<tr>
<td>uddi4j-bin-2_0_2.zip</td>
<td>uddi4j/lib/uddi4j.jar</td>

<td>&lt;wtpInstallDirectory>/eclipse/plugins/org.eclipse.wst.ws.uddi4j_1.0.0/<b>/lib/</b></td>
</tr>

<tr>
<td>wsdl4j-bin-1.4.zip</td>
<td>wsdl4j-1_4/lib/wsdl4j.jar
<br/>wsdl4j-1_4/lib/qname.jar</td>

<td>&lt;wtpInstallDirectory>/eclipse/plugins/org.eclipse.wst.wsdl_1.0.0/<b>lib/</b></td>
</tr>
</table>
</li>
</ul>
</li>

<br/>
<li>
If the Eclipse IDE is already running, close it down</li>

<li>
Restart Eclipse with the <b>-clean</b> argument for the changes to take
effect.</li>
</ol>
</p>
</body>
</html>
