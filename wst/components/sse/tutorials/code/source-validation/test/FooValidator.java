package sourcevalidator.test;

import java.util.Locale;

import org.eclipse.core.resources.IResource;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.wst.sse.ui.internal.reconcile.validator.ISourceValidator;
import org.eclipse.wst.validation.internal.core.Message;
import org.eclipse.wst.validation.internal.core.ValidationException;
import org.eclipse.wst.validation.internal.provisional.core.IMessage;
import org.eclipse.wst.validation.internal.provisional.core.IReporter;
import org.eclipse.wst.validation.internal.provisional.core.IValidationContext;
import org.eclipse.wst.validation.internal.provisional.core.IValidator;

public class FooValidator implements ISourceValidator, IValidator {
	
	protected class LocalizedMessage extends Message {

		private String _message = null;

		public LocalizedMessage(int severity, String messageText) {
			this(severity, messageText, null);
		}

		public LocalizedMessage(int severity, String messageText, IResource targetObject) {
			this(severity, messageText, (Object) targetObject);
		}

		public LocalizedMessage(int severity, String messageText, Object targetObject) {
			super(null, severity, null);
			setLocalizedMessage(messageText);
			setTargetObject(targetObject);
		}

		public void setLocalizedMessage(String message) {
			_message = message;
		}

		public String getLocalizedMessage() {
			return _message;
		}

		public String getText() {
			return getLocalizedMessage();
		}

		public String getText(ClassLoader cl) {
			return getLocalizedMessage();
		}

		public String getText(Locale l) {
			return getLocalizedMessage();
		}

		public String getText(Locale l, ClassLoader cl) {
			return getLocalizedMessage();
		}
	}
	
	IDocument fDocument = null;
	public void connect(IDocument document) {
		fDocument = document;
	}

	public void disconnect(IDocument document) {
		fDocument = null;
	}
	
	/**
	 * implement ISourceValidator
	 */
	public void validate(IRegion dirtyRegion, IValidationContext helper, IReporter reporter) {
		// look for foo
		int start = dirtyRegion.getOffset();
		int length = dirtyRegion.getLength();
		String dirtyText = fDocument.get().substring(start, start + length);
		
		int fooIndex = dirtyText.indexOf("foo");
		if(fooIndex != -1) {
			IMessage m = new LocalizedMessage(IMessage.HIGH_SEVERITY, "found foo");
			m.setOffset(start + fooIndex);
			m.setLength("foo".length());
			try {
				m.setLineNo(fDocument.getLineOfOffset(start + fooIndex) + 1);
			}
			catch (BadLocationException e) {
				m.setLineNo(-1);
			}
			reporter.addMessage(this, m);
		}
	}

	public void cleanup(IReporter reporter) {
		// TODO Auto-generated method stub
		
	}

	public void validate(IValidationContext helper, IReporter reporter) throws ValidationException {
		// TODO Auto-generated method stub
		
	} 
}
