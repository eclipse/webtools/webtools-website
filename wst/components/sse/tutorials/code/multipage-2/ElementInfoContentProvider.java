package org.eclipse.wst.sse.examples.multipage.tabletree;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class ElementInfoContentProvider implements IStructuredContentProvider {
	
	Hashtable fTable = null;
	public ElementInfoContentProvider() {
		fTable = new Hashtable();
	}
	public ElementInfoContentProvider(Hashtable table) {
		super();
		fTable = table;
	}
    /**
     * @see IStructuredContentProvider#getElements(Object inputElement)
     */
	public Object[] getElements(Object inputElement) {
		List elements = new ArrayList();
		Object[] keys = fTable.keySet().toArray();
		for (int i = 0; i < keys.length; i++) {
			Integer count = (Integer)fTable.get(keys[i]);
			elements.add(new ElementCountInfo((String)keys[i], count.intValue()));
		}
		return elements.toArray();
	}

	public void dispose() {
		// 
	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		fTable = (Hashtable)newInput;
	}
}
