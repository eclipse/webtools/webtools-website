package org.eclipse.wst.sse.examples.multipage.tabletree;

import java.util.Hashtable;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.wst.sse.core.internal.provisional.text.IStructuredDocument;
import org.eclipse.wst.sse.core.internal.provisional.text.IStructuredDocumentRegion;
import org.eclipse.wst.xml.core.internal.regions.DOMRegionContext;

public class ElementCountTableViewer extends TableViewer {
	
	private Hashtable fHash = new Hashtable();
	private IStructuredDocument fStructuredDoc = null;
	
	public ElementCountTableViewer(Composite parent, int style) {
		super(parent, style);
		
		setContentProvider(new ElementInfoContentProvider());
		setLabelProvider(new ElementCountLabelProvider());
		
		getTable().setHeaderVisible(true);
		getTable().setLinesVisible(true);
	
		String[] columns = new String[]{"element", "occurrences"};
		createTableColumns(columns);

		setInput(fHash);
	}

	private void createTableColumns(String[] columns) {
		TableLayout tlayout = new TableLayout();
		CellEditor[] cellEditors = new CellEditor[columns.length];
		for (int i = 0; i < columns.length; i++) {
			tlayout.addColumnData(new ColumnWeightData(1));
			TableColumn tc = new TableColumn(getTable(), SWT.NONE);
			tc.setText(columns[i]);
			tc.setResizable(true);
			tc.setWidth(Display.getCurrent().getBounds().width / 14);
		}
		setCellEditors(cellEditors);
		setColumnProperties(columns);
	}
	
	public void addElement(String element) {
		if(fHash.get(element) != null) {
			Integer count = (Integer)fHash.get(element);
			fHash.put(element, new Integer(count.intValue()+1));
		}
		else {
			fHash.put(element, new Integer(1));
		}
	}
	
	public void reset() {
		fHash.clear();
		refresh();
	}
	
	public void setDocument(IDocument doc) {
		
		if(!(doc instanceof IStructuredDocument))
			return;
		
		reset();
		
		fStructuredDoc = (IStructuredDocument)doc;
		
		// *** would have expected an array...
		//IStructuredDocumentRegionList regions = fStructuredDoc.getRegionList();
		IStructuredDocumentRegion[] regions = fStructuredDoc.getStructuredDocumentRegions();
		for(int i=0; i<regions.length; i++) {
			// *** are "types" for sdRegion going to be stable?
			//     do we need a new type @ the structured document region level?
			if(regions[i].getType() != DOMRegionContext.XML_CONTENT)
				addElement(regions[i].getFullText());
		}
		setInput(fHash);
	}
}
