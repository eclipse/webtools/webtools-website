PredefinedDataTypes are contained by value, ie the column owns an instance of the data type.  A Column must have one of, but not both, a containedType or a referencedType. 

Setting the containedType on a column will remove any referencedType, and vice versa.

It is recommended to not set the containedType or referencedType explicitly.  These relationships are managed by the column.  To set the type for a column, simply call setType(DataType newType) on a Column.
