Since there are many languages, rather than constrain ourselves to an enumerated list which is difficult to subclass in the EMF environment, we have chosen to use strings.

JAVA
SQL
PERL
C
ADA
COBOL
FORTRAN
MUMPS
PASCAL
PLI
and so on...
