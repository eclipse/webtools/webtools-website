From 5WD-02-Foundation-2002-12
4.27 SQL-invoked routines

An SQL-invoked routine is either deterministic or possibly non-deterministic. An SQL-invoked function that
is deterministic always returns the identical return value for a given list of SQL argument values. An SQLinvoked
procedure that is deterministic always returns the identical values in its output and inout SQL parameters
for a given list of SQL argument values. An SQL-invoked routine is possibly non-deterministic if it might
produce nonidentical results when invoked with the identical list of SQL argument values.
