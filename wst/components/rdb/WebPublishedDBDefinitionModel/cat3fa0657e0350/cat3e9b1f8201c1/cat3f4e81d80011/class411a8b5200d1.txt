Method is a specialization of Function to indicate the function was created in support of the user-defined type (UDT). 

From 5WD-02-Foundation-2002-12 
4.23 SQL-invoked routines 

- If the SQL-invoked routine is an SQL-invoked method, then an indication of the user-defined type whose descriptor contains the corresponding method specification descriptor
