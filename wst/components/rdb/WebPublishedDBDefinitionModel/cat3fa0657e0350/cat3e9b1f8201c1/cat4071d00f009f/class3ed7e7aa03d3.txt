The literals of this enumeration are used to indicate whether the actionStatement of a Trigger is to be executed once for each affected row, in the case of a row-level trigger, or once for the whole triggering INSERT , DELETE , MERGE , or UPDATE
statement, in the case of a statement-level trigger.

