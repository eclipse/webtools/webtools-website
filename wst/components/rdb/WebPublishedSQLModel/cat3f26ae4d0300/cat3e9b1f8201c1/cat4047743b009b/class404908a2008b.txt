A single statement of the following types (from section 13.5):
<SQL executable statement> ::=
    <SQL control statement>
  | <SQL data statement>
  | <SQL schema statement>
  | <SQL transaction statement>
  | <SQL connection statement>
  | <SQL session statement>
  | <SQL diagnostics statement>
  | <SQL dynamic statement>

