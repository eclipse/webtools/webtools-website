<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="../../../wtp.xsl"?>
<html>
<head>
  <meta name="root" content="../../../../" />
  <title>Run on Server</title>
</head>
<body>

<h1>Run on Server</h1>

<h2>Overview</h2>
<p>
This document provides information on the Run > Run on Server menu item,
including how it works, and how to make it appear or disappear on a given
object.
</p>

<h2>Run/Debug menus</h2>
<p>
The Run, Debug, and Profile menu items are contributed by the Eclipse debug
component. These menus appear on all objects in the UI that are adaptable to
org.eclipse.debug.ui.actions.ILaunchable. Enabling these menus (by adapting
objects to ILaunchable) is a prerequisite to enabling Run on Server.
</p>
<p>
If the Run menu appears on something that is not runnable, one solution is
to try to remove the ILaunchable adapter to remove the menu entirely. 
</p>


<h2>Run on Server Submenu</h2>
<p>
To determine when the Run on Server submenu appears in the Run menu, the
org.eclipse.wst.server.ui.moduleArtifactAdapters extension point is used.
This extension point provides an enablement expression that allows other
plugins to enable the Run on Server submenu for specific objects.
The Run on Server menu item appears on all objects that are accepted by
at least one moduleArtifactAdapters expression.
</p>
<p>
Once the Run on Server menu item is selected, the selected object is
adapted to IModuleArtifact. If the object cannot be adapted, an error
message will be displayed (this should never happen unless an external
plugin provides enablement but not adapting for an object). If the object
can be adapted, Run on Server will proceed to try to find an available
server to run on.
</p>


<h2>Tracing Run on Server Problems</h2>
<p>
The most common problems with Run on Server are it not appearing
on objects that should be runnable, or appearing on objects that aren't
runnable:
</p>

<h3>Run/Debug menu not appearing</h3>
<p>
This problem occurs when the selected object cannot be adapted (either
directly or via an adapter factory) to ILaunchable. The owner of the
object should provide an adapter to make the menu appear.
</p>

<h3>Run/Debug menu appearing on something that's not runnable</h3>
<p>
This problem occurs when the selected object is incorrectly adaptable to
ILaunchable. The owner of the object should remove the ILaunchable interface
or the adapter factory that supports the object.
</p>

<h3>Run on Server not appearing</h3>
<p>
Is Run on Server not appearing on an object that you think is runnable?
Contact the development team responsible for the artifact (e.g. J2EE team
for Servlet) and ask them to add support.
</p>

<h3>Run on Server appearing on something that's not runnable</h3>
<p>
This problem can only occur when some plugin is using the moduleArtifactAdapter
extension point to enable for an object that it shouldn't. Use the steps below
to figure out which plugin is causing the problem and open a defect up against
them.
</p>
<p>
Using a development environment, turn on tracing for org.eclipse.wst.server.ui
and launch a workbench. When you right click on the object and hover over the
Run menu, the following line of trace will be output as Run on Server appears:
"Run On Server for XX is enabled by YY." XX is the object that you have selected,
and YY is the extension point id for the plugin that is doing the enablement.
Track down the id and open a defect against this component.
</p>

<h3>Run on Server appears but fails on execution</h3>
<p>
This problem can only occur when a plugin provides enablement for an object
but does not allow the object to adapt to IModuleArtifact. Use the tracing
steps above to identify the plugin and open a defect to get the object adapted.
</p>


<h2>Appendix</h2>
<p>
The appendix contains some useful hints and tips.
</p>

<h3>Enablement Expression Performance</h3>
<p>
Enablement expressions allow you to create arbitrarily complex expressions using ands, ors,
nots, and Java code. For performance reasons, it's always better to use the expression
support instead of using Java code, which is slower or may be in plugins that aren't loaded
yet. However, simple Java tests can provide much better flexibility and do not need to
cause large delays.
</p>
<p>
The best practice is to use enablement expressions as much as possible to filter down the
number and types of objects that are applicable. As a final check, a property tester can
then be used. However, it is still important to use expressions first, even if you know
you'll be using a property tester for the last check - this will keep the performance up
for all the objects that are filtered out.
</p>

<h3>Adding a Property Tester</h3>
<p>
To add a property tester to an enablement expression, try using the following example.
In your plugin.xml, add the propertyTesters extension point:
<pre>
&lt;extension point="org.eclipse.core.expressions.propertyTesters"&gt;
   &lt;propertyTester
      id="org.eclipse.myplugin.propertyTester"
      namespace="org.eclipse.myplugin"
      properties="someProperty"
      type="java.lang.Object"
      class="org.eclipse.myplugin.internal.MyPropertyTester"&gt;
   &lt;/propertyTester&gt;
&lt;/extension>
</pre>

Inside your plugin, add and implement the property tester code: 
<pre>
package org.eclipse.myplugin.internal;
import org.eclipse.core.expressions.PropertyTester;

public class MyPropertyTester extends PropertyTester {
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		// cast receiver and/or expectedValue
		// if (xx) // check some dynamic property
			return true;
		// else
		//return false;
	}
}
</pre>

You can now use a property expression within the enablement:
<pre>&lt;test property="org.eclipse.myplugin.someProperty" value="true"/&gt;</pre>

</p>

</body>
</html>