<?php                                                                                                                                                                                                                                           require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App    = new App();    $Nav    = new Nav();    $Menu   = new Menu();           include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
$pageTitle = "Common Tools Snippets Subcomponent Overview";
$pageKeywords = "Eclipse WTP webtools";
$pageAuthor ="Ugur Yildirim @ Eteration A.S.";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
# $Nav->addNavSeparator("My Page Links",        "downloads.php");
# $Nav->addCustomNav("My Link", "mypage.php", "_self", 3);
# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank", 3);

# End: page-specific settings

# Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML
$wtpTopButtons
<link type="text/css" href="/webtools/default_style.css" rel="stylesheet">
<div id="midcolumn" style="width: 95% !important; margin-left: 1.5em;">
<span style="float:right;"><a href="/webtools/common/">Back</a></span>
<table border="0" cellpadding="2" cellspacing="5" width="100%">
<tbody>
<tr>
<td align="left" width="100%"><font class="indextop">Snippets subcomponent</font>
<br>
<font class="indexsub">WST Common Snippets subcomponent Overview</font></td>
</tr>
</tbody>
</table>
<table border="0" cellpadding="2" cellspacing="5" width="100%">
<col width="16">
<col width="*">
<tbody>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<p>Last modified April 13, 2005</p>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<p>
			
<i>
				This document is a work in progress. It is an attempt to
				capture the key concepts of the WTP Snippets
				subcomponent. Note that the concepts presented here may
				differ from what is found in the currently released
				code. This document describes the architecture, or "end
				goal", of the Snippets subcomponent. Once finalized, the
				intent is to bring the code and its specifications into
				line with this document.
			</i>
		
</p>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<p></p>
</td>
</tr>
<tr>
<td align="right" valign="top"><img src="../../../../../../images/Adarrow.gif" border="0" height="16" width="16"></td><td>Core Concepts</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<p>
			The Snippets subcomponent provides a simple way for users to
			drag and drop chunks of text into source editors. The
			snippets model refers to these insertable chunks as "Items"
			and their containers as "Category"s. The actual text being
			inserted can be affected by input from the user during the
			"drop" part of the drag-and-drop operation and users are
			free to create categories and items for themselves. Items
			contributed from plug-ins may also provide customized
			insertion behavior, for example substituting wizards and
			custom dialogs for the defaults that are provided. The user
			interacts with graphical representations of the snippet
			items directly, but any exposed APIs do not make specific
			references to the current UI implementation.
		</p>
</td>
</tr>
<tr>
<td align="right" valign="top"><img src="../../../../../../images/Adarrow.gif" border="0" height="16" width="16"></td><td>User Interface</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<p>
			The Snippets view is based on the GEF PaletteViewer,
			primarily because the Snippets view is a logical counterpart
			to the built-in palette found in many graphical editors.
			Snippet Items and Categories are shown as
			PaletteTemplateEntrys and PaletteDrawers, although this
			detail is hidden by the ISnippetItem and ISnippetCategory
			interfaces.
		</p>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<p>
			The standard PaletteViewer user settings for appearance are
			available and supported. The user is also given the ability
			to show and hide individual PaletteDrawers as they wish.
			While the user has full editing abiities for
			PaletteTemplateEntrys and PaletteDrawers they created, those
			originating from plug-in contributions are otherwise
			unmodifiable. The user or a plug-in contributor may choose
			for a drawer to have its visibility automatically controlled
			according to the active editor part's input's platform
			content type.
		</p>
</td>
</tr>
<tr>
<td align="right" valign="top"><img src="../../../../../../images/Adarrow.gif" border="0" height="16" width="16"></td><td>Extension Points</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<h4>SnippetContributions</h4>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<p>
			The org.eclipse.wst.common.snippets.SnippetContributions
			extension point allows other plugins to provide new
			categories and items (made visible as entries and drawers)
			into the Snippets view. One keen notion to keep in mind is
			that contributions provide only the attributes for a snippet
			item or category, not its implementation. Similarly, the
			insertion class is contributed (as this is a key part of a
			snippet item's lifecycle) instead of insertion being
			directly implemented on a provided snippet item
			implementation. This arrangement was conceived to remove the
			need and ability for contributions to maintain any
			information between workbench sessions--the recording of a
			contributed drawer's visibility, for instance.
		</p>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<p>
			For more information, see the reference document for this
			extension point.
		</p>
</td>
</tr>
</tbody>
</table>
</div>
EOHTML;


# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>