<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="../wtp.xsl"?>
<html>
<head>
  <title>the web standard tools subproject</title>
</head>
<body>

<h1>wst</h1>

<h2>Project Vision</h2>
<p>
The project vision is to extend eclipse
platform with support for building multi-tier Web applications. The
project will grow the community of eclipse users, and grow the
community of developers that create Web applications based on Open
Standards and Technologies. In this way, we will help make eclipse the
industry standard tool integration platform for development based on
Open Standards and Technologies. The project must preserve the eclipse
value proposition by providing integration, ease-of-use, function, and
"coolness". Web artifacts must be first class citizens with respect to
the capabilities that eclipse users expect. Servers must be first class
execution environments, including both Open and Commercial
implementations, therefore encourage support of eclipse by server
vendors.
</p>

<h3><a href="components.html">WST Components</a></h3>
<p>
The proposed components and component leads.
</p>

<h2>Project Context</h2>
<p>
Support of Web standards in the scope of the Web Standard
Tools subproject includes languages and specifications published by
open-standard bodies (e.g. W3C, IETF, ECMA, OASIS, WS-I, ANSI) that are
commonly used by Web-centric applications (e.g. HTML, CSS, JavaScript,
XML, XSD, XSLT, SVG, SOAP, WSDL, UDDI, SQL).
</p>
<p>
This subproject will provide support for integrating Web
servers into
Eclipse as first-class execution environments for Web applications.
This support will include the ability to configure Web servers and
assocaite them with Web projects. Examplar tools for supporting popular
Open Source Web servers, such as Apache, are in scope. 
</p>
<p>
Support of <a class="external" href="http://www.jcp.org/"
 target="_blank">JCP standards</a> are explicitly outside the scope of
this project and should be covered by the <a class="external"
 href="http://www.eclipse.org/jdt" target="_top">Java Development Tools
(JDT) project</a> or by the <a class="wikipage" href="../jst/main.html">J2EE
Standard Tools (JST) subproject</a>.
</p>
<p>
The subproject will provide tools that directly support the
base standards and that are also extensible. For example, the HTML
editor will be extensible to support HTML-based template languages such
as PHP and JSP. While a JSP editor is in the scope of the JST
subproject, exemplar tools for popular Open Source languages such as
PHP are in scope.
</p>
<p>
The goal of this project is to provide both tools for
Web application developers and a platform for tool developers.
</p>

<h3>Some Architectural Principles</h3>
<ul>
        <li>Seamlessly extend the eclipse user experience </li>
        <li>Accommodate common project directory layouts </li>
        <ul>
          <li>Avoid need for migration </li>
        </ul>
        <li>Apply capabilities orthogonally to source artifacts: </li>
        <ul>
          <li>Syntax Coloring </li>
          <li>Code Assist </li>
          <li>Quick Fixes </li>
          <li>Refactoring </li>
          <li>Etc. </li>
        </ul>
        <li>Extend the Run command to Web artifacts, e.g. HTML </li>
        <li>Enable hybrid source editing: </li>
        <ul>
          <li>HTML in Java </li>
          <li>SQL in Java </li>
          <li>Etc. </li>
        </ul>
        <li>Enable extension of editors with value-add </li>
        <ul>
          <li>Graphical views </li>
          <li>Form based views </li>
        </ul>
      </ul>

      <h3>Some Use Cases</h3>
      <ul>
        <li>Develop and publish a static HTML site </li>
        <li>Develop Web pages based on JavaScript and CSS </li>
        <li>Deploy an applet on a given HTTP server </li>
        <li>Develop SQL statements and generate static Web pages from
database queries </li>
        <li>Develop XSD and XSLT for XML based Web pages and Web
services </li>
        <li>Develop and publish WSDL schema on UDDI registries </li>
        <li>Explore UDDI registries and dynamically test Web services
via WSDL </li>
        <li>Test Web services for WS-I compliance </li>
      </ul>
      
<h2>Goals for Project Structure and Web Core Model</h2>

      <p>These structures will provide common facilities required by
most Web tool builders. A common and flexible project model is required
as well as a common basis for web artifacts and editors. Our goals are:
      </p>
      <ul>
        <li>Divide and Conquer: Lots of components, so divide the work
using the classical principle of hierarchy </li>
        <li>Layer Build Dependencies: J2EE Standards builds on the Web
Standards, the Extensions build on the Standards </li>
        <li>The structure of software reflects the structure of the
organization that built it: We are building a platform and need to
validate our API. </li>
      </ul>

<h2>Web Core Model</h2>

<p>
This is a set of frameworks, and models, which provide the
necessary abstractions and APIs to work and develop with Web Tools, and
to build IDEs and tools to support these activities. WCM is not just an
adaptation of language specification, but it will focus on the
additional requirements of developers and tool builders. WCM address at
a minimum the following areas:
</p>
      <h3>Project Model</h3>
      <p>Development of Web applications is done in a variety of ways;
It can be divided across multiple related projects, or it can be
factored into one project that can host multiple artifacts. The core
Web project model will extend the eclipse project model and will have
intelligence to recognize and work with Web artifacts, and support the
build activities related to these artifacts. </p>

      <h3>Editor Model</h3>
      <p>The Editor Model will offer developers a set of APIs to access
Web models, participate in build and refactoring activities, and allow
them to easily create various text, and graphical editors. Support for
a Content based selection of Editors will be integrated into the Editor
Model. This model will encompass editors in a broad sense, i.e.
editors, validators and document generators for a wide range of
standard languages corresponding to Web Artifacts taken into account. </p>

      <h3>Web Artifacts</h3>
      <p>These models will represent Web standard models. They will at
minimum include or reference existing models for: </p>
      <ul>
        <li>XML family languages (XML itself, HTML, XHTML, WML, XSD,
DTD, XSLT, SVG, ...) </li>
        <li>JavaScript/ECMAScript </li>
        <li>CSS </li>
        <li>Web Services languages (SOAP, WSDL, UDDI, WSIL, ...) </li>
        <li>Database languages (SQL, ...). </li>
        <li>Other models: DDL, ... </li>
      </ul>

      <h3>Server Models</h3>
      <p>Servers are first class execution environments, they provide
the runtime environment, where the web artifacts are tested, deployed
and executed. Server models must be flexible to accommodate open and
commercial implementations. </p>
      <ul>
        <li>Support different applications servers and server versions </li>
        <li>Start, stop and restart servers </li>
        <li>Run server processes in the Eclipse debugger </li>
        <li>Package, deploy, remove, import, export, accessing/testing
modules </li>
        <li>Extensible framework for server administration </li>
      </ul>

<h2>Web Standard Tools</h2>
 
<p>
Web Standard Tools will provide extensible tools for the
development of Web Applications and support development, testing and
debugging with various servers. The following tools will be made
available:
</p>
      <ul>
        <li>Server tools, Internet preferences, TCP/IP monitor </li>
        <li>HTML (fragments), XML, CSS, JavaScript source editors </li>
        <li>XML/DTD/XSD utilities </li>
        <li>WSDL/XSD graphical editors, Web service UDDI/WSDL Explorer </li>
        <li>SQL Query graphical editor </li>
        <li>Associated natures, builders, validators and EMF models</li>
      </ul>
</body>
</html>
