<?php  				
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	

$App = new App();	
$Nav = new Nav();	
$Menu = new Menu();		

include($App->getProjectCommon());

	#*****************************************************************************
	#
	# Copyright (c) 2010, 2014 IBM Corporation and others.
 	# All rights reserved. This program and the accompanying materials
 	# are made available under the terms of the Eclipse Public License v1.0
 	# which accompanies this distribution, and is available at
 	# http://www.eclipse.org/legal/epl-v10.html
 	# Contributors:
	#     IBM Corporation - initial implementation
	#
	# Author: 		Ant Team
	# Date:			August 30, 2010
	#
	# Description: This is the new main page for the JSDT Team website
	#
	#****************************************************************************
	
	$pageTitle 		= "JavaScript Development Tools (JSDT)";
	$pageKeywords	= "javascript, JavaScript, JSDT, platform, debugging, debugger, jsdt, breakpoints, about, content, epl, compiler, compile, parse, parser";
	$pageAuthor		= "JSDT Team";
	
	include("_sideCommon.php");
	
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
		<div class="homeitem3col">
			<h3>Project Overview</h3>
			<p>
				The JavaScript Development Tools (JSDT) provide plug-ins that implement an IDE
				supporting the development of JavaScript applications and JavaScript within
				web applications. It adds a JavaScript project type and perspective to the
				Eclipse Workbench as well as a number of views, editors, wizards, and
				builders.
			</p>
		</div>
		<div class="homeitem3col">
			<h3>JSDT Architecture</h3>
			<p>
				JSDT has undergone a major refactoring in Neon, not only changing the core platform, but also adding new features like Bower, npm, JSON editor, ECMAScript 2015 (ES6) parser, Node.js support and JavaScript Build Tools (Gulp / Grunt)
				<ol>
					<li><strong><a href="https://wiki.eclipse.org/JSDT/Architecture">JSDT Architecture</a></strong></li>
				</ol>
			</p>
		</div>
		<div class="homeitem3col">
			<h3>JSDT Components</h3>
			<p>
				The JavaScript Development Tools (JSDT) are broken down into three areas, which 
				include:
				<ol>
					<li><strong><a href="/webtools/jsdt/core">JSDT Core</a></strong> - includes the parser, compiler, DOM and more.</li>
					<li><strong><a href="/webtools/jsdt/ui">JSDT UI</a></strong> - includes all of the user interface bits to create the JavaScript IDE (wizard, dialogs, preferences, etc).</li>
					<li><strong><a href="/webtools/jsdt/debug">JSDT Debug</a></strong> - includes support for debugging JavaScript using Rhino and Crossfire</li>
					<li><strong><a href="/webtools/jsdt/web">JSDT Web</a></strong> - any and all support for Client-side (web-pages) JavaScript, implemented in WTP's Source Editing project.</li>
				</ol>
			</p>
			<p>
				The development page includes instructions for setting your environment, and how to contribute <a href="https://wiki.eclipse.org/JSDT/Development">here</a>.
			</p>
		</div>
		<div class="homeitem3col">
			<h2><b>New and Noteworthy</b></h2>
			<p>The JSDT debug component is very responsive to new ideas, feature requests and bug reports. The following is a list of some 
			of the newest and more interesting additions to the component.</p>
			<p>
				The latest and greatest features can be found on the WTP New and Noteworthy <a href="http://www.eclipse.org/webtools/development/news/">New and Noteworthy</a> page.
			    Entries for the JSDT can be found in the <strong>Source Editing and JSDT</strong> section.
			</p>
		</div>
		<div class="homeitem3col">
			<h3>Up To The Minute</h3>
			<p>
				JSDT has released version 3.8!  It can be found in the <a href="http://download.eclipse.org/releases/neon/">Neon repository</a>.
			</p>
		</div>
	</div>
	<div id="rightcolumn">
		$commonside
	</div>
</div>

EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
