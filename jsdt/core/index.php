<?php  				
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	

$App = new App();	
$Nav = new Nav();	
$Menu = new Menu();		

include($App->getProjectCommon());

	#*****************************************************************************
	#
	# Copyright (c) 2010 IBM Corporation and others.
 	# All rights reserved. This program and the accompanying materials
 	# are made available under the terms of the Eclipse Public License v1.0
 	# which accompanies this distribution, and is available at
 	# http://www.eclipse.org/legal/epl-v10.html
 	# Contributors:
	#     IBM Corporation - initial implementation
	#
	# Author: 		Ant Team
	# Date:			August 30, 2010
	#
	# Description: This is the new main page for the JSDT Team website
	#
	#****************************************************************************
	
	$pageTitle 		= "JavaScript Development Tools Core";
	$pageKeywords	= "javascript, JavaScript, JSDT, platform, debugging, debugger, jsdt, breakpoints, about, content, epl, compiler, compile, parse, parser";
	$pageAuthor		= "JSDT Team";
	
	include("../_sideCommon.php");
	include("_pluginCommon.php");
	
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
		<div class="homeitem3col">
			<h3>Project Overview</h3>
			<p>
				The JavaScript Development Tools (JSDT) provide plug-ins that implement an IDE
				supporting the development of JavaScript applications and JavaScript within
				web applications. It adds a JavaScript project type and perspective to the
				Eclipse Workbench as well as a number of views, editors, wizards, and
				builders.
			</p>
		</div>
		<div class="homeitem3col">
			<h3>JSDT Core Bundles</h3>
			<p>
				The JSDT Core is broken down into the following bundles:
				<ul>
					<li><strong>org.eclipse.wst.jsdt.core</strong> - contains the parser, compiler, DOM and more.</li>
					<li><strong>org.eclipse.wst.jsdt.core.tests.compiler</strong> - contains tests for the compiler.</li>
					<li><strong>org.eclipse.wst.jsdt.core.tests.model</strong> - contains tests for the model.</li>
				</ul>
			</p>
		</div>
		<div class="homeitem3col">
			<h2><b>New and Noteworthy</b></h2>
			<p>The JSDT debug component is very responsive to new ideas, feature requests and bug reports. The following is a list of some 
			of the newest and more interesting additions to the component.</p>
			<p>
				The latest and greatest features can be found on the WTP 3.2 <a href="http://www.eclipse.org/webtools/releases/3.2.0/NewAndNoteworthy/sourceediting.php">New and Noteworthy</a> page.
			</p>
			<p>
			 	For a complete listing of new and noteworthy entries check the <a href="http://www.eclipse.org/webtools/development/news/main.php">New and Noteworthy</a> page.
				Entries for the JSDT can be found in the <strong>Source Editing and JSDT</strong> section.
			</p>
		</div>
		<div class="homeitem3col">
			<h3>Up To The Minute</h3>
			<p>
				JSDT Core is preparing to release version 3.2.1!
			</p>
		</div>
	</div>
	<div id="rightcolumn">
		$commonside
		$commonplugin
	</div>
</div>

EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
