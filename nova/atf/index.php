<?php                                                                                                                                                                                                                                           require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App    = new App();    $Nav    = new Nav();    $Menu   = new Menu();           include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

#*****************************************************************************
#
# template.php
#
# Author:               Denis Roy
# Date:                 2005-06-16
#
# Description: Type your page comments here - these are not sent to the browser
#
#
#****************************************************************************

#
# Begin: page-specific settings.  Change these.
$pageTitle              = "The ajax toolkit framework subproject";
$pageKeywords   = "Eclipse WTP webtools atf";
$pageAuthor             ="Ugur Yildirim @ Eteration A.S.";

# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
# $Nav->addNavSeparator("My Page Links",        "downloads.php");
# $Nav->addCustomNav("My Link", "mypage.php", "_self", 3);
# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank", 3);

# End: page-specific settings
#

# Include this php file to get the right column
# as assigned by the $rightColumn variable in HTML content
include( 'rightcolumn.php' );


# Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="midcolumn">
<table><tbody><tr><td width="60%"><h1>atf</h1><div class="wtpsubtitle">the ajax toolkit framework subproject</div></td><td><img src="/webtools/images/wtplogosmall.jpg" alt="WTP Logo" usemap="logomap" align="middle" height="129" hspace="50" width="207"><map id="logomap" name="logomap"><area coords="0,0,207,129" href="/webtools/" alt="WTP Home"></map></td></tr></tbody></table><h2 class="bar">Introduction</h2><p>
The AJAX Toolkit Framework (ATF) provides an extensible
framework and exemplary tools for building IDEs for the many
different AJAX runtime offerings (Dojo, Zimbra, Rico, etc)
in the market. Tools built upon these frameworks will
initially include:
</p><ul class="indent">
<li>
enhanced JavaScript editing features such as
edit-time syntax checking;
</li>
<li>an embedded Mozilla web browser;</li>
<li>an embedded DOM browser; and</li>
<li>an embedded JavaScript debugger.</li>
</ul>
An additional and unique aspect of the framework is the
Personality Builder function, which assists in the
construction of arbitrary AJAX runtime frameworks, thus
allowing those runtimes to be used with ATF tools.
<p>
ATF is an incubator subproject of WTP. This page is
currently under construction. See
<a href="http://www.eclipse.org/atf/" target="_top">ATF Project Home</a>
for more information.
</p>



</div>
$rightColumn
EOHTML;


# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>

