<?php                                                                                                                                                                                                                                           require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App    = new App();    $Nav    = new Nav();    $Menu   = new Menu();           include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

#*****************************************************************************
#
# template.php
#
# Author:               Raghu Srinivasan
# Date:                 2000-02-13
#
# Description: Type your page comments here - these are not sent to the browser
#
#
#****************************************************************************

#
# Begin: page-specific settings.  Change these.
$pageTitle              = "WTP JSF Tools";
$pageKeywords   = "Eclipse WTP JSF Java EE IDE";
$pageAuthor             = "Raghu Srinivasan";

# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
# $Nav->addNavSeparator("My Page Links", "downloads.php");
# $Nav->addCustomNav("My Link", "mypage.php", "_self", 3);
# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank", 3);

# End: page-specific settings
#

# Include this php file to get the right column
# as assigned by the $rightColumn variable in HTML content
#include( 'rightcolumn.php' );


# Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="midcolumn">
<table>
<tr>
<td width="60%">
<h1>$pageTitle</h1>
        </td>
        <td><img
            src="/webtools/images/wtplogosmall.jpg"
            alt="WTP Logo"
            align="middle"
            height="129"
            hspace="50"
            width="207"
            usemap="logomap" /> <map
            id="logomap"
            name="logomap">
            <area
                coords="0,0,207,129"
                href="/webtools/"
                alt="WTP Home" />
        </map></td>
    </tr>
</table>
<h1>Project overview</h1>
<p> The JSF Tools Project adds comprehensive support to the Eclipse Web Tools Platform Project to simplify development and deployment of JavaServer Faces (JSF) applications. The project provides an extensible tooling infrastructure and exemplary tools for building JSF-based, web-enabled applications.
</p>
<h1>Getting Started</h1>
	<ul>
		<li><a href="docs/tutorial/JSFTools_1_0_tutorial.html">JSF Tools Tutorial</a></li>
		<li><a href="http://live.eclipse.org/node/225">Eclipse Webinar- JSF Tools Project</a></li>
		<li><a href="http://live.eclipse.org/node/483">Eclipse Webinar- Building Facelets Applications with the JSF Tools for Eclipse</a></li>
	</ul>
</div>
EOHTML;


# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>

