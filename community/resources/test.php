<?php

# Generate the web page
// Load the XML source
$xml = DOMDocument::load('resources.xml');

// Load the XSL source
$xsl = DOMDocument::load('resources.xsl');

// Configure the transformer
$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl); // attach the xsl rules

$maincontent = $proc->transformToXML($xml);

 
 print $maincontent;
?>
