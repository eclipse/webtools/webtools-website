<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0"
	xmlns:events="http://www.eclipse.org/webtools/events"
	xmlns:date="http://exslt.org/dates-and-times"
	xmlns:bios="http://www.eclipse.org/webtools/biopages"
    exclude-result-prefixes="date events bios">

	<!-- An event will display an optional logo with a link, a date and title with a link,
		 and a list of the presentation types for the event with relevant links. -->
	<xsl:template match="events:event">
		<table cellspacing="5" cellpadding="2" border="0">
			<tr>
				<td valign="top" align="right"><img width="16" height="16" border="0" src="../../images/Adarrow.gif"/></td>
				<td><a name="{@link}"/>
					<xsl:if test="@logo">
						<a href="{@link}" target="_top"><img src="{@logo}" border="0"/></a>
						<br/>
					</xsl:if><!-- date:format-date(@startdate,'MMM d, yyyy') -->
					<xsl:call-template name="formatDateForEvent">
						<xsl:with-param name="date" select="@startdate"/>
					</xsl:call-template>
					<xsl:if test="@startdate != @enddate"><!-- date:format-date(@enddate,'MMM d, yyyy') -->
						- 
					<xsl:call-template name="formatDateForEvent">
						<xsl:with-param name="date" select="@enddate"/>
					</xsl:call-template>
					</xsl:if>
					,&#160;<b><a href="{@link}" target="_top"><xsl:value-of select="@organization"/></a></b>
				</td>
			</tr>
			<tr>
				<td valign="top" align="right">&#160;</td>
				<td valign="top">
					<xsl:call-template name="allpresentations">
						<xsl:with-param name="presentations" select="events:presentation"/>
						<xsl:with-param name="showntypes" select="''"/>
						<xsl:with-param name="i" select="1"/>
					</xsl:call-template>
				</td>
			</tr>
		</table>
		<hr/>
	</xsl:template>
	
	<xsl:template name="allpresentations">
		<xsl:param name="presentations"/>
		<xsl:param name="showntypes"/>
		<xsl:param name="i"/>
		<xsl:variable name="type" select="$presentations[$i]/@type"/>
		<ul type="square">
			<xsl:if test="not(contains($showntypes,concat(',',$type ,',')))">
				<li>
					<u><xsl:value-of select="$type"/></u>
					<ul>
            	  		<xsl:apply-templates select="$presentations[@type=$type]" mode="individualpresentation"/>
					</ul>
				</li>
			</xsl:if>
		</ul>
		<xsl:if test="$presentations[$i+1]">
			<xsl:call-template name="allpresentations">
				<xsl:with-param name="presentations" select="$presentations"/>
				<xsl:with-param name="showntypes" select="concat($showntypes, ',', $type ,',')"/>
				<xsl:with-param name="i" select="$i + 1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<!-- A presentation will have an underlined title with an optional link,
		 and a list of presenters.
		 TODO: Add event resource links. -->
	<xsl:template match="events:presentation" mode="individualpresentation">
		<li>
		<xsl:choose>
			<xsl:when test="@link">
				<a href="{@link}" target="_top"><xsl:value-of select="@title"/></a>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="@title"/>
			</xsl:otherwise>
		</xsl:choose>
		<br/>
		<i>-&#160;
			<xsl:for-each select="events:presenter">
				<xsl:variable name="presentername" select="@name"/>
				<xsl:choose>
					<xsl:when test="document('../biopages.xml')/bios:bios/bios:bio/@name = $presentername">
						<a href="{document('../biopages.xml')/bios:bios/bios:bio[@name = $presentername]/@link}"><xsl:value-of select="@name"/></a>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="@name"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="position() != last()">
					,&#160;
				</xsl:if>
			</xsl:for-each>
   		</i>
		</li>
	</xsl:template>
	
	<!-- Given a date put it in the following format MMM d, yyyy
		 For example, Sep 1, 2005 -->
	<xsl:template name="formatDateForEvent">
		<xsl:param name="date"/>
		<xsl:value-of select="date:monthAbbreviation($date)"/>&#160;<xsl:value-of select="date:dayInMonth($date)"/>,&#160;<xsl:value-of select="date:year($date)"/>
	</xsl:template>
</xsl:stylesheet>