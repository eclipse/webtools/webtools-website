
/**
 * AreaServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.1 Nov 13, 2006 (07:31:44 LKT)
 */
package org.tempuri.areaservice;
/**
 *  AreaServiceSkeleton java skeleton for the axisService
 */
public class AreaServiceSkeleton{


	/**
	 * Auto generated method signature

	 * @param param0

	 */
	public  org.tempuri.areaservice.Area CalculateRectArea
	(
			org.tempuri.areaservice.Parameters param0
	)

	{
		//Todo fill this with the necessary business logic
		throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#CalculateRectArea");
	}

}
