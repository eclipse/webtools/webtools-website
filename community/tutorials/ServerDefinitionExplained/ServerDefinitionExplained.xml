<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="../../../wtp.xsl"?>
<html>
	<head>
		<meta name="root" content="../../../../" />
		<title>Generic server - Server Definition file explained</title>
	</head>
	<body>
		<h1>Generic server - Server Definition file explained</h1>
		<p>
			<b>By Gorkem Ercan</b>
			<br />
			December 13, 2005
			<br />
			<br />
		</p>

		<h2>Introduction</h2>
		<br></br>
		<p>
		Generic server is a special server and runtime implementation of 
		base server tooling that can adjust its behaviour by a server 
		type definition file. Server type definition files are XML based meta information files
		that are introduced using 
		<i>"org.eclipse.jst.server.generic.core.serverdefinition"</i> extension point</p>
		
		<p>
			This document describes the different parts of the server definition files.
		</p>

		<br></br>
		
		<h2>Server definition file introduction</h2>
		<br></br>
		<p>
	        A server definition file virtually has two parts; Properties and Server information.
			Properties are variables that is something that can be manipulated and changed 
			using generic server graphical user interface. Server information is used by the 
			generic server to perform server tooling functionality. This information can be 
			defined so that properties are used to determine their values.
		<br/><br/>
		If we define a property like;
		<pre><strong>
	  <font color="#0000FF">&lt;property id=</font>"serverAddress"
		<font color="#0000FF">label=</font>"Address:"
		<font color="#0000FF">type=</font>"string"
		<font color="#0000FF">context=</font>"server"
		<font color="#0000FF">default=</font>"127.0.0.1" <font color="#0000FF">/&gt;</font>
		</strong>
		</pre>
		<br/>
		 And if we refer to this property in a server information element such as:
	<pre>
	<strong>
	<font color="#0000FF">&lt;jndiConnection&gt;</font>
	<font color="#0000FF">    &lt;providerUrl&gt;</font>iiop://${serverAddress}:2001<font color="#0000FF">&lt;/providerUrl&gt;</font>
	<font color="#0000FF">    &lt;initialContextFactory&gt;</font>org.objectweb.carol.jndi.spi.MultiOrbInitialContextFactory<font color="#0000FF">&lt;/initialContextFactory&gt;</font>
	<font color="#0000FF">&lt;/jndiConnection&gt;</font></strong>
	</pre>
	<br/>
	When used in the generic server tooling the provider URL becomes<br/>
	<pre><strong><font  color="#009933">    iiop://127.0.0.1:2001</font></strong></pre>
	
	</p>

		<h2>
			Properties to collect user data
		</h2>
		<br></br>
		<p>	The defined properties show up in the generic server user interface for 
			collecting user data. A property element must have all the following attributes:
			<br/><br/>
			<b>id:</b> A property is referred in the server definition and the generic server APIs with this value.
			If you wish to refer to this property later in the server definiton file wrap it inside <i>${.. }</i>. Ids must be unique.
			<br/><br/>

			<b>label:</b> User friendly alias for the property. This is value is shown to users.
			<br/><br/>
			<b>type:</b> Properties are string valued. This attribute determines the type of GUI 
				to use. The allowed values, <i>"string"</i> renders a text widget, 
				<i>"boolean"</i> renders a check box, 
				<i>"directory"</i> renders a file path dialog, 
				<i>"file"</i> renders a file dialog.
			<br/><br/>
			<b>context:</b>Determined whether the property is part of server runtime or a server instance. 
			The user is asked for the value of the property in new Server Runtime wizard or New Server wizard
			depending on this value. Allowed values are <i>"server"</i>, and <i>"runtime"</i>
				
			<br/><br/>
			<b>default:</b> This is the initial value presented to user.
			</p>
			
		<h2>Defining classpaths</h2>
		<p>
			Well defined classpaths are an important part of server definitions. A classpath definition 
			can be referred in more than one section of the server definition. It can be used to 
			start, stop server or define the entries to be added to a project' s build path. 
			Classpaths are defined using the classpath element.
			<pre>
			<strong>
		<font color="#0000FF">&lt;classpath id="</font>jonas<font color="#0000FF">"&gt;</font>
		<font color="#0000FF">    &lt;archive path="</font>${classPath}/lib/common/ow_jonas_bootstrap.jar<font color="#0000FF">" /&gt;</font>
		<font color="#0000FF">    &lt;archive path="</font>${classPath}/conf<font color="#0000FF">" /&gt;</font>
		<font color="#0000FF">&lt;/classpath&gt;</font>
			</strong>
			</pre>
			<br/>
			<b>id:</b> Is the name that this classpath is referred to in server definition file and the generic
				server APIs
			<br/><br/>
			<b>archive and path:</b> this element points either to a directory or a java library that will be a part of the 
			      classpath. 
			  <br/><br/>    
			 If a flexible project is targeted for a generic server its build path is updated by 
			 the generic server tooling to include required libraries by the server. In server definition
			 files the project element is used to determine which classpath 
			 definition is used for project build paths.
			 <br/><br/>
			  <b>project:</b> element contains only one element that refers to a classpath definition.
		 	<pre><strong>
		<font color="#0000FF">&lt;project&gt;</font>
		<font color="#0000FF">   &lt;classpathReference&gt;</font>jonas<font color="#0000FF">&lt;/classpathReference&gt;</font>
		<font color="#0000FF">&lt;/project&gt;</font>
		    </strong></pre>
			      
		</p>

		<h2>Information to start and stop your server</h2>
		<p>
		The start and stop elements are very similiar tags that are used to define parameters to launch
		a server, and stop a running server respectively. They are both consist of the same elements. Generic server 
		framework support two kinds of launches, java launch and external launch. The type of the launch used is 
		determined by the launch configuration in the definition of the server type.

		<br/><br/>
		<b>workingDirectory:</b> This is the directory in which the application server starts. 
		<br/><br/>
		<b>programArguments:</b> Specific values that the server is expecting (a user name or a URLs, configuration 
		params, for example).
		<br/><br/>
		<b>environmentVariable:</b> These are the <i>name</i> and <i>value</i> pairs that are used as environment variables for launchs.
		<br/><br/>
		If a java launch is configured you should define mainClass, vmParameters, classpathReference tags in addition to above.
        <br/><br/>
		<b>mainClass</b>  A java application server is a Java application after all. Like any other Java application
		 you will need a class with a main method to start it. This the name of that class.
		<br/><br/>
		<b>vmParameters:</b> Values that are used as parameters to java virtual machine.
		<br/><br/>
		<b>classpathReference:</b> This is the classpath definition used to invoke the main class.
		<br/><br/>
		When defining an external launch use the external, and debugPort tags with workingDirectory and programArguments.
		<br/><br/>
		<b>debugPort:</b>This is the port number that eclipse debugger will connect to when the server is started in debug mode.
		<br/><br/>
		<b>external:</b>This is the external executable or script that will be used for launching the server. The os attribute in the 
		tag indicates the platform of the definition. You can have as many external tags as the number of platform you wish to support.
		<br/><br/>
		The following is an example for an external launch definition.
		<pre><strong>
		<font color="#0000FF">&lt;start&gt;</font>
		<font color="#0000FF">	&lt;external&gt;</font>${startScript}<font color="#0000FF">&lt;/external&gt;</font>
		<font color="#0000FF">	&lt;workingDirectory&gt;</font>${domainDirectory}<font color="#0000FF">&lt;/workingDirectory&gt;</font>
		<font color="#0000FF">	&lt;debugPort&gt;</font>${debugPort}<font color="#0000FF">&lt;/debugPort&gt;</font>
		<font color="#0000FF">	&lt;environmentVariable&gt;</font>
		<font color="#0000FF">		&lt;name&gt;</font>debugFlag<font color="#0000FF">&lt;/name&gt;</font>
		<font color="#0000FF">		&lt;value&gt;</font>true<font color="#0000FF">&lt;/value&gt;</font>
		<font color="#0000FF">	&lt;/environmentVariable&gt;</font>
		<font color="#0000FF">&lt;/start&gt;</font>
		</strong></pre>	
		
		</p>
		<h2>Publishers</h2>
		<p>
		Generic publishers are general purpose publishers that can be used for publishing to different
		servers. It is also possible to implement a server specific publisher, if the publishing
		mechanisms of the server requires it. A new publisher is introduced using
		 <i>org.eclipse.jst.server.generic.core.genericpublisher</i> extension point. WTP includes the 
		general purpose ANT publisher. ANT based publisher simply calls specific targets on an ANT script to 
		publish modules to a server.
		<br/><br/> 
		A server definition file can contain some server specific information that is used for initializing
		the publisher. The publisher element in the server definiton can be used for this purpose.
		<br/><br/>
		<pre><strong>
	    <font color="#0000FF">&lt;publisher id="</font>org.eclipse.jst.server.generic.antpublisher<font color="#0000FF">"&gt;</font>
		<font color="#0000FF">    &lt;publisherdata&gt;</font>
		<font color="#0000FF">        &lt;dataname&gt;</font>build.file<font color="#0000FF">&lt;/dataname&gt;</font>
		<font color="#0000FF">        &lt;datavalue&gt;</font>/buildfiles/jonas.xml<font color="#0000FF">&lt;/datavalue&gt;</font>
		<font color="#0000FF">    &lt;/publisherdata&gt;</font>
	<font color="#0000FF">&lt;/publisher&gt;</font>
		</strong>
		</pre>
		<br/>
		<b>id:</b> This is the id of the publisher. This value must match the value in the <i>plugin.xml</i>
		where the publisher is defined.
		<br/><br/> 
		<b>publisherdata:</b> A single name value pair that will be passed to publisher.
		
		</p>
	<h2>Modules</h2>
	<p>
		The <i>module</i> element shows the module types this server definition can work. 
		It also includes information on publishing the module.
		<br/><br/>
		<b>type:</b> The module type. This value must match the value in the <i>plugin.xml</i> where the 
		module type is defined.
		<br/><br/>
		<b>publishDir:</b> The directory where the final module will be published to.
		<br/><br/>
		<b>publisherReference:</b> The id of the generic publisher that should be used to publishing 
		of this module type.

	</p>
	<h2>Ports</h2>
	<p>
	the <i>port</i> element indicates the ports and the protocols that the server starts on. These 
	values are used to determine if a server is alive.
	<br/><br/>
	<b>no:</b> The port number.
	<br/><br/>
	<b>protocol:</b> The protocol supported by this port.
	<br/><br/>
	<b>name:</b> User friendly name of the port' s protocol.

	</p>
	<h2>JNDI properties</h2>
	<p>
	The JNDI properties for a server are needed for any clients that need jndi such as EJB testers. The 
	<i>jndiConnection</i> element is used to provide the jndi information for accessing an application server.
	<br/><br/>
	<b>providerUrl:</b> The url to jndi server.
	<br/><br/>
	<b>initialContextFactory:</b> Name of the initial context factory class for the server' s jndi implementation.
	<br/><br/>
	<b>jndiProperty:</b> Name, value pairs that may be used for establishing jndi connection, such as usernames, passwords, etc.
	</p>
	
	
	</body>
</html>
