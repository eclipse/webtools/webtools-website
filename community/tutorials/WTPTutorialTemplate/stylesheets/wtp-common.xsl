<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template name="infobox">
<xsl:param name="root"/>
<xsl:param name="title" />
<xsl:param name="icon" />
<xsl:param name="description" />
<xsl:param name="items" />
<TABLE cellspacing="0" cellpadding="0" width="275" border="0">
	<TR>
		<TD rowspan="3" height="15" bgcolor="#0080C0"  style="padding-top: 5px; padding-bottom: 5px;"><center><SPAN
			style="color: #ffffff; font-weight:bold; font-size: 11px; font-family: Verdana,Arial,Helvetica;  padding-left: 5px; padding-right: 5px;"><xsl:value-of select="$title"/></SPAN></center></TD> 
		<TD rowspan="1" height="13" width="57%"></TD>
	</TR>
	<TR>
		<TD height="3" bgcolor="#0080C0"><IMG
			src="{$root}/images/c.gif"
			width="1" height="10"/></TD>
	</TR>
</TABLE>
<TABLE cellspacing="0" cellpadding="0" width="275" border="0">
	<TR>
		<TD bgcolor="#0080C0"><IMG
			src="{$root}/images/c.gif"
			width="2" height="1"/></TD>
		<TD bgcolor="white"><IMG
			src="{$root}/images/c.gif"
			width="5" height="1"/></TD>
		<TD width="100%">
          <xsl:if test="$icon != ''">
            <img border="0" align="right">
              <xsl:attribute name="src">
                <xsl:value-of select="$icon"/>
              </xsl:attribute>
            </img>
          </xsl:if>
          <xsl:if test="$description">
            <xsl:apply-templates select="$description/*|$description/@*|$description/text()" mode="body"/>
          </xsl:if>
				<TABLE border="0" cellspacing="2">
					<TBODY>
                        <!-- xsl:value-of select="$items/*"/-->
                        <xsl:for-each select="$items">

						<TR>

							<!-- TD valign="top">
                              <xsl:if test="attribute::icon">
                                <img border="0">
                                  <xsl:attribute name="src">
                                    <xsl:value-of select="attribute::icon"/>
                                  </xsl:attribute>
                                </img>
                              </xsl:if>
                            </TD-->
							<TD>
                              <xsl:if test="count(child::item) > 0">
                                  
                                    <xsl:for-each select="item">
                                      <xsl:choose>
                                    <xsl:when test="@link and @title">
                                      <a>
                                        <xsl:attribute name="href">
                                          <xsl:value-of select="attribute::link"/>
                                        </xsl:attribute> 
                                        <b><xsl:value-of select="attribute::title"/></b>
                                      </a>
                                      
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <xsl:if test="@title">
                                        <b>
                                          <xsl:value-of select="attribute::title"/>
                                        </b>
                                      </xsl:if>
                                    </xsl:otherwise>
                                  </xsl:choose>  
                                    <table border="0">
                                      <tr>
                                        <xsl:if test="count(child::*) > 0 or child::text()">
                                        <td valign="top">
                                          <img src="{$root}/images/Adarrow.gif"/>
                                        </td>
                                        <td valign="top">
                                          <xsl:apply-templates select="*|@*|text()" mode="infobox"/>
                                        </td>
                                        </xsl:if>
                                      </tr>
                                    </table>
                                    </xsl:for-each>
                                   </xsl:if>
                            </TD>
						</TR>

                                 
                              
                    </xsl:for-each>
					</TBODY>
				</TABLE>
		
</TD>
		<TD bgcolor="white"><IMG
			src="{$root}/images/c.gif"
			width="5" height="1"/></TD>
		<TD bgcolor="#0080C0"><IMG
			src="{$root}/images/c.gif"
			width="2" height="1"/></TD>
	</TR>
	<TR>
		<TD bgcolor="#0080C0" colspan="5"><!--IMG
			src="{$root}/images/c.gif"
			width="1" height="2"/--></TD>
	</TR>
</TABLE>
</xsl:template>

<xsl:template name="page">
<xsl:param name="element"/>
<xsl:param name="title"/>
<xsl:param name="subtitle"/>
<xsl:param name="root"/>
<html>
		<head>
			<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type" />
			<link type="text/css" href="{$root}/default_style.css" rel="stylesheet" />
			<link type="text/css" href="{$root}/webtools/wtp.css" rel="stylesheet" />
			<title><xsl:value-of select="$title"/></title>
		</head>
<body>
<table border="0" cellpadding="2" cellspacing="5" width="100%">
  				<tbody>			
    <tr>
      <td align="left" width="60%"><font class="indextop"><xsl:value-of select="$title"/></font><br />
      <font class="indexsub"><xsl:value-of select="$subtitle"/></font></td>
      <td width="40%"> <img src="{$root}/webtools/images/wtplogosmall.jpg" align="middle" height="129" hspace="50" width="207" /></td>
    </tr>
    </tbody>
			</table>
<xsl:apply-templates select="$element">
  <xsl:with-param name="root" select="$root"/>
</xsl:apply-templates>
</body>
</html>
</xsl:template>


<xsl:template match="*|@*|text()" mode="infobox">
	<xsl:copy>
		<xsl:apply-templates select="*|@*|text()" mode="infobox"/>
	</xsl:copy>
</xsl:template>

<xsl:template match="*|@*|text()" mode="body">
		<xsl:copy>
			<xsl:apply-templates select="*|@*|text()" mode="body"/>
		</xsl:copy>
</xsl:template>

</xsl:stylesheet>