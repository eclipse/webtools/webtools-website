This package contains a template to be used to author a WTP tutorial.

Tutorial Name
-------------
By default the tutorial in this package is named tutorial. You can rename
the tutorial with any name you like provided you keep the XML extension.

A bit about XML
---------------
This tutorial package uses XML as the source format for the tutorial. This
approach was taken in keeping with the design principles of the WTP website
that keep content separated from presentation. It is not the intention of
WTP to limit those that can write tutorials. If you are not comfortable working
in XML you can also submit your tutorial in HTML.

Wikipedia contains more information about XML at http://en.wikipedia.org/wiki/Xml

How do I view the tutorial?
---------------------------
Modern Web browsers should be able to display your tutorial by simply opening
the XML file. If your browser cannot display the tutorial you can also build an
HTML version of your tutorial using the included Ant script. (You will need Ant
installed in order to run the script. Ant can be installed from 
http://ant.apache.org/.) Once built you will have an HTML version of your tutorial
that you can open in any browser.

How do I submit my tutorial for publication on the WTP website?
--------------------------------------------------------------
Once your tutorial is ready to be reviewed an published you can submit it by opening
a bug against the WTP website component
https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Web%20Tools&component=website
Once submitted your tutorial will be reviewed and may require some changes before
publication. Please try not to get discouraged. Reviews are part of the open source 
process and help ensure high quality material is published on the WTP site.