<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2007 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    15022010  - initial API and implementation
 * 
 * Content for this page is driven by the following data files in /webtools/community:
 * books.xml, articles.xml, biopages.xml, presentations-data.xml, tutorials.xml,
 * and whitepapers.xml
 *******************************************************************************/
$pageTitle = "Eclipse Web Tools Community Resources";
$pageKeywords	= "";
$pageAuthor		="Ugur Yildirim @ Eteration A.S.";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

# Functions
function date_cmp($a, $b) {
	// sort descending by date
	$a = strtotime($a->date);
	$b = strtotime($b->date);
	return $b - $a;
}

function pubDate_cmp($a, $b) {
	// sort descending by publicationDate
	$a = strtotime($a['publicationDate']);
	$b = strtotime($b['publicationDate']);
	return $b - $a;
}

function title_cmp($a, $b) {
	// sort by title
	$a = $a['title'];
	$b = $b['title'];
	return strcasecmp($a, $b);
}

# Main

# How new is new in days?
$notTooOld = 30;

# Build an assoc array of bio pages
$xml = simplexml_load_file('biopages.xml');

foreach ($xml->bio as $bio) {
	$bios[(string)$bio['name']] = (string)$bio['link'];
}

# Books
$xml = simplexml_load_file('books.xml');

# Generate the books section
$book_h = "<a name=\"books\"></a><h3>Books</h3>";
$book_h .= "<ul>";

foreach ($xml->book as $book) {
	$book_h .= "<li>";
	$title = $book['title'];
	$link = $book['link'];
	$image = $book['image'];
	$timestamp = strtotime($book['publicationDate']);
	$date = date("Y", $timestamp);
	
	// Approximately how many days ago was this published?
	$date_diff = (time() - $timestamp) / 86400;
	if ($image) {
		$book_h .= "<span class=\"sectionimage\">";
		if ($link) {
			$book_h .= "<a href=\"$link\"><img src=\"$image\"></a>";
		} else {
			$book_h .= "<img src=\"$image\">";
		}
		$book_h .= "</span>";
	}
	if ($link) {
		$book_h .= "<a href=\"$link\">$title</a>";
	} else {
		$book_h .= $title;
	}
	$book_h .= " ($date)";
	if ($date_diff <= $notTooOld) $book_h .= " <img src=\"/images/new.gif\" />";
	$book_h .= "<br /><br />by ";

	$count = count($book->author);

	foreach ($book->author as $author) {
		$count--;
		$name = (string)$author['name'];
		
		// see if we have a bio for this person
		if (isset($bios[$name])) {
			$book_h .= "<a href=\"$bios[$name]\">$name</a>";
		} else {
			$book_h .= $name;
		}
		if ($count) $book_h .= ", ";
	}
	$book_h .= "<br /><br />Publisher: ";
	$name = $book->publisher['name'];
	$link = $book->publisher['link'];
	if ($link) {
		$book_h .= "<a href=\"$link\">$name</a>";
	} else {
		$book_h .= $name;
	}
	if ($image) {
		$book_h .= "<div class=\"clearall\" />";
	}
	$book_h .= "</li>";
}
$book_h .= "</ul>";

# Articles
# Get articles array
$xml = simplexml_load_file('articles.xml');
$articles = $xml->xpath('/articles:articles/articles:article');

# Sort desending by publication date;
usort($articles, "pubDate_cmp");

# Generate the articles section
$art_h = "<a name=\"articles\"></a><h3>Articles</h3>";
$art_h .= "<ul>";

foreach ($articles as $article) {
	$art_h .= "<li>";
	$title = $article['title'];
	$link = $article['link'];
	$timestamp = strtotime($article['publicationDate']);
	$date = date("M d, Y", $timestamp);
	
	// Approximately how many days ago was this published?
	$date_diff = (time() - $timestamp) / 86400;
	$art_h .= "<a href=\"$link\" target=\"_blank\">$title</a> ($date)";
	if ($date_diff <= $notTooOld) $art_h .= " <img src=\"/images/new.gif\" />";
	$art_h .= "<br />";
	$name = $article->publication['name'];
	$link = $article->publication['link'];
	if ($link) {
		$art_h .= "<a href=\"$link\" target=\"_blank\">$name</a>";
	} else {
		$art_h .= "$name";
	}
	$art_h .= " by ";
	
	$count = count($article->author);
	foreach ($article->author as $author) {
		$count--;
		$name = (string)$author['name'];
		
		// see if we have a bio for this person
		if (isset($bios[$name])) {
			$art_h .= "<a href=\"$bios[$name]\">$name</a>";
		} else {
			$art_h .= $name;
		}
		if ($count) $art_h .= ", ";
	}
	$art_h .= "</li>";
}
$art_h .= "</ul>";

# Presentations
# Get presentations array
$xml = simplexml_load_file('presentations-data.xml');

# Get the base URL for downloads
$base = $xml['base'];

# Get presentations array
$presentations = $xml->xpath('/presentations/presentation');

# Sort desending by publication date;
usort($presentations, "date_cmp");

# Generate the presentaions section
$pres_h = <<<EOHTML
<a name="presentations"></a>
<h3>Presentations</h3>
<ul>
<p>
If you have a presentation that you'd like to share with the WTP community,
please contribute it by creating a <a href='https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Web%20Tools'>Bugzilla</a> bug against WTP. Use
<ul>
<b>Product:</b> Web Tools<br />
<b>Component:</b> website<br />
<b>Severity:</b> enhancement<br /><br />
</ul>
Include the title, speaker, venue, and date in the bug comments.
Attach your presentation to the bug report. 
</p>
EOHTML;

foreach ($presentations as $presentation) {
	$pres_h .= "<li>";
	$title = $presentation->title;
	$link = $base . trim($presentation->file);
	$pres_h .= "<a href=\"$link\">$title</a>";
	
	foreach ($presentation->translation as $translation) {
		$title = $translation['language'];
		$link = $base . trim($translation);
		$pres_h .= " [<a href=\"$link\">$title</a>] ";
	}
	$timestamp = strtotime($presentation->date);
	$date = date("M d, Y", $timestamp);
	
	// Approximately how many days ago was this published?
	$date_diff = (time() - $timestamp) / 86400;
	$pres_h .= " ($date)";
	if ($date_diff <= $notTooOld) $pres_h .= " <img src=\"/images/new.gif\" />";
	$pres_h .= "<br />by ";
	
	$count = count($presentation->presenter);
	foreach ($presentation->presenter as $presenter) {
		$count--;
		$name = (string)$presenter;
		
		// see if we have a bio for this person
		if (isset($bios[$name])) {
			$pres_h .= "<a href=\"$bios[$name]\">$name</a>";
		} else {
			$pres_h .= $name;
		}
		if ($count) $pres_h .= ", ";
	}
	$pres_h .= " at $presentation->venue";
	$pres_h .= "</li>";
}
$pres_h .= "</ul>";

# Tutorials
$xml = simplexml_load_file('tutorials.xml');

# Generate the tutorials section
$tut_h = <<<EOHTML
<a name="tutorials"></a>
<h3>Tutorials</h3>
<ul>
<p>
Interested in writing a new WTP tutorial? Download the 
<a href="tutorials/WTPTutorialTemplate.zip">WTP tutorial template</a>
and get started!
</p>
EOHTML;

$count = count($xml->category);
foreach ($xml->category as $category) {
	$count--;
	$name = $category['name'];
	$link = "#" . $category['id'];
	$tut_h .= "<a href=\"$link\">$name</a>";
	if ($count) $tut_h .= " | ";
}
$tut_h .= "<br />";

foreach ($xml->category as $category) {
	$name = $category['name'];
	$id = $category['id'];
	$tut_h .= "<br /><a name=\"$id\"></a><b>$name</b>";
	
	// Get all tutorias for a category
	$tutorials = $xml->xpath("/tutorials:tutorials/tutorials:tutorial[contains(@categories, \"$id\")]");

	// Sort by title
	usort($tutorials, "title_cmp");
	
	foreach ($tutorials as $tutorial) {
		$tut_h .= "<li>";
		$title = $tutorial['title'];
		$link = $tutorial['link'];

		//Use later of posted or updated date
		$posted = strtotime($tutorial['datePosted']);
		$updated = strtotime($tutorial['lastUpdated']);
		if ($updated > $posted) {
			$timestamp = $updated;
			$image = "/images/updated.gif";
		} else {
			$timestamp = $posted;
			$image = "/images/new.gif";
		}
		$date = date("M d, Y", $timestamp);
		
		// Approximately how many days ago was this published?
		$date_diff = (time() - $timestamp) / 86400;
		$tut_h .= "<a href=\"$link\">$title</a> ($date)";
		if ($date_diff <= $notTooOld) $tut_h .= " <img src=\"$image\" />";
		$tut_h .= "<br /><br />";
		$tut_h .= $tutorial->abstract;
		$tut_h .= "</li>";
	}
}
$tut_h .= "</ul>";

# White papers
# Get white papers array
$xml = simplexml_load_file('whitepapers.xml');
$papers = $xml->xpath('/whitepapers:whitepapers/whitepapers:paper');

# Sort desending by publication date;
usort($papers, "pubDate_cmp");

# Generate the white papers section
$paper_h = "<a name=\"whitepapers\"></a><h3>White Papers</h3>";
$paper_h .= "<ul>";

foreach ($papers as $paper) {
	$paper_h .= "<li>";
	$title = $paper['title'];
	$link = $paper['link'];
	$timestamp = strtotime($paper['publicationDate']);
	$date = date("M d, Y", $timestamp);
	
	// Approximately how many days ago was this published?
	$date_diff = (time() - $timestamp) / 86400;
	$paper_h .= "<a href=\"$link\" target=\"_blank\">$title</a> ($date)";
	if ($date_diff <= $notTooOld) $paper_h .= " <img src=\"/images/new.gif\" />";
	$paper_h .= "</li>";
}
$paper_h .= "</ul>";

# Generate the web page
$html = <<<EOHTML
<div id="maincontent">
	<div id="midcolumn">
		<table>
			<tr>
				<td width="60%">
					<h1>WTP community resources</h1>
					<div class="wtpsubtitle">$pageTitle</div>
				</td>
				<td>
					<img src="/webtools/images/wtplogosmall.jpg"
						align="middle" height="129" hspace="50" width="207" />
				</td>
			</tr>
		</table>
		<div class="homeitem3col">
<h3>Resources from around the WTP community...</h3>
<p>
This page is about finding more information about WTP from around the WTP community.
The community has provided many
<a href="#articles">articles</a>,
<a href="#books">books</a>,
<a href="#presentations">presentations</a>,
<a href="#tutorials">tutorials</a>, and
<a href="#whitepapers">white papers</a>
that are dedicated to WTP and can help you to make the most of the WTP platform and tools.
</p>
<p>
We'd like these pages to be useful to you - so please open a
<a href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Web%20Tools&amp;component=website">
bug report</a> for any corrections, additions, pointers, or comments!
</p>
</div>
		<div class="homeitem3col">
			$book_h
		</div>
		<div class="homeitem3col">
			$art_h
		</div>
		<div class="homeitem3col">
			$pres_h
		</div>
		<div class="homeitem3col">
			$tut_h
		</div>
		<div class="homeitem3col">
			$paper_h
		</div>
	</div>
</div>

EOHTML;
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
