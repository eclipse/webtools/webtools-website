<?php
//require_once "/home/data/httpd/eclipse-php-classes/system/dbconnection_bugs_ro.class.php";
ini_set("display_errors", "true");
error_reporting (E_ALL);

include("standardStuff.php");

function getGroup($name, $group ) {

    echo "<h3>$name</h3>\n";

    //$ldapserver="ldapslave";
    // name didn't work? ping, or, see /etc/hosts to get address
    $ldapserver="172.25.25.11";
    //echo "<br/>ldapserver: $ldapserver <br/>";
    
    $ds = ldap_connect($ldapserver);

    ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);

    if ($ds) {
        $r = ldap_bind($ds); # anonymous bind
        if( $r ) {
            $result=ldap_search($ds, "cn=$group,ou=group,dc=eclipse,dc=org", "(member=*)");

            $info = ldap_get_entries($ds, $result);

            $uid = "DOESNOTEXIST";

            // print number of entries found
            $nEntries = ldap_count_entries($ds, $result);
            // echo "Number of entries found: " . $nEntries . "<p>";
            $i = 0;
            $nrecords = $info[$i]["count"];
            // echo "nrecords: " . $nrecords. "<br />\n";

            for ($ii=0; $ii<$nrecords; $ii++){
                // echo "Record: " . $ii;
                $data = $info[$i][$ii];
                //echo $data."($i.$ii):&nbsp;&nbsp;".$info2[$i][$data][0]."<br />\n";
                if( $data == "member" ) {
                    //                                echo "in member<br />\n";
                    $nmembers = $info[0]["member"]["count"];
                    // echo "n members: " . $nmembers . "<br />\n";
                    echo "<table border='1' cellpadding='2' align='center' width='90%'>";
                    echo "<th width='40%'>Name</th><th width='40%'>Email</th><th width='20%'>cvs id</th>\n";
                    for($j = 0; $j<$nmembers; $j++ ) {
                        $attr = $info[0]["member"][$j];
                        //echo $i .". " . $j . ". " ."&nbsp;&nbsp;&nbsp;". $attr . "<br />\n";
                        $justthese = array("ou",  "uid", "givenname", "sn", "mail");
                        $result2=ldap_search($ds, $attr, "(mail=*)", $justthese);
                        $personinfo = ldap_get_entries($ds, $result2);
                        // The following is the structure of the multidemensional array returned
                        //
                        //return_value["count"] = number of entries in the result
                        //return_value[0] : refers to the details of first entry
                        //
                        //return_value[i]["dn"] =  DN of the ith entry in the result
                        //
                        //return_value[i]["count"] = number of attributes in ith entry
                        //return_value[i][j] = jth attribute in the ith entry in the result
                        //
                        //return_value[i]["attribute"]["count"] = number of values for
                        //                                        attribute in ith entry
                        //return_value[i]["attribute"][j] = jth value of attribute in ith entry
                        //

                        $pi = 0;
                        for ($pii=0; $pii<$personinfo[$pi]["count"]; $pii++){
                            $data = $personinfo[$pi][$pii];
                            //echo "&nbsp;" . $data."(" . $pi.".".$pii . "):&nbsp;&nbsp;" . $personinfo[$pi][$data][0];
                        }
                        //echo "<br />\n";
                        echo "<tr>\n";

                        echo "    <td>\n";
                        echo "    " . $personinfo[$pi]['givenname'][0] . "&nbsp;" . $personinfo[$pi]['sn'][0];
                        echo "    </td>\n";

                        echo "    <td>\n";
                        echo "     " .    str_replace("@","{at}", $personinfo[$pi]['mail'][0]);
                        echo "    </td>\n";

                        echo "    <td>\n";
                        echo "     " .  $personinfo[$pi]['uid'][0];
                        echo "    </td>\n";

                        echo "</tr>\n";



                    }
                }  echo "</table>\n";
            }


        }
        else {
            echo "did not access<br />\n";
            echo "ldap_result: " . ldap_error($ds) . "<br />\n";
        }
    }
    ldap_close($ds);
}


standardHTMLHeader("Committer List");
echo "<h1>Committers, by sub-project</h1>\n";

echo "<p>Date of Committers Query: " . date(DATE_RFC822) . "</p>\n";
echo "    <h2>Committer List</h2>";
echo "    <p>";
echo "        Most of the code in WTP, of course, comes from the dedicated work of the WTP Committers. The current, dynamically updated list of Committers and sub-project teams are always available in the";
echo "        <a";
echo "            href='http://www.eclipse.org/projects/project_summary.php?projectid=webtools'>Eclipse Portals Eclipse Web Tools Platform Project</a>.";
echo " The following is a static snapshot of those committers that have contributed to this release.";
echo "    </p>";
echo "<h2>Committers in Projects releasing code</h2>";
getGroup("Common Project", "webtools.common-dev");
getGroup("EJB Tools Project", "webtools.ejbtools-dev");
getGroup("JEE Tools Project", "webtools.jeetools-dev");
getGroup("Dali Project", "webtools.dali-dev");
getGroup("Java Server Faces Project", "webtools.jsf-dev");
getGroup("Server Tools Project", "webtools.servertools-dev");
getGroup("Source Editing Project", "webtools.sourceediting-dev");
getGroup("Webservices Project", "webtools.webservices-dev");
echo "<h2>Committers in Incubating Projects</h2>";
getGroup("WTP Incubator Project", "webtools.incubator-dev");
getGroup("ATF Project", "webtools.atf-dev");
echo "<h2>Committers in Quiescent Projects</h2>";
getGroup("Datatools Project", "webtools.datatools-dev");
standardHTMLFooter();

?>
