<?php
require_once "/home/data/httpd/eclipse-php-classes/system/dbconnection_bugs_ro.class.php";

function exploreResult ($ldapResult) {

    echo $ldapResult["count"] . ": number of entries in the result" . "<br />\n";
    echo $ldapResult[0] . ": details of first entry" . "<br />\n";
    echo "<br />\n";
    
    for ($i = 0;$i<$ldapResult["count"];$i++) {
        echo "<br />\n";
        echo "Entry: " . $i . "<br />\n";
        echo $ldapResult[$i]["dn"] . ": DN of entry " . $i . "<br />\n";
        echo $ldapResult[$i]["count"] . ": number of attributes entry " . $i . "<br />\n";
        for ($j = 0; $j < $ldapResult[$i]["count"]; $j++) {
            echo "&nbsp;&nbsp;&nbsp;&nbsp;" . $ldapResult[$i][$j] . ": [$i] [$j]" . "<br />\n"; //jth attribute in the ith entry in the result
            echo "&nbsp;&nbsp;&nbsp;&nbsp;" . $ldapResult[$i][$ldapResult[$i][$j]]["count"] . ": number of values for attribute in ith entry" . "<br />\n";
            for ($k = 0; $k < $ldapResult[$i][$ldapResult[$i][$j]]["count"]; $k++) {
                echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $ldapResult[$i][$ldapResult[$i][$j]][$k] . ": kth value of jth attribute in ith entry" . "<br />\n";
            }
        }
    }
    echo "<br />\n";
}

function test_connection( ) {
    $ds = ldap_connect();

    ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);

    if ($ds) {
        $r = ldap_bind($ds); # anonymous bind
        if( $r ) {
            $sr=ldap_search($ds, "dc=eclipse,dc=org", "(objectClass=*)");

            $info = ldap_get_entries($ds, $sr);

            exploreResult($info);
        }
        ldap_close($ds);
    }
}

ini_set("display_errors", "true");
error_reporting (E_ALL);
echo "LDAP Explore Page <br />";
test_connection();

exit();
?>
