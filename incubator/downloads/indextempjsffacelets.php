<?php                                                                                                                                                                                                                                           require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App    = new App();    $Nav    = new Nav();    $Menu   = new Menu();           include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'


        #
        # Begin: page-specific settings.  Change these.
        $pageTitle              = "WTP Incubator Downloads";
        $pageKeywords   = "Eclipse WTP webtools XSL XML IDE";
        $pageAuthor             = "David Williams";

        # Add page-specific Nav bars here
        # Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
        # $Nav->addNavSeparator("My Page Links",        "downloads.php");
        # $Nav->addCustomNav("My Link", "mypage.php", "_self", 3);
        # $Nav->addCustomNav("Google", "http://www.google.com/", "_blank", 3);

        # End: page-specific settings
        #
	# Generate the web page
	// Load the XML source
	$xml = new DOMDocument;
	$xml->loadHTMLFile('indextempjsffacelets.html');

	//Set the page title
	$xpath = new DOMXPath($xml);
	$titleNode = $xpath->query("/html/head/title")->item(0); 
	// echo "<br />titleNodeValue: " . $titleNode->nodeValue;
	$pageTitle = ($titleNode != null) ? $titleNode->childNodes->item(0)->nodeValue : "eclipse.org webtools page";

	// Load the XSL source
	$xsl = DOMDocument::load($root . '../../../webtools/wtpphoenix.xsl');

	// Configure the transformer
	$proc = new XSLTProcessor;
	$proc->importStyleSheet($xsl); // attach the xsl rules

	// work on just the body of the original (not head, etc.)
	$xmlbody=$xml->getElementsByTagName('body')->item(0); 
	$maincontent = $proc->transformToXML($xmlbody);
	
        # Paste your HTML content between the EOHTML markers!
        $html = <<<EOHTML
<div id="maincontent">
	<div id="midcolumn">
		<p>
		$maincontent
		</p>
	</div>
        
        <div id="rightcolumn">
        <div class="sideitem">
			<h6>Incubation</h6>
   			<div align="center"><a href="/projects/what-is-incubation.php"><img 
        		align="center" src="/images/egg-incubation.png" 
        		border="0" alt="Incubation" /></a></div>
		</div>
		</div>
</div>
EOHTML;


        # Generate the web page
        $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>

