<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
$pageKeywords	= "";
$pageAuthor		="Ugur Yildirim @ Eteration A.S.";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

//Set the page title
$pageTitle = "Eclipse Web Tools Platform Project - Contributors";

$html = <<<EOHTML
$wtpTopButtons
<div id="maincontent">
	<div id="midcolumn">
		<p>
		This content was removed, since it was out of date and not maintained. Removal details documented in <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=295000">bug 295000</a>. 
		</p>
		<p>Similar information can be found at the Eclipes Foundation's <a href="http://www.eclipse.org/projects/listofprojects.php">Projects Pages</a>.</p>
		<p></p>
	</div>
</div>


EOHTML;
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
