<?xml version="1.0"?>
<!--
	
	Transforms Bugzilla RDF report into milestone format.
	
	Change Log:
	
	2005-06-16: Arthur Ryman <ryman@ca.ibm.com>
	- created
	
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	xmlns:bz="http://www.bugzilla.org/rdf#"
	xmlns:nc="http://home.netscape.com/NC-rdf#" exclude-result-prefixes="rdf bz nc" >

	<xsl:output method="xml" indent="yes" />

	<xsl:key name="target_milestones" match="//bz:target_milestone"
		use="." />

	<xsl:template match="/">

		<plan xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xsi:noNamespaceSchemaLocation="../../../../development/milestone_plans/milestonePlan.xsd">
			<component name="Overview of" subproject="any">
				<milestone name="Milestones">
					<title>
						<xsl:text>All Milestones -&#32;</xsl:text>
						<xsl:value-of select="count(//bz:bug)" />
						<xsl:text>&#32;Enhancement Requests</xsl:text>
					</title>
					<xsl:call-template name="categories" />
				</milestone>
			</component>
		</plan>

	</xsl:template>

	<xsl:template name="categories">
		<xsl:for-each select="//bz:target_milestone">
			<xsl:sort select="." />
			<xsl:if
				test="generate-id(.)=generate-id(key('target_milestones',.)[1])">
				<category name="{.}">
					<xsl:variable name="target_milestone" select="." />
					<xsl:for-each
						select="//bz:bug[bz:target_milestone=$target_milestone]">
						<xsl:sort select="bz:component" />
						<xsl:sort select="bz:id" />
						<xsl:variable name="priority">
							<xsl:choose>
								<xsl:when test="bz:priority='P1'">
									<xsl:text>high</xsl:text>
								</xsl:when>
								<xsl:when test="bz:priority='P2'">
									<xsl:text>medium</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>low</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<item priority="{$priority}" status="new">
							<description>
								<xsl:text>[</xsl:text>
								<xsl:value-of select="bz:component" />
								<xsl:text>]&#32;</xsl:text>
								<xsl:value-of
									select="bz:short_short_desc" />
							</description>
							<bugzilla link="{@rdf:about}" />
						</item>
					</xsl:for-each>
				</category>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>
