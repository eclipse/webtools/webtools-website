<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="../wtphome.xsl"?>
<sections title="capability defintions in wtp" subtitle="The WTP Eclipse Capabilities">
  <meta name="root" content="../../../.." />

 <section class="main" name="Current Status">
  <description>
   <p>
  	11/01/2005: Initial version - Lawrence Mandel
   </p>
  </description>
 </section>
 <section class="main" name="Introduction and Background">
  <description>
   <p>
  	The purpose of this document is to provide information about WTP's capability definitions.
  	Eclipse capabilities allow users to select the function they are using and filters out
  	perspectives, views, menu contributions, etc. from the user's workspace based on the
  	capabilities that are selected. The Eclipse capabilities preference page can be located
  	at Window->Preferences...->General->Capabilities.
  </p>
  <p>
    The initial set of WTP capability definitions is based on the capability defintions in the
    Eclipse SDK.
  </p>
  <p>
  	The capability definitions descibed below were composed from comments on <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=110940" target="_top">bug 110940</a>.
   </p>
  </description>
 </section>
 <section class="main" name="WTP Capabilities">
  <description>
   <p>
  	WTP adds the following capabilities to Eclipse:
   </p>
   <p>
   	Development
   	<ul>
   	  <li>
        Database Development
      </li>
      <li>
        Enterprise Java Development
      </li>
      <li>
        Web Development
      </li>
      <li>
        Web Service Development
      </li>
      <li>
        XML Development
      </li>
    </ul>
   </p>
  </description>
 </section>
 <section class="main" name="Capability Dependecies">
  <description>
   <p>
    Capabilities can define dependencies on other capabilities. This allows for the enablement
    or disablement of capabilies with the selection of other, dependent capabilities. To illustrate
    this we can look at a simple example where two capabilities, A and B, are defined. In this example
    B requires A. Enablement of B requires the enablement of A, which will happen automatically when
    B is selected. Similarily, the disablement of A will also disable B.
   </p>
   <p>
  	The WTP capabilities are defined with the following dependencies:<br />
  	<img src="capabilitydependencies.gif"/>
   </p>
  </description>
 </section>
 <section class="main" name="Capability Plug-in encapsulation">
  <description>
   <p>
  	The WTP capabilities control the activity of the WTP plug-ins as described below. The
  	affiliation of the plug-ins represents which subproject defines the control.
   </p>
   <p>
    <b>Database Development</b>
    <ul>
     <li>org.eclipse.wst.rdb.*  (WST)</li>
    </ul>
   </p>
   <p>
    <b>Enterprise Java Development</b>
    <ul>
     <li>org.eclipse.jst.common.*  (JST)</li>
     <li>org.eclipse.jst.ejb.*  (JST)</li>
     <li>org.eclipse.jst.j2ee.*  (JST)</li>
     <li>org.eclipse.jst.jsp.*  (JST)</li>
     <li>org.eclipse.jst.server.*  (JST)</li>
     <li>org.eclipse.jst.servlet.*  (JST)</li>
    </ul>
   </p>
   <p>
    <b>Web Development</b>
    <ul>
     <li>org.eclipse.wst.css.*  (WST)</li>
     <li>org.eclipse.wst.html.*  (WST)</li>
     <li>org.eclipse.wst.internet.monitor.*  (WST)</li>
     <li>org.eclipse.wst.javascript.*  (WST)</li>
     <li>org.eclipse.wst.server.*  (WST)</li>
     <li>org.eclipse.wst.web.*  (WST)</li>
    </ul>
   </p>
   <p>
    <b>Web Service Development</b>
    <ul>
     <li>org.eclipse.jst.ws.*  (JST)</li>
     <li>org.apache.axis.*  (WST)</li>
     <li>org.apache.wsil4j.*  (WST)</li>
     <li>org.eclipse.wst.command.*  (WST)</li>
     <li>org.eclipse.wst.ws.*  (WST)</li>
     <li>org.eclipse.wst.wsdl.*  (WST)</li>
     <li>org.eclipse.wst.wsi.*  (WST)</li>
     <li>org.uddi4j.*  (WST)</li>
     <li>org.wsdl4j.*  (WST)</li>
    </ul>
   </p>
   <p>
    <b>XML Development</b>
    <ul>
     <li>org.apache.xerces.*  (WST)</li>
     <li>org.eclipse.wst.common.*  (WST)</li>
     <li>org.eclipse.wst.dtd.*  (WST)</li>
     <li>org.eclipse.wst.internet.cache.*  (WST)</li>
     <li>org.eclipse.wst.internet.proxy.*  (WST)</li>
     <li>org.eclipse.wst.sse.*  (WST)</li>
     <li>org.eclipse.wst.validation.*  (WST)</li>
     <li>org.eclipse.wst.xml.*  (WST)</li>
     <li>org.eclipse.wst.xsd.*  (WST)</li>
    </ul>
   </p>
  </description>
 </section>
</sections>