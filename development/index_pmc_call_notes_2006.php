<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
$pageKeywords	= "PMC Meetings Eclipse Web Tools Platform WTP";
$pageAuthor		= "David Williams";

$root = $_SERVER['DOCUMENT_ROOT'];

$pageTitle = "Eclipse Web Tools Platform PMC Meetings";

$xmlString = <<<EOXML
<html>
<body>
<h1>$pageTitle</h1>

<h2>Meeting agendas and minutes</h2>
<ul>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-12-19">December 19, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-12-12">December 12, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-12-05">December 05, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-11-28">November 28, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-11-21">November 21, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-11-14">November 14, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-11-07">November 07, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-10-31">October 31, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-10-24">October 24, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-10-17">October 17, 2006</a></li>
	<li>October 10, 2006: Eclipse Summit Europe, so most PMC members absent. No notes.</li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-10-03">October 03, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-09-26">September 26, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-09-19">September 19, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-09-12">September 12, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-09-05">September 05, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-08-29">August 29, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-08-22">August 22, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-08-15">August 15, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-08-08">August 08, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-08-01">August 01, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-07-25">July 25, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-07-18">July 18, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-07-11">July 11, 2006</a></li>
	<li>July 04, 2006: PMC in recess.</li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-06-27">June 27, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-06-20">June 20, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-06-13">June 13, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-06-06">June 06, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-05-30">May 30, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-05-23">May 23, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-05-16">May 16, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-05-09">May 9, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-05-02">May 2, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-04-25">April 25, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-04-18">April 18, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-04-11">April 11, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-04-04">April 4, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-03-28">March 28, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-03-21">March 21, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-03-14">March 14, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-03-07">March 7, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-02-28">February 28, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-02-21">February 21, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-02-14">February 14, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-02-07">February 07, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-01-31">January 31, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-01-24">January 24, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-01-17">January 17, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-01-10">January 10, 2006</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2006-01-03">January 03, 2006</a></li>
</ul>
<hr />
<p>Back to <a href="/webtools/development/index_pmc_call_notes.php">meeting list</a>.</p>
<p>Please send any additions or corrections to <a href="mailto: david_williams@us.ibm.com">David Williams.</a></p>
</body>
</html>
EOXML;

$xml = DOMDocument::loadXML($xmlString);
// Load the XSL source
$xsl = DOMDocument::load($root . '/webtools/wtpnova.xsl');

// Configure the transformer
$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl); // attach the xsl rules

$maincontent = $proc->transformToXML($xml);


$html = <<<EOHTML
<div id="maincontent">
	<div id="midcolumn">
	$maincontent
	</div>
</div>
EOHTML;
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
	?>
