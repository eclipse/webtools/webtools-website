<?php                                                                                                                       require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App    = new App();    $Nav    = new Nav();    $Menu   = new Menu();       include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
$pageKeywords   = "";
$pageAuthor     = "David Williams";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

$pageTitle = "eclipse.org webtools page";

header("Location: https://wiki.eclipse.org/WTP_Development_Status_Meetings");

$html = <<<EOHTML
<div id="maincontent">
    <div id="midcolumn">
<p>This <a href="https://wiki.eclipse.org/WTP_Development_Status_Meetings">site</a> has been moved. If you are not
automatically forwarded there, please see the <a href="https://wiki.eclipse.org/WTP_Development_Status_Meetings">WTP
Development Status Meetings</a> page on our wiki site. </p>
    </div>
</div>


EOHTML;
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
