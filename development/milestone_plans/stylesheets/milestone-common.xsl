<?xml version="1.0" encoding="UTF-8"?>
<!--
 
 ChangeLog:
 
 2005-02-24 Phil Avery, pavery@us.ibm.com
 - added 'onmouseover' and 'onmouseout' actions
 to display 'detail' info in a new window
 
-->
<xsl:stylesheet
 version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:xalan="http://xml.apache.org/xslt"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xmlns="http://www.w3.org/1999/xhtml">
 <xsl:template name="printHeader">
  <xsl:param name="rel"></xsl:param>
  <xsl:param name="title"></xsl:param>
  <xsl:param name="description"></xsl:param>
  <table
   cellSpacing="5"
   cellPadding="2"
   width="100%"
   border="0">
   <tr>
    <td>
     <font class="indextop">
      <xsl:value-of select="$title" />
     </font>
     <br />
     <font class="indexsub">
      <xsl:value-of select="$description" />
     </font>
    </td>
    <td>
     <br />
    </td>
    <td width="28%">
     <img
      width="120"
      height="86">
      <xsl:attribute name="src">
							<xsl:value-of select="concat($rel, 'development/milestone_plans/stylesheets/images/Idea.jpe')" />
						</xsl:attribute>
     </img>
    </td>
   </tr>
  </table>
 </xsl:template>
 <xsl:template name="printLegend">
  <xsl:param name="rel" />
  <xsl:variable
   name="prefix"
   select="concat($rel, 'development/milestone_plans/stylesheets/images/')" />
  <table
   cellspacing="5"
   cellpadding="2"
   width="100%"
   border="0">
   <tbody>
    <tr>
     <td
      valign="top"
      align="left"
      width="100%"
      bgcolor="#0080c0"
      colspan="4">
      <b>
       <font face="Arial,Helvetica">
        <font color="#ffffff">Legend</font>
       </font>
      </b>
     </td>
    </tr>
    <tr>
     <td valign="top">
      <img
       height="5"
       width="14">
       <xsl:attribute name="src">
								<xsl:value-of select="concat($prefix, 'progress.gif')" />
							</xsl:attribute>
      </img>
     </td>
     <td width="50%">item is under development.</td>
     <td valign="top">
      <img
       height="10"
       width="10">
       <xsl:attribute name="src">
								<xsl:value-of select="concat($prefix, 'glass.gif')" />
							</xsl:attribute>
      </img>
     </td>
     <td width="50%">item is under investigation.</td>
    </tr>
    <tr>
     <td valign="top">
      <img
       height="10"
       width="10">
       <xsl:attribute name="src">
								<xsl:value-of select="concat($prefix, 'ok.gif')" />
							</xsl:attribute>
      </img>
     </td>
     <td width="50%">item is finished.</td>
     <td valign="top">
      <img
       height="12"
       width="40">
       <xsl:attribute name="src">
								<xsl:value-of select="concat($prefix, 'help.gif')" />
							</xsl:attribute>
      </img>
     </td>
     <td width="50%">help wanted</td>
    </tr>
    <tr>
     <td valign="top">
      <img
       height="10"
       width="10">
       <xsl:attribute name="src">
								<xsl:value-of select="concat($prefix, 'defer.gif')" />
							</xsl:attribute>
      </img>
     </td>
     <td width="50%">item is deferred.</td>
     <td valign="top">
      <font class="indexsub">
       <img>
        <xsl:attribute name="src">
									<xsl:value-of select="concat($prefix, 'new.gif')" />
								</xsl:attribute>
       </img>
      </font>
     </td>
     <td width="50%">new</td>
    </tr>
   </tbody>
  </table>
 </xsl:template>
 <xsl:template name="drawHelpWanted">
  <xsl:param name="rel"></xsl:param>
  <xsl:variable
   name="prefix"
   select="concat($rel, 'development/milestone_plans/stylesheets/images/')">
  </xsl:variable>
  <img
   height="12"
   width="40">
   <xsl:attribute name="src">
				<xsl:value-of select="concat($prefix, 'help.gif')" />
			</xsl:attribute>
  </img>
 </xsl:template>
 <xsl:template name="drawDetail">
  <xsl:param name="rel"></xsl:param>
  <xsl:param name="detail"></xsl:param>
  <xsl:variable
   name="prefix"
   select="concat($rel, 'development/milestone_plans/stylesheets/images/')">
  </xsl:variable>
  <xsl:variable name="apos">'</xsl:variable>
  <xsl:variable name="quote">"</xsl:variable>
  <xsl:variable name="apos-translated"></xsl:variable>
  <xsl:variable name="detail-escapedMarkup">
   <xsl:apply-templates
    select="$detail"
    mode="escape-detail" />
  </xsl:variable>
  <xsl:variable
   name="detail-escaped"
   select="translate($detail-escapedMarkup, $quote, $apos)">
  </xsl:variable>
  <img
   height="15"
   width="16">
   <xsl:attribute name="src">
				<xsl:value-of select="concat($prefix, 'detail.gif')" />
			</xsl:attribute>
   <xsl:attribute name="onmouseout">
				<xsl:value-of select="'hidePopup()'" />
			</xsl:attribute>
   <xsl:attribute name="onmouseover">
				<xsl:value-of select='concat( "showPopup(" , $quote, normalize-space($detail-escaped) , $quote, ")")' />
			</xsl:attribute>
  </img>
 </xsl:template>
 <xsl:template name="drawBug">
  <xsl:param name="bug"></xsl:param>
  [
  <a>
   <xsl:attribute name="href">
				<xsl:value-of select="concat('https://bugs.eclipse.org/bugs/show_bug.cgi?id=', $bug)" />
			</xsl:attribute>
   <xsl:value-of select="$bug" />
  </a>
  ]
 </xsl:template>
 <xsl:template name="drawStatus">
  <xsl:param name="rel"></xsl:param>
  <xsl:param name="value"></xsl:param>
  <xsl:param name="defaultImage"></xsl:param>
  <xsl:variable
   name="prefix"
   select="concat($rel, 'development/milestone_plans/stylesheets/images/')">
  </xsl:variable>
  <xsl:choose>
   <xsl:when test="$value = 'in-progress'">
    <img
     height="5"
     width="13">
     <xsl:attribute name="src">
						<xsl:value-of select="concat($prefix, 'progress.gif')" />
					</xsl:attribute>
    </img>
   </xsl:when>
   <xsl:when test="$value = 'done'">
    <img
     height="10"
     width="10">
     <xsl:attribute name="src">
						<xsl:value-of select="concat($prefix, 'ok.gif')" />
					</xsl:attribute>
    </img>
   </xsl:when>
   <xsl:when test="$value = 'investigate'">
    <img
     height="10"
     width="10">
     <xsl:attribute name="src">
						<xsl:value-of select="concat($prefix, 'glass.gif')" />
					</xsl:attribute>
    </img>
   </xsl:when>
   <xsl:when test="$value = 'deferred'">
    <img
     height="10"
     width="10">
     <xsl:attribute name="src">
                			<xsl:value-of select="concat($prefix, 'defer.gif')" />
                		</xsl:attribute>
    </img>
   </xsl:when>
   <xsl:otherwise>
    <xsl:if test="$defaultImage">
     <img
      height="10"
      width="10">
      <xsl:attribute name="src">
                			<xsl:value-of select="concat($prefix, $defaultImage)" />
                		</xsl:attribute>
     </img>
    </xsl:if>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:template>
 <!--  the following templates are used to escape HTML markup in order to render detail content -->
 <xsl:template
  match="*|@*|comment()|processing-instruction()|text()"
  mode="escape-detail">
  <xsl:copy>
   <xsl:apply-templates
    select="*|@*|comment()|processing-instruction()|text()"
    mode="escape-detail" />
  </xsl:copy>
 </xsl:template>
 <xsl:template
  match="*"
  mode="escape-detail">
  &lt;
  <xsl:value-of select="name(.)" />
  &gt;
  <xsl:apply-templates
   select="*|@*|comment()|processing-instruction()|text()"
   mode="escape-detail" />
  &lt;/
  <xsl:value-of select="name(.)" />
  &gt;
 </xsl:template>
</xsl:stylesheet>