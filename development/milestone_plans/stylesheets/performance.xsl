<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xslt" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">  

	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../stylesheets/milestone-common.xsl" />
	<xsl:variable name="plans" select="document(/report/milestone/plan/@location)/plan" />
	<xsl:variable name="rel" select="'../../../'" />
	<xsl:variable name="title" select="'Eclipse WTP performance improvement plan'" />

	<xsl:template match="report">
		<html>
			<head>
				<title><xsl:value-of select="$title"/></title>
				<link href="../stylesheets/images/default_style.css" type="text/css" rel="stylesheet" />
			</head>
			<body>
				<xsl:call-template name="printHeader">
					<xsl:with-param name="rel" select="$rel" />
					<xsl:with-param name="title" select="$title" />
					<xsl:with-param name="description" select="'Eclipse Web Tools Platform project performance improvements'" />
				</xsl:call-template>
				<xsl:for-each select="/report/milestone[substring(@name, 5) >= substring(/report/@current-milestone, 5)]/plan">
					<xsl:apply-templates select="." />
				</xsl:for-each>
				<xsl:call-template name="printLegend">
					<xsl:with-param name="rel" select="$rel" />
				</xsl:call-template>
				<br />
				<br />
				<br />
				<p>
					Please see our
					<a href="http://eclipse.org/legal/privacy.html">privacy policy</a>
					and website
					<a href="http://eclipse.org/legal/termsofuse.html">terms of use</a>
					. For problems with the eclipse.org site, please contact the
					<a href="mailto:webmaster@eclipse.org">webmaster</a>
					or read the
					<a href="http://eclipse.org/webmaster/index.html">webmaster FAQ</a>
					for answers to common questions!
				</p>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="plan">
		<xsl:variable name="plan" select="document(@location)"/>
		<xsl:for-each select="$plan/plan/component" >
			<xsl:sort select="@subproject"/>
			<xsl:sort select="@name"/>
			<xsl:apply-templates select="."/>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="component">
		<xsl:for-each select="milestone">
			<xsl:sort data-type="number" select="@name"/>
			<xsl:apply-templates select="."/>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="milestone">
		<xsl:for-each select="category">
			<xsl:apply-templates select="."/>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="category">
		<xsl:if test="@name='Performance' or @name='performance'">
			<table width="100%">
				<tr>
					<td height="20" bgcolor="#0080c0" colspan="2">
						<a><xsl:attribute name="name"><xsl:value-of select="concat(../../@subproject, '', ../../@name, '', ../@name)" /></xsl:attribute></a>					
						<b>
							<font color="#ffffff"><xsl:value-of select="concat(../../@subproject, ' ', ../../@name, ' ', ../@name)" /></font>
						</b>
					</td>
					<td>
						<xsl:value-of select="@description"/>
					</td>
				</tr>
			</table>
			<xsl:for-each select="item">
				<xsl:apply-templates select="."/>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>

	<xsl:template match="item">
		<table>
			<tr>
				<td>
					<img src="../stylesheets/images/Adarrow.gif" nosave="" border="0" height="16" />
				</td>
				<td>
					<xsl:call-template name="drawStatus">
						<xsl:with-param name="rel" select="$rel" />
						<xsl:with-param name="value" select="@status" />
						<xsl:with-param name="defaultImage" select="'default.gif'" />
					</xsl:call-template>
					<b><xsl:value-of select="description" /></b>
					&#160;[<xsl:value-of select="@priority"/>]
					<xsl:if test="@helpWanted">
						<xsl:call-template name="drawHelpWanted">
							<xsl:with-param name="rel" select="$rel" />
						</xsl:call-template>
					</xsl:if>
				</td>
			</tr>
		</table>
		<table>
			<xsl:if test="detail">
				<tr>
					<td valign="top"><i>Detail:</i></td>
					<td><xsl:apply-templates select="detail" mode="markup"/></td>
				</tr>
			</xsl:if>
			<xsl:if test="step">
				<tr>
					<td valign="top"><i>Steps:</i></td>
					<td>
						<ol>
							<xsl:for-each select="step">
								<li><xsl:apply-templates select="description" mode="markup"/></li>
							</xsl:for-each>
						</ol>
					</td>
				</tr>
			</xsl:if>
			<xsl:if test="bugzilla">
				<tr>
					<td valign="top"><i>Bugzillas:</i></td>
					<td><a href="{bugzilla[1]/@link}"><xsl:value-of select="bugzilla[1]/@link"/></a></td>
				</tr>
				<xsl:for-each select="bugzilla[position() > 1]">
					<tr>
						<td></td>
						<td><a href="{@link}"><xsl:value-of select="@link"/></a></td>
					</tr>
				</xsl:for-each>
			</xsl:if>
			<xsl:if test="junit-test">
				<tr>
					<td valign="top"><i>JUnit tests:</i></td>
					<td><a href="{junit-test[1]/@link}"><xsl:value-of select="junit-test[1]/@link"/></a></td>
				</tr>
				<xsl:for-each select="junit-test[position() > 1]">
					<tr>
						<td></td>
						<td><a href="{@link}"><xsl:value-of select="@link"/></a></td>
					</tr>
				</xsl:for-each>
			</xsl:if>
			<xsl:if test="verification">
				<tr>
					<td valign="top"><i>Verification:</i></td>
					<td><xsl:apply-templates select="verification" mode="markup"/></td>
				</tr>
			</xsl:if>
		</table>
		<br/>
	</xsl:template>

	<xsl:template match="*|@*|text()" mode="markup">
		<xsl:copy>
			<xsl:apply-templates select="*|@*|text()" mode="markup"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
