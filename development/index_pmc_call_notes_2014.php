<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon()); # All on the same line to unclutter the user's desktop'
$pageKeywords	= "PMC Meetings Eclipse Web Tools Platform WTP";
$pageAuthor		= "David Williams";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

$pageTitle = "Eclipse Web Tools Platform PMC Meetings";

$xmlString = <<<EOXML
<html>
<body>
<h1>$pageTitle</h1>
<h2>Call Info</h2>

<p>Toll free (in US and Canada): 888-426-6840 <br />
Access code: 18184877# <br />
Caller pay alternative number: 215-861-6239<br />
<a
	href="https://www.teleconference.att.com/servlet/glbAccess?process=1&accessCode=6460142&accessNumber=2158616239">Full
list of global access numbers</a><br />
<br />
<p><a href="http://www.timeanddate.com/worldclock/custom.html?cities=224,207,1440,107&amp;hour=11&amp;min=0&amp;sec=0&amp;p1=207">Call
Time: 1500 UTC</a></p>

<h2>Meeting agendas and minutes</h2>
<ul compact="true">

<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2014-12-02">December 2, 2014</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2014-11-18">November 18, 2014</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2014-11-04">November 4, 2014</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2014-10-21">October 21, 2014</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2014-10-07">October 7, 2014</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2014-09-09">September 9, 2014</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2014-08-26">August 26, 2014</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2014-08-12">August 12, 2014</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2014-07-22">July 22, 2014</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2014-06-17">June 17, 2014</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2014-05-27">May 27, 2014</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2014-05-06">May 6, 2014</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2014-04-15">April 15, 2014</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2014-03-25">March 25, 2014</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2014-02-25">February 25, 2014</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2014-02-11">February 11, 2014</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2014-01-14">January 14, 2014</a></li>

<li><a href="index_pmc_call_notes_2013.php">2013</a></li>
<li><a href="index_pmc_call_notes_2012.php">2012</a></li>
<li><a href="index_pmc_call_notes_2011.php">2011</a></li>
<li><a href="index_pmc_call_notes_2010.php">2010</a></li>
<li><a href="index_pmc_call_notes_2009.php">2009</a></li>
<li><a href="index_pmc_call_notes_2008.php">2008</a></li>
<li><a href="index_pmc_call_notes_2007.php">2007</a></li>
<li><a href="index_pmc_call_notes_2006.php">2006</a></li>
<li><a href="index_pmc_call_notes_2005.php">2005</a></li>
<li><a href="index_pmc_call_notes_2004.php">2004</a></li>
</ul>
<hr />
<!-- <p>Back to <a href="/webtools/development/index_pmc_call_notes.php">meeting list</a>.</p>
-->
<p>Please send any additions or corrections to <a href="mailto: david_williams@us.ibm.com">David Williams.</a></p>
</body>
</html>
EOXML;

$xml = DOMDocument::loadHTML($xmlString);
// Load the XSL source
//$xsl = DOMDocument::load($root . '/webtools/wtpphoenix.xsl');
// Load the XSL source
$xsl = DOMDocument::load($root . '/webtools/wtpnova.xsl');
// Configure the transformer
$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl); // attach the xsl rules

//echo "xml:";
//echo $xml->saveXML();
$maincontent = $proc->transformToXml($xml);
//echo "maincontent:";
//echo $maincontent;
$html = <<<EOHTML
<div id="maincontent">
$wtpTopButtons
$maincontent
</div>
EOHTML;

$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
