<?php
  function shiftDown($array, $size, $start)
  {
  	for ($i = $size; $i > $start; $i--)
  	{
  		$array[$i] = $array[$i - 1];
  	}
  	return $array;
  }

  function genTop10($sqlquery, $urlquery, $col2name, $dayBuffer)
  {
	require_once "/home/data/httpd/eclipse-php-classes/system/dbconnection_bugs_ro.class.php";
	
	# Connect to database
	$dbc = new DBConnectionBugs();
	$dbh = $dbc->connect();
	$rs = mysql_query($sqlquery, $dbh);
	
	if(mysql_errno($dbh) > 0)
	{
		echo "There was an error processing this request".
		
		# For debugging purposes - don't display this stuff in a production page.
		# echo mysql_error($dbh);
		
		# Mysql disconnects automatically, but I like my disconnects to be explicit.
		$dbc->disconnect();
		exit;
	}

	$currReporter = null;
	$reporter[1] = null;
	$reporter[2] = null;
	$reporter[3] = null;
	$reporter[4] = null;
	$reporter[5] = null;
	$reporter[6] = null;
	$reporter[7] = null;
	$reporter[8] = null;
	$reporter[9] = null;
	$reporter[10] = null;
	$currCount = 0;
	$count[1] = 0;
	$count[2] = 0;
	$count[3] = 0;
	$count[4] = 0;
	$count[5] = 0;
	$count[6] = 0;
	$count[7] = 0;
	$count[8] = 0;
	$count[9] = 0;
	$count[10] = 0;
	if ($dayBuffer == null)
		$dayBuffer = 0;
	$cutoff = date("YmdHis", mktime(date("H"), date("i"), date("s"), date("m"), date("d") - $dayBuffer, date("Y")));
	while($myrow = mysql_fetch_assoc($rs))
	{
		if ($myrow['reporter'] == null || $myrow['reporter'] == "")
		{
			$currReporter = null;
			$currCount = 0;
			continue;
		}
		if ($myrow['delta_ts'] != null && $cutoff < $myrow['delta_ts'])
		{
			continue;
		}
		if ($myrow['reporter'] != $currReporter)
		{
			if ($currReporter != null && $currCount > 0)
			{
				for ($i = 10; $i > 0; $i--)
				{
					if ($reporter[$i] == null)
					{
						if ($i == 1)
						{
							$reporter[1] = $currReporter;
							$count[1] = $currCount;
							break;
						}
						else
						{
							continue;
						}
					}
					else if ($currCount > $count[$i])
					{
						if ($i == 1)
						{
							$reporter = shiftDown($reporter, 10, 1);
							$count = shiftDown($count, 10, 1);
							$reporter[1] = $currReporter;
							$count[1] = $currCount;
							break;
						}
						else
						{
							continue;
						}
					}
					else if ($i != 10)
					{
						$reporter = shiftDown($reporter, 10, $i + 1);
						$count = shiftDown($count, 10, $i + 1);
						$reporter[$i + 1] = $currReporter;
						$count[$i + 1] = $currCount;
						break;
					}
					else
					{
						break;
					}
				}
			}
			$currReporter = $myrow['reporter'];
			$currCount = 1;
		}
		else
		{
			$currCount++;
		}
	}

	$dbc->disconnect();
	$rs = null;
	$dbh = null;
	$dbc = null;

	echo "<table border=\"1\" cellspacing=\"0\" align=\"center\">";
	echo "<tr><th>Rank</th><th>$col2name</th><th>Bug count</th></tr>";
	for ($i = 1; $i <= 10; $i++)
	{
		if ($reporter[$i] != null && $count[$i] > 0)
		{
			if ($i % 2 != 0)
			{
				echo "<tr><td align=\"center\" bgcolor=\"#dddddd\">$i</td><td bgcolor=\"#dddddd\" align=\"right\">$reporter[$i]</td><td bgcolor=\"#dddddd\" align=\"right\"><a href=\"$urlquery\">$count[$i]</a></td></tr>";
			}
			else
			{
				echo "<tr><td align=\"center\">$i</td><td align=\"right\">$reporter[$i]</td><td align=\"right\"><a href=\"$urlquery\">$count[$i]</a></td></tr>";
			}
		}
	}
	echo "</table>";
  }
?>