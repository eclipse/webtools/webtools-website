<?php 
//ini_set("display_errors", "true");
//error_reporting (E_ALL);
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");      require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");      require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");     $App   = new App();  $Nav   = new Nav();  $Menu  = new Menu();

       #*****************************************************************************
       #
       # poll2011name.php
       #
       # Author:            David Williams
       # Date:                     2010-04-07
       #
       # Description: Quick poll to get input for Helios +1 Sim. Release name. 
       #
       #
       #****************************************************************************
       
       #
       # Begin: page-specific settings.  Change these. 
	$pageTitle 		= "Community Poll";
       $pageKeywords = "Eclipse Simultaneous Release 2011";
       $pageAuthor          = "David Williams";
       $theme = "Nova";




              
       # Paste your HTML content between the markers!   
ob_start();
?>            


<div id="maincontent" style="width: 90%; padding: 5%;">

              <h1><?= $pageTitle ?></h1>
                 <h2>Help decide the name for the 2011 Eclipse Simultaneous Release</h2>
                     <p>There will be an Eclipse Simultaneous Release on the fourth Wednesday of
                     June, 2011. But what to call it? </p>
                     <p>Below are the results of our May, 2010 community poll.</p>
                     <p>BUT, it is not done yet. More processes have been started before settling on final 
                     name. The Planning Council will take these community preferences, and 
                     evaluate them, in order, for any issues of Trademark infringement, etc. It appears 
                     a lot of you preferred 'Indigo' so hopefully that will pass all other scrutiny, but if not
                     we'll examine next on the list.</p>
                     <p>We should get this settled in a couple of weeks. Thanks!</p>
<pre>                     
Indigo: 36 % 
Ion:    16 % 
Isis:   10 % 
Iris:    9 % 
Isaac:   9 % 
Ivory:   7 % 
Ionia:   4 % 
Izar:    4 % 
Indra:   2 % 
Indus:   1 % 

Total votes: 339 
</pre>                     
</div>

<?php
       $html = ob_get_contents();
       ob_end_clean();

       # Generate the web page
       $App->generatePage($theme, $Menu, NULL, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>


