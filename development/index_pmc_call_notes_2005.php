<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
$pageKeywords	= "PMC Meetings Eclipse Web Tools Platform WTP";
$pageAuthor		= "David Williams";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

$pageTitle = "Eclipse Web Tools Platform PMC Meetings";

$xmlString = <<<EOXML
<html>
<body>
<h1>$pageTitle</h1>
<h2>Call Info</h2>
<p>Toll free in the US: 877-421-0030</p>
<p>Alternate: 770-615-1247</p>
<p>Access code: 800388# </p>
<p><a href="http://wiki.eclipse.org/images/f/f6/WTP_status_phone_access.pdf">Full list of phone numbers</a></p>
<p><a href="http://www.timeanddate.com/worldclock/custom?cities=224,207,1440,107&amp;hour=11&amp;min=0&amp;sec=0&amp;p1=207">Call
Time: 1500 UTC</a></p>
<h2>Meeting agendas and minutes</h2>
<ul>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-12-20">December 20, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-12-13">December 13, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-12-06">December 6, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-11-29">November 29, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-11-22">November 22, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-11-15">November 15, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-11-08">November 8, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-11-01">November 1, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-10-25">October 25, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-10-18">October 18, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-10-11">October 11, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-10-04">October 4, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-09-27">September 27, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-09-20">September 20, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-09-13">September 13, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-09-06">September 6, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-08-30">August 30, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-08-23">August 23, 2005</a></li>
		<li>8/16 Vacation; no meeting</li>
		<li>8/9 Vacation; no meeting</li>
		<li>8/2 Vacation; no meeting</li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-07-26">July 26, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-07-19">July 19, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-07-12">July 12, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-07-05">July 05, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-06-28">June 28, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-06-21">June 21, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-06-14">June 14, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-06-07">June 7, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-05-31">May 31, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-05-24">May 24, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-05-17">May 17, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-05-10">May 10, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-05-03">May 3, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-04-26">April 26, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-04-12">April 12, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-04-05">April 5, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-03-29">March 29, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-03-22">March 22, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-03-15">March 15, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-03-08">March 8, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-02-21">February 21, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-02-15">February 15, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-02-08">February 8, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-02-01">February 1, 2005</a></li>
		<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2005-01-04">January 4, 2005</a></li>
</ul>
<hr />
<p>Back to <a href="/webtools/development/index_pmc_call_notes.php">meeting list</a>.</p>
<p>Please send any additions or corrections to <a href="mailto: david_williams@us.ibm.com">David Williams.</a></p>
</body>
</html>
EOXML;

	$xml = DOMDocument::loadXML($xmlString);
	// Load the XSL source
	$xsl = DOMDocument::load($root . '/webtools/wtpnova.xsl');

	// Configure the transformer
	$proc = new XSLTProcessor;
	$proc->importStyleSheet($xsl); // attach the xsl rules

	$maincontent = $proc->transformToXML($xml);

$html = <<<EOHTML
<div id="maincontent">
	<div id="midcolumn">
	$maincontent
	</div>
</div>
EOHTML;
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
