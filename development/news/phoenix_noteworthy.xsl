<?xml version="1.0" encoding="utf-8"?>
<!-- 
    An XSLT stylesheet that formats the N&N pages to resemble the Eclipse Phoenix look and feel.
    
    Change Log:
    
    2007-02-22: 15022010 
    - Stole file from Nitin and created new file. Changed to Phoenix LAF.
    
    2005-06-22: Nitin Dahyabhai <nitind@us.ibm.com>
    - Created.
    
    
    
    Usage: 1 release per file, 
    
    <release name="release or milestone number, like 1.5">
    <greeting>salutations just for this page</greeting>
    <component name="component name, such as Structured Source Editing">
    <item title="item title can go here">
    <title>item title can also go here</title>
    <description>description of this new feature, preferably with cropped .pngs of the UI</description>
    </item>
    </component>
    </release>
    
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <!-- read the release name into variable -->
    <xsl:template match="release">
        <xsl:variable name="release" select="@name" />
        <div id="maincontent">
            <div id="midcolumn">
                <table>
                    <tr>
                        <td width="100%">
                            <h1>
                                Eclipse Web Tools Platform
                                <xsl:value-of select="$release" />
                                <br/>New and Noteworthy
                            </h1>
                        </td>
	                    <td valign="top">
	                      	<a href="javascript:history.back()">Back</a>
	                    </td>
                    </tr>
                </table>
                <xsl:apply-templates select="greeting" />
				<xsl:apply-templates select="header" />
				<xsl:if test="count(//header) &gt; 0">
					<p/>
				</xsl:if>
                <xsl:apply-templates select="component" />
				<xsl:if test="count(//footer) &gt; 1">
					<hr />
				</xsl:if>
				<xsl:apply-templates select="footer" />
				<xsl:if test="count(//footer) &lt; 1">
				</xsl:if>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="component">
        <h2 class="bar">
            <xsl:value-of select="@name" />
        </h2>
        <xsl:apply-templates select="item" />
    </xsl:template>

    <xsl:template match="item">
        <xsl:comment>new item</xsl:comment>
        <h3>
          <xsl:attribute name="id">
            <xsl:value-of select="@title" />
          </xsl:attribute>

            <xsl:apply-templates select="title" />
            <xsl:value-of select="@title" />
        </h3>
        <ul class="indent">
            <xsl:apply-templates select="description" />
        </ul>
        <br />
    </xsl:template>

    <xsl:template match="greeting">
        <xsl:apply-templates select="*|@*|text()" />
    </xsl:template>

	<xsl:template match="header">
		<xsl:apply-templates select="*|@*|text()" />
	</xsl:template>

	<xsl:template match="footer">
		<xsl:apply-templates select="*|@*|text()" />
	</xsl:template>

    <xsl:template match="title">
        <xsl:apply-templates select="*|@*|text()" />
    </xsl:template>

    <xsl:template match="description">
        <xsl:apply-templates select="*|@*|text()" />
    </xsl:template>

    <xsl:template match="*|@*|text()">
        <xsl:copy>
            <xsl:apply-templates select="*|@*|text()" />
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
