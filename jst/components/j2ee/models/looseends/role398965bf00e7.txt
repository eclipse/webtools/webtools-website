The variable element provides information on the scripting variables defined by this tag.
It is a (translation time) error for an action that has one or more variable subelements to have a TagExtraInfo class that returns a non-null object.
The subelements of variable are of the form:
name-given -- The variable name as a constant
name-from-attribute -- The name of an attribute whose (translation time) value will give the name of the variable. One of name-given or namefrom-attribute is required.
variable-class -- Name of the class of the variable. java.lang.String is default.
declare -- Whether the variable is declared or not. True is the default.
scope -- The scope of the scripting variable defined. NESTED is default.

