The tei-class element indicates the subclass of javax.servlet.jsp.tagext.TagExtraInfo for this tag. The class is instantiated at translation time. This element is optional.
@regexp fully qualified Java class name
