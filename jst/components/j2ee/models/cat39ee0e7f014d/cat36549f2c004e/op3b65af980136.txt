Get the component type of this array. 

If this is a multi-dimensional array, the component type will be the nested array type.
