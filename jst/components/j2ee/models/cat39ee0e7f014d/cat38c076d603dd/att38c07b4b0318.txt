@deprecated See jsp::TagLibRef

The taglib-location element contains the location (as a resource relative to the root of the web application) where to find the Tag Libary Description file for the tag library.
