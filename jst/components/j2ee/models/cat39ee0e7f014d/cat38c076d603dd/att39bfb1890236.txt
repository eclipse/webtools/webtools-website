The url-pattern element contains the url pattern of the mapping. Must follow the rules specified in Section 10 of the Servlet API Specification.
