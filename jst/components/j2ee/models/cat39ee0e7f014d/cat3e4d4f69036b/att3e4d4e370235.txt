The include-coda element is a context-relative
	path that must correspond to an element in the
	Web Application. When the element is present,
	the given path will be automatically included
	(as in an include directive) at the end of each
	JSP page in this jsp-property-group.
