@since J2EE1.4
The jsp-property-groupType is used to group a number of
files so they can be given global property information.
All files so described are deemed to be JSP files. The
following additional properties can be described:

- Control enabling of EL evaluation. - Control enabling
of Scripting elements. - Indicate pageEncoding
information. - Indicating that a resource is a JSP
document - Prelude and Coda automatic includes.
