 A taglib-uri element describes a URI identifying a
        tag library used in the web application.  The body
        of the taglib-uri element may be either an
        absolute URI specification, or a relative URI.
        There should be no entries in web.xml with the
        same taglib-uri value.
