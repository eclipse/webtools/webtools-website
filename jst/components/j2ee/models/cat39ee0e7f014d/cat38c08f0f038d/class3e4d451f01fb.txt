@since J2EE1.4
This group keeps the usage of the contained description related
elements consistent across J2EE deployment descriptors.
