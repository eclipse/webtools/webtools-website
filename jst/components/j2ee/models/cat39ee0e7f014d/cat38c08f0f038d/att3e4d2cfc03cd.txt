The message-destination-name element specifies a
        name for a message destination.  This name must be
        unique among the names of message destinations
        within the Deployment File.
