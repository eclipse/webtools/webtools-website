@since J2EE1.3
The ejb-local-refType is used by ejb-local-ref elements for
the declaration of a reference to an enterprise bean's local
home. The declaration consists of:

    - an optional description
    - the EJB reference name used in the code of the Deployment
      Component that's referencing the enterprise bean
    - the expected type of the referenced enterprise bean
    - the expected local home and local interfaces of the
      referenced enterprise bean
    - optional ejb-link information, used to specify the
      referenced enterprise bean
