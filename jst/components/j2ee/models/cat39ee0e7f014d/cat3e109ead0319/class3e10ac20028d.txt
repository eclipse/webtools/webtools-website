The ejb-link element is used in the service-impl-bean element to specify that a Service Implementation Bean is defined as a Web Service Endpoint.

The value of the ejb-link element must be the ejb-name of an enterprise bean in the same ejb-jar file.

Used in: service-impl-bean

Examples:
<ejb-link>EmployeeRecord</ejb-link>
	<ejb-link>../products/product.jar#ProductEJB</ejb-link>

