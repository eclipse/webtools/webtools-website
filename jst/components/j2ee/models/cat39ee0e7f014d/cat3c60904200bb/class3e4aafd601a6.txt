@since J2EE1.4
The required-config-propertyType contains a declaration
of a single configuration property used for specifying a
required configuration property name. It is used
by required-config-property elements.

Example:

<required-config-property>Destination</required-config-property>
