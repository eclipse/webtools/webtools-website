@since J2EE1.4
The outbound-resourceadapterType specifies information about
an outbound resource adapter. The information includes fully
qualified names of classes/interfaces required as part of
the connector architecture specified contracts for
connection management, level of transaction support
provided, one or more authentication mechanisms supported
and additional required security permissions.

If there is no authentication-mechanism specified as part of
resource adapter element then the resource adapter does not
support any standard security authentication mechanisms as
part of security contract. The application server ignores
the security part of the system contracts in this case.
