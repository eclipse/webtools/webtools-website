The connection-interface element specifies the fully
        qualified name of the Connection interface supported
        by the resource adapter.

        Example:

            <connection-interface>javax.resource.cci.Connection
            </connection-interface>
