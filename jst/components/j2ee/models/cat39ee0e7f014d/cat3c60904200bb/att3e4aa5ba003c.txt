The element reauthentication-support specifies
        whether the resource adapter implementation supports
        re-authentication of existing Managed- Connection
        instance. Note that this information is for the
        resource adapter implementation and not for the
        underlying EIS instance. This element must have
        either a "true" or "false" value.
