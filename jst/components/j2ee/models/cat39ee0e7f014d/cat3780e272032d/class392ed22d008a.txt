The method element is used to denote a method of an enterprise bean's
home or remote interface, or a set of methods. The ejb-name element
must be the name of one of the enterprise beans in declared in the
deployment descriptor; the optional method-intf element allows to
distinguish between a method with the same signature that is defined in
both the home and remote interface; the method-name element specifies
the method name; and the optional method-params elements identify a
single method among multiple methods with an overloaded method name.

There are three possible styles of the method element syntax:

1. 	<method>
		<ejb-name>EJBNAME</ejb-name>
   		<method-name>*</method-name>
	</method>

   This style is used to refer to all the methods of the specified
   enterprise bean's home and remote interfaces.

2. 	<method>
		<ejb-name>EJBNAME</ejb-name>
   		<method-name>METHOD</method-name>
	</method>>

   This style is used to refer to the specified method of the
   specified enterprise bean. If there are multiple methods with
   the same overloaded name, the element of this style refers to
   all the methods with the overloaded name.





3. 	<method>
		<ejb-name>EJBNAME</ejb-name>
   		<method-name>METHOD</method-name>
		<method-params>
   			<method-param>PARAM-1</method-param>
   			<method-param>PARAM-2</method-param>
          			...
   			<method-param>PARAM-n</method-param>
		</method-params>
	<method>	

   This style is used to refer to a single method within a set of
   methods with an overloaded name. PARAM-1 through PARAM-n are the
   fully-qualified Java types of the method's input parameters (if
   the method has no input arguments, the method-params element
   contains no method-param elements). Arrays are specified by the
   array element's type, followed by one or more pair of square
   brackets (e.g. int[][]).


Used in: method-permission and container-transaction

Examples:

    Style 1: The following method element refers to all the methods of
		the EmployeeService bean's home and remote interfaces:

		<method>
			<ejb-name>EmployeeService</ejb-name>
			<method-name>*</method-name>
		</method>

	Style 2: The following method element refers to all the create
		methods of the EmployeeService bean's home interface:

    		<method>
			<ejb-name>EmployeeService</ejb-name>
			<method-name>create</method-name>
		</method>

	Style 3: The following method element refers to the
		create(String firstName, String LastName) method of the
	 	EmployeeService bean's home interface.

    		<method>
			<ejb-name>EmployeeService</ejb-name>
			<method-name>create</method-name>
			<method-params>
				<method-param>java.lang.String</method-param>
				<method-param>java.lang.String</method-param>
			</method-params>
		</method>

	
	The following example illustrates a Style 3 element with
	more complex parameter types. The method
			foobar(char s, int i, int[] iar, mypackage.MyClass mycl,
				mypackage.MyClass[][] myclaar)
        would be specified as:

    		<method>
			<ejb-name>EmployeeService</ejb-name>
			<method-name>foobar</method-name>
			<method-params>
				<method-param>char</method-param>
				<method-param>int</method-param>
				<method-param>int[]</method-param>
				<method-param>mypackage.MyClass</method-param>
				<method-param>mypackage.MyClass[][]</method-param>
			</method-params>
		</method>

	The optional method-intf element can be used when it becomes
   necessary to differentiate between a method defined in the home
   interface and a method with the same name and signature that is
   defined in the remote interface.

	For example, the method element

   		<method>
			<ejb-name>EmployeeService</ejb-name>
			<method-intf>Remote</method-intf>
			<method-name>create</method-name>
			<method-params>
				<method-param>java.lang.String</method-param>
				<method-param>java.lang.String</method-param>
			</method-params>
		</method>

	can be used to differentiate the create(String, String) method
   defined in the remote interface from the create(String, String)
   method defined in the home interface, which would be defined as

   		<method>
			<ejb-name>EmployeeService</ejb-name>
			<method-intf>Home</method-intf>
			<method-name>create</method-name>
			<method-params>
				<method-param>java.lang.String</method-param>
				<method-param>java.lang.String</method-param>
			</method-params>
		</method>

