Provides a hint as to the content of the body of this tag. Primarily intended for use by page composition tools.There are currently three values specified:

tagdependent - The body of the tag is interpreted by the tag implementation itself, and is most likely in a		different "langage", e.g embedded SQL statements.

JSP - The body of the tag contains nested JSP syntax

empty - The body must be emptyThe default (if not defined) is JSP#PCDATA ::=  tagdependent | JSP | empty
