<?xml-stylesheet type="text/xsl" href="../../../development/milestone_plans/stylesheets/milestone-bulletList.xsl"?>
<plan xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../../../development/milestone_plans/milestonePlan.xsd">
	<component name="ws" subproject="jst">
		<description>JST Web services tools</description>
		<milestone name="M5">
			<title>M5 Plan</title>

			<category name="Web Service Framework and API">
				<item bug="96812" priority="medium" status="deferred">
					<description>Support automatic determination of compatible Web service RTs and Servers.</description>
					<developer name="rsinha@ca.ibm.com"/>
				</item>
				<item bug="93309" priority="high" status="in-progress">
					<description>Improved defaults for servers, server types, module types.</description>
					<developer name="rsinha@ca.ibm.com"/>
				</item>
				<item bug="96791" priority="high" status="in-progress">
					<description>Respect preferences for servers, server types, module types.</description>
					<developer name="rsinha@ca.ibm.com"/>
				</item>
				<item bug="96802" priority="high" status="in-progress">
					<description>Remove obsolete code / frameworks / extension points.</description>
					<developer name="rsinha@ca.ibm.com"/>
				</item>
			</category>

			<category name="Integration with Other Components">
				<item bug="98916" priority="high" status="in-progress">
					<description>React to changes in upstream plugins (ongoing), chiefly Server and J2EE tools.</description>
					<developer name="kathy@ca.ibm.com"/>
				</item>
			</category>

			<category name="Extensible Web Services Wizards">
				<item bug="98919" priority="high" status="investigate">
					<description>Clean up popup actions: Insure they are available strictly on appropriate objects.</description>
					<developer name="kathy@ca.ibm.com"/>
				</item>
				<item bug="89103" priority="medium" status="investigate">
					<description>Exploit Web Service Finder framework from WSDL selection page.</description>
					<developer name="joan@ca.ibm.com"/>
				</item>
				<item bug="89102" priority="medium" status="investigate">
					<description>Plug wizards into Web Service Consumer framework.</description>
					<developer name="gilberta@ca.ibm.com"/>
				</item>
				<item bug="92294" priority="medium" status="investigate">
					<description>Web service scenarios (or wizards) should the created IWebService or IWebServiceClient to callers.</description>
					<developer name="pmoogk@ca.ibm.com"/>
				</item>
			</category>

			<category name="Command Line Tools and ANT Tasks">
				<item bug="98921" priority="medium" status="deferred">
					<description>Define Command Line and/or ANT Task entry points to Web service scenarios.</description>
					<developer name="cbrealey@ca.ibm.com"/>
				</item>
			</category>

			<category name="Apache Axis">
				<item bug="92603" priority="high" status="investigate">
					<description>Support Apache Tomcat 5.5.</description>
					<developer name="kathy@ca.ibm.com"/>
				</item>
				<item bug="98922" priority="high" status="investigate">
					<description>Support Apache Geronimo.</description>
					<developer name="kathy@ca.ibm.com"/>
				</item>
				<item bug="98340" priority="high" status="investigate">
					<description>Support Apache Axis 1.2 (instead of Apache Axis 1.1 or 1.0).</description>
					<developer name="kathy@ca.ibm.com"/>
				</item>
				<item bug="98924" priority="medium" status="investigate">
					<description>Factor creation logic out of the axis.consumption plugin.</description>
					<developer name="kathy@ca.ibm.com"/>
				</item>
				<item bug="88684" priority="medium" status="deferred" helpWanted="true">
					<description>Enable Web service wizards to target multiple versions of Apache Axis (eg. 1.0, 1.1, 1.2RC3).</description>
					<developer name="kathy@ca.ibm.com"/>
					<step><description>Define internal Axis extension point.</description></step>
					<step><description>Define pluggable sets of Axis emitter option preferences.</description></step>
					<step><description>Define pluggable Axis jar manifests for emitter and deployed service runtime use.</description></step>
					<step><description>Define pluggable Axis emitter Ant task facades.</description></step>
				</item>
				<item bug="89924" priority="medium" status="deferred" helpWanted="true">
					<description>Support generation of Axis Java clients to multiple module types (not just Web).</description>
					<developer name="cbrealey@ca.ibm.com"/>
				</item>
				<item bug="89923" priority="medium" status="deferred" helpWanted="true">
					<description>Integrate generation of Axis JUnit tests into Test Facility extension point.</description>
					<developer name="cbrealey@ca.ibm.com"/>
				</item>
				<item bug="89922" priority="medium" status="deferred" helpWanted="true">
					<description>Support creation of Axis EJB Web services from existing Stateless Session EJBs (bottom-up).</description>
					<developer name="cbrealey@ca.ibm.com"/>
				</item>
			</category>

			<category name="Quality">
				<item bug="98745" priority="high" status="in-progress" helpWanted="true">
					<description>Define JUnit testcases for internal provisional API.</description>
					<developer name="sengpl@ca.ibm.com"/>
				</item>
				<item bug="98745" priority="medium" status="in-progress" helpWanted="true">
					<description>Define or enhance non-API JUnit testcases.</description>
					<developer name="sengpl@ca.ibm.com"/>
				</item>
				<item bug="96010" priority="high" status="investigate" helpWanted="true">
					<description>Write on-line documentation.</description>
					<developer name="cbrealey@ca.ibm.com"/>
				</item>
				<item bug="98776" priority="medium" status="investigate" helpWanted="true">
					<description>Add dynamic help to SWT componentry.</description>
					<developer name="pmoogk@ca.ibm.com"/>
				</item>
				<item bug="98772" priority="high" status="in-progress" helpWanted="true">
					<description>Integrate missing JavaDoc.</description>
					<developer name="cbrealey@ca.ibm.com"/>
				</item>
				<item bug="84074" priority="medium" status="in-progress" helpWanted="true">
					<description>Housekeeping: Clean up compilation warnings. Common cases:</description>
					<developer name="cbrealey@ca.ibm.com"/>
				</item>
			</category>

			<category name="Performance">

				<item bug="93111" priority="medium" status="investigate" helpWanted="true">
					<description>Use Eclipse 3.1 message bundles.</description>
					<developer name="sengpl@ca.ibm.com"/>
				</item>

				<item bug="98745" priority="high" status="done">
					<description>Automate JUnit performance tests.</description>
					<developer name="sengpl@ca.ibm.com"/>
					<detail>
						<ul>
							<li>JUnit tests are planned for the following scenarios:</li>
							<ul>
								<li>Java Web service bottom-up for Apache Axis 1.1 and Apache Tomcat 5.0.</li>
								<li>Java Web service skeleton for Apache Axis 1.1 and Apache Tomcat 5.0.</li>
								<li>Java Web service client for Apache Axis 1.1 and Apache Tomcat 5.0.</li>
								<li>Web Services Explorer logic (ie. without JSPs).</li>
								<li>Environment Command Framework.</li>
							</ul>
						</ul>
					</detail>
					<verification>
						<ul>
							<li>JUnit tests run regularly as part of the build.</li>
							<li>Performance improves incrementally.</li>
							<li>Regressions are captured in Bugzilla.</li>
						</ul>
					</verification>
				</item>

				<item bug="98932" priority="medium" status="investigate">
					<description>Improve performance and usability of server start up.</description>
					<developer name="sengpl@ca.ibm.com"/>
					<detail>
						<ul>
							<li>Start server on a thread other than the main Eclipse GUI thread.</li>
							<li>Report progress to the user.</li>
							<li>Enable the user to hit Cancel or Back to escape the process.</li>
							<li>Dispatch all remaining Commands at time of "Finish" in an Eclipse job.</li>
						</ul>
					</detail>
					<verification>
						<ul>
							<li>Eclipse GUI and wizard remains responsive during server startup.</li>
							<li>Eclipse wizards are dismissed at moment of "Finish".</li>
						</ul>
					</verification>
				</item>		        

				<item bug="98938" priority="medium" status="investigate">
					<description>Improve performance and usability of long-running code generation.</description>
					<developer name="sengpl@ca.ibm.com"/>
					<detail>
						<ul>
							<li>Run code generation on a thread other than the main Eclipse GUI thread.</li>
							<li>Report progress to the user.</li>
							<li>Enable the user to hit Cancel or Back to escape the process.</li>
						</ul>
					</detail>
					<verification>
						<ul>
							<li>Eclipse GUI and wizard remains responsive during code generation.</li>
						</ul>
					</verification>
				</item>		        

				<item bug="98941" priority="medium" status="deferred">
					<description>A-modal wizards.</description>
					<developer name="sengpl@ca.ibm.com"/>
					<detail>
						<ul>
							<li>Launch Web service wizards as a-modal such that the main Eclipse GUI is still available for us.</li>
						</ul>
					</detail>
					<verification>
						<ul>
							<li>Eclipse main GUI is available for use at all times during the wizard.</li>
						</ul>
					</verification>
				</item>

				<item bug="96982" priority="high" status="investigate">
					<description>Remove unnecessary server/application restarts.</description>
					<developer name="sengpl@ca.ibm.com"/>
					<detail>
						<ul>
							<li>Refactor commands to push server startup and project restart to as late as possible.</li>
							<li>Where possible, refactor scenarios to include only a single publish action.</li>
						</ul>
					</detail>
					<verification>
						<ul>
							<li>Use JUnit tests to measure elapsed time.</li>
							<li>Monitor servers and applications to ensure they are not restarted unnecessarily.</li>
						</ul>
					</verification>
				</item>

				<item bug="98944" priority="medium" status="deferred">
					<description>Improve UI Responsiveness.</description>
					<developer name="sengpl@ca.ibm.com"/>
					<detail>
						<ul>
							<li>Measure and improve speed of wizard page flips.</li>
							<li>Measure and improve speed of Web Services Explorer links.</li>
							<li>Measure and improve speed of Command execution times.</li>
							<li>Measure and improve speed of SWT control responses.</li>
							<li>Optimize slow algorithms in wizard pages and Commands.</li>
							<li>Investigate use of secondary threads for some Commands.</li>
							<li>Report accurate progress messages to the user.</li>
						</ul>
					</detail>
					<verification>
						<ul>
							<li>Use JUnit tests to measure elapsed times for certain actions.</li>
							<li>Exercise wizard scenarios to assess qualitative performance.</li>
						</ul>
					</verification>
				</item>

				<item bug="95696" priority="high" status="investigate" helpWanted="true">
					<description>Identify and resolve memory leaks.</description>
					<developer name="sengpl@ca.ibm.com"/>
					<detail>
						<ol>
							<li>Investigate tools which monitor and measure memory usage.</li>
							<li>Identify and fix memory leaks.</li>
						</ol>
					</detail>
					<verification>
						<ul>
							<li>Use JUnit to monitor memory footprint during, before and after each scenario</li>
						</ul>
					</verification>               	
				</item>

				<item bug="98982" priority="medium" status="deferred" helpWanted="true">
					<description>Identify and remove non-thread safe code.</description>
					<developer name="sengpl@ca.ibm.com"/>
				</item>

			</category>					

		</milestone>
	</component>
</plan>