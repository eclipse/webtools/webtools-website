<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="../../../../../wtp.xsl"?>
<html>
	<head>
		<meta name="root" content="../../../../../../" />
		<title>Bottom Up Web Service Tutorial</title>
	</head>
	<body>
		<h1>Bottom Up Web Service Tutorial</h1>
		<p>
			<b>By Kathy Chan</b>
			<br />
			Feb 25, 2005
			<br />
			<br />
		</p>

		<h2>Introduction</h2>
		<br></br>
		<p>
			This document is a one of a series of tutorials to
			demonstrate the use of the Web Services tools in the Web
			Tools Platform Project.
		</p>
		<p>
			This tutorial shows how to create a simple Web service and
			Web service client from a Java class. The Java class in this
			scenario converts between the Celsius and Farenheit
			temperature scales.
		</p>
		<br></br>
		<h2>Getting Started</h2>
		<br />
		<p>
			Follow the instruction at
			<a href="GettingStartedWebService.html">
				Getting Started with Web Service
			</a>
			before proceeding with the tutorial.
		</p>
		<br />
		<h2>Set Up</h2>
		<br></br>
		<p>
			Before creating the Web service, there are two
			prerequisites:
			<ol>
				<li>
					<a href="InstallTomcat.html">
						Install Apache Tomcat
					</a>
				</li>
				<li>Create a Web project</li>
				<ol>
					<li>
						Open
						<b>
							File -> New -> Other... -> Web -> J2EE Web
							Project
						</b>
						to create a new Web project named
						<tt>Converter</tt>
						.
					</li>
					<li>
						Enter
						<tt>converter</tt>
						into the
						<b>Name</b>
						field.
					</li>
					<li>
						Choose a
						<b>Servlet version</b>
						of
						<b>2.3</b>
						.
					</li>
					<li>
						Choose a
						<b>Target server</b>
						of
						<b>Apache Tomcat v4.1</b>
						.
					</li>
					<li>
						Deselect
						<b>Add module to an EAR project</b>
						. Result:
					</li>

					<br></br>
					<img border="0"
						SRC="resources/BottomUpWebService/image9E4.JPG" />
					<li>
						Click
						<b>Finish</b>
						.
					</li>
				</ol>

			</ol>
		</p>

		<h2>
			Creating a bottom up Java bean Web service and Web service
			client
		</h2>
		<br></br>
		<ol>
			<li>
				Import the
				<a
					href="resources/BottomUpWebService/wtp/Converter.java">
					wtp/Converter.java
				</a>
				class into Converter/JavaSource (be sure to preserve the
				package).
			</li>
			<li>
				Select the
				<b>Converter.java</b>
				file.
			</li>
			<li>
				Open File -&gt; New -&gt; Other... -&gt; Web Services
				-&gt; Web Service.
			</li>
			<li>
				Select
				<b>Generate a proxy</b>
				.
			</li>
			<li>
				Select
				<b>Test the Web service</b>
				.
			</li>
			<li>
				Select
				<b>Monitor the Web service</b>
				.
			</li>
			<li>
				Select
				<b>Overwrite files without warning</b>
				.
			</li>
			<li>
				Result:
			</li>
			<br></br>
			<img border="0"
				src="resources/BottomUpWebService/image8OG.JPG" />
			<li>
				Click
				<b>Next</b>
				. Result:
			</li>
			<br></br>
			<img border="0"
				src="resources/BottomUpWebService/imageB1G.JPG" />
			<li>
				Click
				<b>Next</b>
				to get to the
				<b>Service Deployment Configuration Page</b>
				. Result:
			</li>
			<br></br>
			<img border="0"
				src="resources/BottomUpWebService/imageL0F.JPG" />
			<li>
				If you want to choose a server different from the one
				defaulted by the wizard, click the
				<b>Edit</b>
				button to:
				<a href="SelectServer.html">
					select a server
				</a>
				.
			</li>
			<li>
				Click
				<b>Finish</b>
				.
			</li>
			<li>
				It will take about one minute for the wizard to assemble
				the Web service and Web service client Web projects,
				start Apache Tomcat, and deploy the projects to Tomcat.
				Once finished, the generated Sample JSP Web application
				will appear in the browser view, maximized here for
				clarity:
			</li>
			<br></br>
			<img border="0"
				src="resources/BottomUpWebService/image9ET.JPG" />
			<li>
				Under
				<b>Methods</b>
				, click on
				<b>celsiusToFarenheit(float)</b>
				.
			</li>
			<li>
				Under
				<b>Inputs</b>
				, enter
				<tt>37</tt>
				into the
				<b>celsius</b>
				entry field.
			</li>
			<li>
				Click on
				<b>Invoke</b>
				. In the Result view, you should get a response of
				<b>98.6</b>
				.
			</li>
			<br></br>
			<img border="0"
				src="resources/BottomUpWebService/imageOA2.JPG" />
			<li>
				<p>
					Since you selected the "Monitor Web service" check
					box, a TCP/IP port is automatically created on the
					Tomcat server. When you in invoked the Web service,
					the TCP/IP view would come up automatically showing
					the SOAP message request and response. Double-click
					on the TCP/IP view, select XML view in the drop-down
					combo for both request and response. Result:
				</p>
			</li>
			<br></br>
			<img border="0"
				src="resources/BottomUpWebService/imageL76.JPG" />
		</ol>
		<h2>Explanation</h2>
		<p>
			The Web Service wizard is orchestrating the end-to-end
			generation, assembly, deployment, installation and execution
			of the Web service, Web service client, and sample JSPs. In
			this scenario we clicked Finish on page one. In effect, this
			is allowing the wizard to pick reasonable defaults according
			to the basic high-level choices made on the first page. If
			you repeat this scenario, but use the Next button to work
			thru the wizard, you will learn more about the kinds of
			choices that are available and the kinds of defaults being
			assumed.
		</p>
		<p>
			After completing this scenario, the WSDL for the Converter
			Web service can be found in
			Converter/WebContent/wsdl/Converter.wsdl.
		</p>
		<p>
			Now that your Web service is running, there are a few
			interesting things you can do with this WSDL file. Examples:
		</p>
		<ol>
			<li>
				You can double-click on the WSDL to open the WSDL
				graphical editor.
			</li>

			<li>
				You can right-click on the WSDL and choose
				<b>
					Web Services -&gt; Test with Web Services Explorer
				</b>
				to test the service.
			</li>

			<li>
				You can right-click on the WSDL and choose
				<b>Web Services -&gt; Publish WSDL file</b>
				to publish the service to a public UDDI registry.
			</li>

			<li>
				You can click on the WSDL and choose
				<b>
					-&gt; New -&gt; Other... -&gt; Web Services -&gt;
					Web Service Client
				</b>
				to generate a Web service client. Note, however, that we
				have already witnessed the internal and visual features
				of the Web Service Client wizard since, in addition to
				being its own wizard, it is quite literally embedded
				within the larger Web Service wizard.
			</li>
		</ol>

	</body>
</html>
